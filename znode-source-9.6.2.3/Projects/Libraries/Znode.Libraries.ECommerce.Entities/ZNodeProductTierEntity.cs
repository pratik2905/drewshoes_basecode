﻿using System;
using System.Xml.Serialization;
using Znode.Libraries.Framework.Business;

namespace Znode.Libraries.ECommerce.Entities
{
    // Represents a product tier price
    [Serializable()]
    [XmlRoot("ZNodeProductTier")]
    public class ZnodeProductTierEntity : ZnodeBusinessBase
    {
        #region Private variables
        private int _ProductTierId;
        private int _ProductId;
        private int _ProfileId;
        private int _TierQuantity;
        private decimal _MinQuantity;
        private decimal _MaxQuantity;
        private decimal _Price;
        #endregion

        #region Public Properties  
        // Gets or sets the product tier Id.
        [XmlElement()]
        public int ProductTierID
        {
            get { return _ProductTierId; }
            set { _ProductTierId = value; }
        }

        // Gets or sets the product Id.
        [XmlElement()]
        public int ProductID
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        // Gets or sets the profile Id.
        [XmlElement()]
        public int ProfileID
        {
            get { return _ProfileId; }
            set { _ProfileId = value; }
        }

        // Gets or sets the tier start.
        [XmlElement()]
        public int TierQuantity
        {
            get { return _TierQuantity; }
            set { _TierQuantity = value; }
        }

        // Gets or sets the price.
        [XmlElement()]
        public decimal Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        // Gets or sets the tier start.
        [XmlElement()]
        public decimal MinQuantity
        {
            get { return _MinQuantity; }
            set { _MinQuantity = value; }
        }

        // Gets or sets the tier start.
        [XmlElement()]
        public decimal MaxQuantity
        {
            get { return _MaxQuantity; }
            set { _MaxQuantity = value; }
        }
        #endregion
    }
}
