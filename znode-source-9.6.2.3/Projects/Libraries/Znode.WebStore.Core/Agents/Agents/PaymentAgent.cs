﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.WebStore.Agents
{
    public class PaymentAgent : BaseAgent, IPaymentAgent
    {
        #region Private Variables
        protected readonly IPaymentClient _paymentClient;
        protected readonly IOrderClient _orderClient;
        protected readonly ICartAgent _cartAgent;
        protected readonly IQuoteClient _quoteClient;
        #endregion

        #region Public Constructor
        public PaymentAgent(IPaymentClient paymentClient, IOrderClient orderClient, IQuoteClient quoteClient)
        {
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
            _cartAgent = GetService<ICartAgent>();
            _orderClient = GetClient<IOrderClient>(orderClient);
            _quoteClient = GetClient<IQuoteClient>(quoteClient);
        }
        #endregion

        #region Public Methods

        public virtual PaymentDetailsViewModel GetPaymentDetails(int paymentSettingId, string quoteNumber = "")
        {
            string gatwayname = string.Empty;
            FilterCollection filters = new FilterCollection();

            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "1"));
            filters.Add(new FilterTuple(ZnodePaymentSettingEnum.PaymentSettingId.ToString(), FilterOperators.Equals, paymentSettingId.ToString()));
            PaymentSettingModel paymentSetting = null
                ;

            List<PaymentSettingModel> paymentSettingList = GetPaymentSettingListFromCache(PortalAgent.CurrentPortal.PortalId, Helper.GetProfileId().GetValueOrDefault(), filters, null);
            if (HelperUtility.IsNotNull(paymentSettingList))
                paymentSetting = paymentSettingList.FirstOrDefault(x => x.PaymentSettingId.Equals(paymentSettingId));


            PaymentDetailsViewModel model = new PaymentDetailsViewModel();

            if (HelperUtility.IsNotNull(paymentSetting))
            {
                model.IsPoDocUploadEnable = paymentSetting.IsPoDocUploadEnable;
                model.IsPoDocRequire = paymentSetting.IsPoDocRequire;
                model.IsBillingAddressOptional = paymentSetting.IsBillingAddressOptional;
                string totalAmount = string.Empty;
                if (!string.IsNullOrEmpty(quoteNumber))
                {
                    totalAmount = GetQuoteTotal(quoteNumber);
                }
                else
                {
                    totalAmount = GetOrderTotal();
                }
                if (string.Equals(paymentSetting.PaymentTypeName, ZnodeConstant.PAYPAL_EXPRESS.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
                          string.Equals(paymentSetting.PaymentTypeName, ZnodeConstant.Amazon_Pay.ToString(), StringComparison.CurrentCultureIgnoreCase))
                {
                    totalAmount = ConvertTotalToLocale(totalAmount);
                }
                else if (!string.IsNullOrEmpty(paymentSetting?.GatewayCode))
                {
                    totalAmount = Encryption.EncryptPaymentToken(ConvertTotalToLocale(totalAmount));
                }
                else
                {
                    model.HasError = string.IsNullOrEmpty(paymentSetting?.GatewayCode);
                }
                model.GatewayCode = paymentSetting.GatewayCode;
                model.PaymentCode = paymentSetting.PaymentCode;
                model.PaymentProfileId = paymentSetting.ProfileId;
                model.Total = totalAmount;
            }
            return model;
        }

        public virtual PaymentSettingViewModel GetPaymentSetting(int paymentSettingId, int portalId = 0)
        {
            try
            {
                PaymentSettingViewModel paymentSettingViewModel = null;
                PaymentSettingModel paymentSettingModel = GetPaymentSettingFromCache(paymentSettingId, portalId);
                int? profileid = paymentSettingModel.ProfileId;
                if (paymentSettingModel.IsCallToPaymentAPI)
                {
                    string paymentDisplayName = paymentSettingModel.PaymentDisplayName;
                    paymentSettingViewModel = GetPaymentSettingByPaymentCodeFromCache(paymentSettingModel.PaymentCode)?.ToViewModel<PaymentSettingViewModel>();

                    if (HelperUtility.IsNotNull(paymentSettingViewModel))
                    {
                        paymentSettingViewModel.PaymentSettingId = paymentSettingId;
                        paymentSettingViewModel.ProfileId = profileid;
                        paymentSettingViewModel.PaymentTypeName = paymentSettingModel.PaymentTypeName;
                        paymentSettingViewModel.PaymentDisplayName = paymentDisplayName;
                        paymentSettingViewModel.PaymentCode = paymentSettingModel.PaymentCode;
                    }
                    return paymentSettingViewModel;
                }
                return paymentSettingModel.ToViewModel<PaymentSettingViewModel>();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }

        }

        // Call PayNow method in Payment Application
        public virtual GatewayResponseModel ProcessPayNow(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.PayNow(model);
            return new GatewayResponseModel { HasError = true, };
        }

        // Call PayNow method in Payment Application
        public GatewayResponseModel ProcessPayPal(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.PayPal(model);
            return new GatewayResponseModel { HasError = true, };
        }

        public string GetOrderTotal()
        {
            decimal? total = 0;
            //Get shopping Cart from Session or cookie
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                           _cartAgent.GetCartFromCookie();

            if (HelperUtility.IsNotNull(shoppingCart))
                total = shoppingCart.Total;

            string strTotal = ConvertTotalToLocale(Convert.ToString(total));
            return strTotal;
        }

        //Get Quote Total
        public virtual string GetQuoteTotal(string quoteNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(quoteNumber))
                    return _quoteClient.GetQuoteTotal(quoteNumber);

                return string.Empty;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return string.Empty;
            }
        }

        //Get saved credit card details by customer GUID
        public PaymentMethodCCDetailsListModel GetPaymentCreditCardDetails(string customersGUID)
        {
            try
            {
                return _paymentClient.GetSavedCardDetailsByCustomerGUID(customersGUID);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return new PaymentMethodCCDetailsListModel { HasError = true };
            }
        }

        //Get Count of saved credit card by customers GUID.
        public int GetSaveCreditCardCount(string customersGUID)
        {
            try
            {
                PaymentMethodCCDetailsListModel cards = _paymentClient.GetSavedCardDetailsByCustomerGUID(customersGUID);
                return cards?.PaymentMethodCCDetails?.Count ?? 0;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Payment.ToString(), TraceLevel.Error);
                return 0;
            }
        }

        //Delete saved credit card details 
        public bool DeleteSavedCreditCardDetail(string paymentGUID)
        {
            try
            {
                return _paymentClient.DeleteSavedCreditCardDetail(paymentGUID);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Payment.ToString(), TraceLevel.Error);
                return false;
            }
        }

        /// <summary>
        /// Call PayPal payment finalize method in Payment Application
        /// </summary>
        /// <param name="submitPaymentModel">Submit Payment Model</param>
        /// <returns>Gateway Response Model</returns>
        public GatewayResponseModel FinalizePayPalProcess(SubmitPaymentModel submitPaymentModel)
        {
            if (HelperUtility.IsNotNull(submitPaymentModel.PaymentToken))
                return _paymentClient.FinalizePayPalProcess(submitPaymentModel);
            return new GatewayResponseModel { HasError = true, };
        }

        //Get the amount total in string formate as per locale
        public string FormatOrderTotal(decimal? orderTotal)
        {
            string formattedOrderTotal = ConvertTotalToLocale(Convert.ToString(orderTotal.GetValueOrDefault()));
            return formattedOrderTotal;
        }

        #endregion

        #region AmazonPay

        //Get Amazon Pay address from amazon.
        public SubmitPaymentModel GetAmazonPayAddressDetails(SubmitPaymentModel model)
           => _paymentClient.GetAmazonPayAddressDetails(model);

        //Call AmazonPay method in payment application.
        public GatewayResponseModel ProcessAmazonPay(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.AmazonPay(model);
            return new GatewayResponseModel { HasError = true, };
        }

        #endregion

        #region Private Method
        //to convert total amount to locale wise
        private string ConvertTotalToLocale(string total)
             => total.Replace(",", ".");


        public virtual List<PaymentSettingModel> GetPaymentSettingListFromCache(int portalId, int profileId, FilterCollection filters, SortCollection sort)
        {
            ZnodeLogging.LogMessage("DSODE-97-PaymentAgent GetPaymentSettingListFromCache start " + profileId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            string cacheKey = WebStoreConstants.PaymentSettingListCacheKey + "_" + Convert.ToString(PortalAgent.CurrentPortal.PortalId) + "_" + Convert.ToString(Helper.GetProfileId().GetValueOrDefault());
            List<PaymentSettingModel> model = null;
            if (HelperUtility.IsNull(HttpContext.Current.Cache[cacheKey]))
            {
                ZnodeLogging.LogMessage("DSODE-97-PaymentAgent GetPaymentSettingListFromCache If start " + profileId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                //Get payment option list.
                PaymentSettingListModel paymentOptionListModel = _paymentClient.GetPaymentSettings(null, filters, sort, null, null);
                model = paymentOptionListModel.PaymentSettings?.Where(x => profileId.ToString().Contains(x.ProfileId.ToString()) || x.ProfileId == null)?.ToList();
                Helper.AddIntoCache(model, cacheKey, "CurrentPortalCacheDuration");
            }
            else
            {
                ZnodeLogging.LogMessage("DSODE-97-PaymentAgent GetPaymentSettingListFromCache else start " + profileId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                model = Helper.GetFromCache<List<PaymentSettingModel>>(cacheKey);
            }
            ZnodeLogging.LogMessage("DSODE-97-PaymentAgent GetPaymentSettingListFromCache end " + profileId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return model;
        }

        public virtual PaymentSettingModel GetPaymentSettingFromCache(int paymentSettingId, int portalId, bool isPaymentApplication = false)
        {
            string cacheKey = (WebStoreConstants.PaymentSettingCacheKey + "_" + Convert.ToString(paymentSettingId) + "_" + Convert.ToString(portalId) + "_" + Convert.ToString(isPaymentApplication)).ToLower();
            PaymentSettingModel paymentSettingModel = null;
            if (HelperUtility.IsNull(HttpContext.Current.Cache[cacheKey]))
            {
                paymentSettingModel = _paymentClient.GetPaymentSetting(paymentSettingId, isPaymentApplication, new ExpandCollection { ZnodePaymentSettingEnum.ZnodePaymentType.ToString() }, portalId);
                Helper.AddIntoCache(paymentSettingModel, cacheKey, "CurrentPortalCacheDuration");
            }
            else
            {
                paymentSettingModel = Helper.GetFromCache<PaymentSettingModel>(cacheKey);
            }
            return paymentSettingModel;
        }

        public virtual PaymentSettingModel GetPaymentSettingByPaymentCodeFromCache(string paymentCode)
        {
            string cacheKey = (WebStoreConstants.PaymentSettingByCodeCacheKey + "_" + Convert.ToString(paymentCode)).ToLower();
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Checkout Agent GetPaymentSettingByPaymentCodeFromCache =:" + cacheKey, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            PaymentSettingModel paymentSettingViewModel = null;
            if (HelperUtility.IsNull(HttpContext.Current.Cache[cacheKey]))
            {
                paymentSettingViewModel = _paymentClient.GetPaymentSettingByPaymentCode(paymentCode);
                Helper.AddIntoCache(paymentSettingViewModel, cacheKey, "CurrentPortalCacheDuration");
            }
            else
            {
                paymentSettingViewModel = Helper.GetFromCache<PaymentSettingModel>(cacheKey);
            }
            return paymentSettingViewModel;
        }
        #endregion
    }
}