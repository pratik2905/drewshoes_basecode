﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Core.Extensions;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.WebStore.Controllers
{
    [NoCacheAttribute]
    public class CheckoutController : BaseController
    {
        #region protected Read-only members
        protected readonly IUserAgent _userAgent;
        protected readonly ICheckoutAgent _checkoutAgent;
        protected readonly ICartAgent _cartAgent;
        protected readonly IPaymentAgent _paymentAgent;
        protected readonly bool IsEnableSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;
        protected readonly string TotalTableView = "_TotalTable";
        protected readonly string CheckoutReceipt = "CheckoutReciept";
        #endregion

        #region Public Constructor
        public CheckoutController(IUserAgent userAgent, ICheckoutAgent checkoutAgent, ICartAgent cartAgent, IPaymentAgent paymentAgent)
        {
            _userAgent = userAgent;
            _checkoutAgent = checkoutAgent;
            _cartAgent = cartAgent;
            _paymentAgent = paymentAgent;
        }
        #endregion
        // GET: Checkout
        public virtual ActionResult Index(bool IsSinglePage = true)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController Index start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            CartViewModel accountQuoteViewModel = null;
            // This method is call to get payment Api header as store on the view
            AjaxHeadersModel response = _checkoutAgent.GetPaymentAPIHeader();
            ViewBag.PaymentApiResponseHeader = response.Authorization;

            if (!Equals(Request.QueryString["QuoteId"], null))
            {
                accountQuoteViewModel = _cartAgent.SetQuoteCart(Convert.ToInt32(Request.QueryString["QuoteId"]));
            }

            if (!Equals(Request.QueryString["ShippingId"], null))
            {
                _cartAgent.AddEstimatedShippingIdToCartViewModel(int.Parse(Convert.ToString(Request.QueryString["ShippingId"])));
            }

            if ((User.Identity.IsAuthenticated || Convert.ToString(Request.QueryString["mode"]) == "guest"))
            {
                return _cartAgent.GetCartCount() < 1 ? RedirectToAction<HomeController>(x => x.Index()) : IsEnableSinglePageCheckout ? View("SinglePage", _checkoutAgent.GetUserDetails(accountQuoteViewModel?.UserId ?? 0)) : View("MultiStepCheckout", _checkoutAgent.GetBillingShippingAddress());
            }
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController Index end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return RedirectToAction("Login", "User", new { returnUrl = "~/checkout", isSinglePageCheckout = IsEnableSinglePageCheckout });
        }

        //Account address
        public virtual ActionResult AccountAddress(int userid = 0, int quoteId = 0, int addressId = 0, string addressType = "", bool isAddressFromSession = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController AccountAddress start " + userid, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (quoteId > 0)
                return PartialView("_BillingShippingAddress", _checkoutAgent.GetBillingShippingAddress(userid, quoteId > 0, addressType, addressId, 0, false, false, false));
            else
            {
                return PartialView("_BillingShippingAddress", _checkoutAgent.GetBillingShippingAddress(userid, isAddressFromSession, addressType, addressId, 0, false, false, false));
            }
        }

        public virtual ActionResult RefreshAddressOptions(string addressType, bool isCalculateCart = true)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController RefreshAddressOptions start " + addressType, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            // we are refreshing alternate dropdown that ' why we set billing and shipping
            ViewData["addressType"] = addressType == "billing" ? "shipping" : "billing";
            return PartialView("_AddressOptions", _checkoutAgent.GetBillingShippingAddress(0, false, string.Empty, 0, 0, false, false, isCalculateCart));
        }


        //Get Shipping options
        public virtual ActionResult ShippingOptions(bool isQuote = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController ShippingOptions", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return CheckoutActionView("_ShippingOptions", _checkoutAgent.GetShippingOptions(null, isQuote));
        }

        //get Payment options
        public virtual ActionResult PaymentOptions(bool isQuote = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-PaymentOptions start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            var model = _checkoutAgent.PaymentOptions();
            ViewData["isQuote"] = isQuote;
            ZnodeLogging.LogMessage("DSODE-97-PaymentOptions end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return CheckoutActionView("_PaymentOptions", model);
        }

        //Get logged in user cart to review
        public virtual ActionResult CartReview(int? shippingOptionId, int? shippingAddressId, string shippingCode, string additionalInstruction = "", bool isQuoteRequest = false, bool isCalculateCart = true)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController CartReview " + shippingCode, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return PartialView("_CartReview", _cartAgent.CalculateShipping(shippingOptionId.GetValueOrDefault(), shippingAddressId.GetValueOrDefault(), shippingCode, additionalInstruction, isQuoteRequest, isCalculateCart));
        }

        //get promossion and coupons 
        [HttpPost]
        //[ValidateAntiForgeryToken] -- For future use.
        public virtual ActionResult ApplyDiscount(string discountCode, bool isGiftCard = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController ApplyDiscount " + discountCode, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            CartViewModel cartViewModel = _cartAgent.ApplyDiscount(discountCode, isGiftCard);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController ApplyDiscount  " + cartViewModel?.OrderStatus, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            string totalView = RenderRazorViewToString(TotalTableView, cartViewModel);
            return Json(new
            {
                html = totalView,
                coupons = cartViewModel.Coupons,
                vouchers = cartViewModel.Vouchers,
                giftCardCode = discountCode,
                isGiftCard = isGiftCard,
                message = cartViewModel.SuccessMessage,
                freeshipping = !isGiftCard ? cartViewModel.FreeShipping : false,
                encryptedTotalAmount = Encryption.EncryptPaymentToken(Convert.ToString(cartViewModel?.Total).Replace(",", "."))
            }, JsonRequestBehavior.AllowGet);
        }

        //Get address by address id while change address
        public virtual ActionResult GetAddress(int AddressId, int OtherAddressId, string addressType = null)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetAddress start AddressId:- " + AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            AddressViewModel model = _userAgent.GetAddress(AddressId);
            if (HelperUtility.IsNull(model))
            {
                //if model is null then associate it by checking if it's associated to the order 
                //It will handle editing one time address
                AddressListViewModel addressListModel = _checkoutAgent.GetBillingShippingAddress(addressType, AddressId, OtherAddressId);
                model = addressListModel?.AddressList
                                        ?.FirstOrDefault(o => o.AddressId == AddressId);
            }
            ViewBag.AddressType = addressType;

            //set countries for address
            if (HelperUtility.IsNull(model))
            {
                model = new AddressViewModel();
            }

            model.OtherAddressId = OtherAddressId;
            model.Countries = _userAgent.GetCountries();
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetAddress end AddressId:- " + AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return PartialView("_EditAddress", model);
        }

        public virtual ActionResult EditAddress(int AddressId, int otherAddressId, string type, string mode = null, int UserId = 0, bool IsFromEdit = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController EditAddress start" + AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            //Address type
            ViewBag.AddressType = type;
            ViewBag.HideDisplayName = (mode == "guest") || !(HttpContext.Request.LogonUserIdentity.IsAuthenticated) ? true : false;
            //Get address list
            AddressListViewModel model = _checkoutAgent.GetBillingShippingAddress(type, AddressId, otherAddressId, UserId, false, IsFromEdit);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController EditAddress end" + AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return PartialView("_AccountAddress", model);
        }

        //Change address Billing and shipping.
        public virtual ActionResult ChangeAddress(int AddressId, int otherAddressId, string type, string mode = null)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController ChangeAddress start AddressId:- " + AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            //Address type
            ViewBag.AddressType = type;
            ViewBag.HideDisplayName = (mode == "guest") || !(HttpContext.Request.LogonUserIdentity.IsAuthenticated) ? true : false;
            //Get address list
            AddressListViewModel model = _checkoutAgent.GetBillingShippingAddress(type, AddressId, otherAddressId);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController ChangeAddress end AddressId:- " + AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return PartialView("_AccountAddress", model);
        }

        //Update address if user change address at checkout time.
        public virtual ActionResult UpdateAddress(AddressViewModel model, bool formChange, string addressType)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController UpdateAddress AddressId:- " + model?.AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (formChange)
            {
                ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController UpdateAddress if AddressId:- " + model?.AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                AddressListViewModel allAddresses = new AddressListViewModel();
                if (!HttpContext.User.Identity.IsAuthenticated)
                {
                    Helper.ClearCache($"UserAccountAddressList");
                    SessionHelper.RemoveDataFromSession(WebStoreConstants.UserAccountKey);
                    if (HelperUtility.IsNotNull(addressType))
                    {
                        model.IsDefaultShipping = Equals(addressType, WebStoreConstants.ShippingAddressType);
                        model.IsDefaultBilling = Equals(addressType, WebStoreConstants.BillingAddressType);
                        model.UseSameAsShippingAddress = model.IsBothBillingShipping;
                    }
                }
                else
                {
                    allAddresses = _userAgent.GetAddressList();

                    if (HelperUtility.IsNull(allAddresses?.AddressList) || allAddresses.AddressList.Count < 1)
                    {
                        if (HelperUtility.IsNotNull(addressType))
                        {
                            model.IsDefaultShipping = Equals(addressType, WebStoreConstants.ShippingAddressType);
                            model.IsDefaultBilling = Equals(addressType, WebStoreConstants.BillingAddressType);
                        }
                        else
                        {
                            model.IsDefaultShipping = true;
                            model.IsDefaultBilling = true;
                        }
                    }
                }
                if (Equals(addressType?.ToLower(), WebStoreConstants.BillingAddressType))
                {
                    model.OtherAddressId = Convert.ToInt32(allAddresses?.ShippingAddress?.AddressId);
                }
                else if (Equals(addressType?.ToLower(), WebStoreConstants.ShippingAddressType))
                {
                    model.OtherAddressId = Convert.ToInt32(allAddresses?.BillingAddress?.AddressId);
                }
                //Update Address
                ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController UpdateAddress AddressId:- " + model?.AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                AddressViewModel resultModel = _userAgent.CreateUpdateAddress(model, addressType);

                if (!string.IsNullOrEmpty(resultModel.ErrorMessage))
                    return Json(new { status = true, error = resultModel.ErrorMessage, addressType = addressType }, JsonRequestBehavior.AllowGet);

                //Set New billing shipping address
                if (resultModel?.AddressId > 0)
                {
                    _checkoutAgent.SetBillingShippingAddress(resultModel.AddressId, model.OtherAddressId, resultModel.UserId, addressType);
                }
                ZnodeLogging.LogMessage("DSODE-97-CheckoutController UpdateAddress end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                return Json(new { status = resultModel.HasError, error = resultModel.ErrorMessage, addressType = addressType }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController UpdateAddress else AddressId:- " + model?.AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                //Set New billing shipping address
                _checkoutAgent.SetBillingShippingAddress(model.AddressId, model.OtherAddressId, model.UserId, addressType);
                return Json(new { status = false, error = "", addressType = addressType }, JsonRequestBehavior.AllowGet);
            }
        }

        public virtual ActionResult UpdateSearchAddress(AddressViewModel addressViewModel)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController UpdateSearchAddress  " + addressViewModel?.AddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            AddressViewModel addViewModel = _checkoutAgent.UpdateSearchAddress(addressViewModel);
            string htmlContent = string.Empty;

            return Json(new
            {
                html = htmlContent,
            }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult DisplayShippingAddress(int userId = 0)
        {
            return PartialView("_DisplayAddress", _checkoutAgent.GetBillingShippingAddress(userId, false, null, 0, 0).ShippingAddress);
        }


        //TODO
        public virtual ActionResult GetPaymentProvider(string paymentType, int paymentSettingId)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetPaymentProvider paymentSettingId:- " + paymentSettingId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            switch (paymentType)
            {
                case "purchase_order":
                    return ActionView("_PurchaseOrder", _paymentAgent.GetPaymentDetails(paymentSettingId));
                default:
                    break;
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetPaymentProvider", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return View();
        }

        //Submit order (work with both checkout Single/Multistep check out)
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public virtual ActionResult SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller Started:- " + ZnodeConstant.BrowserRefresh + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (!TempData.ContainsKey(ZnodeConstant.BrowserRefresh))
            {
                TempData.Add(ZnodeConstant.BrowserRefresh, true);
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller _checkoutAgent.SubmitOrder started:- " + ZnodeConstant.BrowserRefresh + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                OrdersViewModel order = _checkoutAgent.SubmitOrder(submitOrderViewModel);
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller _checkoutAgent.SubmitOrder Ended:- " + order.HasError + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                if (HelperUtility.IsNotNull(order) && !order.HasError)
                {
                    order.Total = submitOrderViewModel.Total;
                    order.SubTotal = submitOrderViewModel.SubTotal;
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller _PayPalReturnUrl:- " + submitOrderViewModel?.PayPalReturnUrl + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    // Below code is used for "PayPal Express" to redirect payment website.
                    if (!string.IsNullOrEmpty(submitOrderViewModel.PayPalReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.PayPalCancelUrl) && HelperUtility.Equals(submitOrderViewModel.PaymentType?.ToLower(), ZnodeConstant.PayPalExpress.ToLower()))
                    {
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller _PayPalReturnUrl Error Message:- " + order.ErrorMessage + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        TempData["Error"] = order.HasError;
                        TempData["ErrorMessage"] = order.ErrorMessage;
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller PayPalExpressResponseText:- " + order?.PayPalExpressResponseText + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        return Json(new { responseText = order.PayPalExpressResponseText });
                    }

                    // Below code is used for "Amazon Pay" to redirect payment website.
                    if (!string.IsNullOrEmpty(submitOrderViewModel.AmazonPayReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.AmazonPayCancelUrl) && HelperUtility.Equals(submitOrderViewModel.PaymentType?.ToLower(), ZnodeConstant.AmazonPay.ToLower()))
                    {
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller AmazonPayReturnUrl:- " + order?.TrackingNumber + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        return Json(new { responseText = Convert.ToString(order.PaymentStatus), responseToken = order.TrackingNumber });
                    }
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller IsFromPayPalExpress:- " + submitOrderViewModel?.IsFromPayPalExpress + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    // Below code is used, after payment success from "PayPal Express" return to "return url" of "PayPal Express" i.e. "SubmitPaypalOrder".
                    if (submitOrderViewModel.IsFromPayPalExpress)
                    {
                        TempData["Order"] = order;
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller payment success from PayPal Express:- " + order + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { sucess = true });
                    }
                    // Below code is used, after payment success from "Amazon Pay" return to "return url" of "AmazonPay" i.e. "SubmitAmazonPayOrder".
                    if (submitOrderViewModel.IsFromAmazonPay)
                    {
                        TempData["Order"] = order;
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller IsFromAmazonPay:- " + order + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { sucess = true });
                    }

                    if (!User.Identity.IsAuthenticated)
                    {
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller IsAuthenticated:- " + User?.Identity?.IsAuthenticated + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        _userAgent.RemoveGuestUserSession();
                    }

                    _cartAgent.RemoveAllCartItems(order.OmsOrderId);

                    // Below code is used, for after successfully payment from "Credit Card" return receipt.
                    if (Equals(submitOrderViewModel?.PaymentType?.ToLower(), ZnodeConstant.CreditCard.ToLower()))
                    {
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller PaymentType:- " + submitOrderViewModel?.PaymentType?.ToLower() + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        return Json(new { receiptHTML = RenderRazorViewToString(CheckoutReceipt, order), omsOrderId = order.OmsOrderId });
                    }

                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    TempData["OrderId"] = order.OmsOrderId;

                    return RedirectToAction<CheckoutController>(x => x.OrderCheckoutReceipt());
                }

                /*Nivi New*/
                if (order.HasError)
                {
                    TempData["Error"] = order.HasError;
                    TempData["ErrorMessage"] = order?.ErrorMessage;
                }

                // Return error message, if payment through "Credit Card" raises any error.
                if (Equals(submitOrderViewModel?.PaymentType?.ToLower(), ZnodeConstant.CreditCard.ToLower()))
                {
                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller PaymentType Credit Card:- " + submitOrderViewModel?.PaymentType?.ToLower() + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    return Json(new { error = order.ErrorMessage });
                }

                // Return error message, if payment through "PayPal Express" raises any error.
                if (!string.IsNullOrEmpty(submitOrderViewModel.PayPalReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.PayPalCancelUrl))
                {
                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller PayPal Express:- " + submitOrderViewModel?.PayPalReturnUrl + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    return Json(new { error = order.ErrorMessage, responseText = order.PaymentStatus });
                }

                // Return error message, if payment through "AmazonPay" raises any error.
                if (!string.IsNullOrEmpty(submitOrderViewModel.AmazonPayReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.AmazonPayCancelUrl))
                {
                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller AmazonPay:- " + submitOrderViewModel?.AmazonPayCancelUrl + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    return Json(new { error = order.ErrorMessage, responseText = order.PaymentStatus });
                }
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller Order Error Message Credit Card:- " + order.ErrorMessage + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                SetNotificationMessage(GetErrorNotificationMessage(order.ErrorMessage));
            }
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder Controller Ended:- " + ZnodeConstant.BrowserRefresh + " OrderNo:- " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            TempData.Remove(ZnodeConstant.BrowserRefresh);
            return RedirectToAction<CheckoutController>(x => x.Index(true));
        }

        [HttpGet]
        public virtual ActionResult SubmitOrder(OrdersViewModel order)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitOrder start " + order?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (!Equals(order, null) && order.OmsOrderId > 0)
            {
                ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitOrder If" + order?.OmsOrderId + " OrderNo:- " + order?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                return ActionView("CheckoutReciept", order);
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitOrder end orderNo:- " + order?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return RedirectToAction<HomeController>(x => x.Index());

        }

        //Remove applied coupon.
        [HttpGet]
        public virtual ActionResult RemoveCoupon(string couponCode)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController RemoveCoupon start" + couponCode, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            CartViewModel cartViewModel = _cartAgent.RemoveCoupon(couponCode);
            cartViewModel.ErrorMessage = string.IsNullOrEmpty(couponCode) ? WebStore_Resources.RequiredCouponCode : WebStore_Resources.ErrorCouponCode;
            string totalView = RenderRazorViewToString(TotalTableView, cartViewModel);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController RemoveCoupon end" + couponCode, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(new
            {
                html = totalView,
                coupons = cartViewModel.Coupons,
                giftCardCode = couponCode,
                isGiftCard = false,
                message = cartViewModel.SuccessMessage,
                encryptedTotalAmount = Encryption.EncryptPaymentToken(Convert.ToString(cartViewModel?.Total).Replace(",", ".")),
                isShippingBasedCoupon = cartViewModel.IsShippingBasedCoupon
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetPaymentDetails(int paymentSettingId)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController GetPaymentDetails start " + paymentSettingId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(_paymentAgent.GetPaymentDetails(paymentSettingId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetPaymentDetailsForQuotes(int paymentSettingId, string quoteNumber)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController GetPaymentDetailsForQuotes start " + quoteNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(_paymentAgent.GetPaymentDetails(paymentSettingId, quoteNumber), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetPaymentCreditCardDetails(string customerGUID)
        {
            ZnodeLogging.LogMessage("DSODE-97-CheckoutController GetPaymentCreditCardDetails start " + customerGUID, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(_paymentAgent.GetPaymentCreditCardDetails(customerGUID), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetSaveCreditCardCount(string customerGUID)
        {
            return Json(!string.IsNullOrEmpty(customerGUID) ? _paymentAgent.GetSaveCreditCardCount(customerGUID) : 0, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetBillingAddressDetail(int portalId, int billingAddressId = 0, int shippingAddressId = 0)
            => Json(new
            {
                data = _userAgent.GetBillingAddressDetail(billingAddressId, shippingAddressId),
                orderNumber = _checkoutAgent.GenerateOrderNumber(portalId)
            }, JsonRequestBehavior.AllowGet);

        [HttpGet]
        public virtual ActionResult SubmitPaypalOrder(int shippingAddressId, int billingAddressId, int shippingOptionId, int paymentSettingId, string additionalInstruction, string token, string paymentCode, string orderNumber, string inHandDate = "", string jobName = "", string shippingConstraintCode = "")
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitPaypalOrder start " + shippingAddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (string.IsNullOrEmpty(token))
            {
                SetNotificationMessage(GetErrorNotificationMessage(WebStore_Resources.ErrorUnablePlaceOrder));
                return RedirectToAction<CheckoutController>(x => x.Index(true));
            }

            SubmitOrderViewModel submitOrderViewModel = _checkoutAgent.SetPayPalToken(token, shippingAddressId, billingAddressId, shippingOptionId, paymentSettingId, additionalInstruction, paymentCode, orderNumber, inHandDate, jobName, shippingConstraintCode);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController submitOrderViewModel " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            TempData.Remove(ZnodeConstant.BrowserRefresh);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitOrder start " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            var status = SubmitOrder(submitOrderViewModel);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitOrder end " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            //Added PayPalToken and Identity in tempdata to be used in further action method.

            bool hasError = HelperUtility.IsNotNull(TempData["Error"]) ? Convert.ToBoolean(TempData["Error"]) : false;
            string ErrorMessage = HelperUtility.IsNotNull(TempData["ErrorMessage"]) ? Convert.ToString(TempData["ErrorMessage"]) : null;
            OrdersViewModel order = (OrdersViewModel)TempData["Order"];
            if (!hasError)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    _userAgent.RemoveGuestUserSession();
                }

                _cartAgent.RemoveAllCartItems();

                TempData["OrderId"] = (order?.OmsOrderId).GetValueOrDefault();
            }

            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                SetNotificationMessage(GetErrorNotificationMessage(ErrorMessage));
            }

            int omsOrderId = (order?.OmsOrderId).GetValueOrDefault();
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SubmitPaypalOrder end " + submitOrderViewModel?.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return hasError ? RedirectToAction<CheckoutController>(x => x.Index(true)) : RedirectToAction<CheckoutController>(x => x.OrderCheckoutReceipt());
        }


        public virtual ActionResult OrderCheckoutReceipt()
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController OrderCheckoutReceipt start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            int omsOrderId = Convert.ToInt32(_checkoutAgent.GetOrderIdFromCookie());
            if (omsOrderId > 0)
            {
                ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController OrderCheckoutReceipt if block", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                GetService<IWebstoreHelper>().SaveDataInCookie(WebStoreConstants.UserOrderReceiptOrderId, null, 1);

                OrdersViewModel order = _checkoutAgent.GetOrderViewModel(omsOrderId);
                return HelperUtility.IsNotNull(order) ? View(CheckoutReceipt, order) : RedirectToAction<HomeController>(x => x.Index());
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController OrderCheckoutReceipt end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return RedirectToAction<HomeController>(x => x.Index());
        }

        //to get locale wise amount
        protected virtual string GetLocaleWiseAmount(decimal amount)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetLocaleWiseAmount start " + amount, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            string formattedAmount = Convert.ToString(amount);
            if (!string.IsNullOrEmpty(formattedAmount) && formattedAmount.Contains(","))
            {
                formattedAmount = formattedAmount.Replace(",", ".");
            }

            return formattedAmount;
        }

        //Quote receipt after successful quote creation.
        public virtual ActionResult QuoteReceipt(int quoteId, bool isPendingPayment = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController QuoteReceipt start " + quoteId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            ViewData["QuoteId"] = quoteId;
            ViewData["IsPendingPayment"] = isPendingPayment;

            if (!User.Identity.IsAuthenticated)
            {
                _userAgent.RemoveGuestUserSession();
                return RedirectToAction<HomeController>(x => x.Index());
            }

            if (quoteId > 0)
            {
                AccountQuoteViewModel accountQuoteViewModel = _userAgent.GetQuoteView(quoteId);
                return HelperUtility.IsNotNull(accountQuoteViewModel) ? View("_QuoteReceipt", accountQuoteViewModel) : RedirectToAction<HomeController>(x => x.Index());
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController QuoteReceipt end " + quoteId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return RedirectToAction<UserController>(x => x.QuoteHistory(null));
        }

        //Get Ajax headers.
        public virtual JsonResult GetAjaxHeaders()
        {
            AjaxHeadersModel response = _checkoutAgent.GetAppHeader();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //Get Payment App Header
        public virtual JsonResult GetPaymentAppHeader()
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetPaymentAppHeader start ", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            AjaxHeadersModel response = _checkoutAgent.GetPaymentAPIHeader();
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetPaymentAppHeader end ", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(new
            {
                response.Authorization
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetshippingBillingAddress(int portalId, int shippingId = 0, int billingId = 0)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetshippingBillingAddress start " + portalId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            AddressListViewModel addressListViewModel = _userAgent.GetshippingBillingAddress();
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetshippingBillingAddress end " + portalId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(new
            {
                Shipping = _checkoutAgent.SetAddressByAddressType(WebStoreConstants.ShippingAddressType, shippingId, addressListViewModel),
                Billing = _checkoutAgent.SetAddressByAddressType(WebStoreConstants.BillingAddressType, billingId, addressListViewModel),
                orderNumber = _checkoutAgent.GenerateOrderNumber(portalId)
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //Get list of search billing locations.
        public JsonResult GetSearchBillingLocation(string query)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetSearchBillingLocation start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(_checkoutAgent.GetSearchLocation(query, PortalAgent.CurrentPortal.PortalId, WebStoreConstants.BillingAddressType), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //Get list of search shipping locations.
        public JsonResult GetSearchShippingLocation(string query)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetSearchShippingLocation start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(_checkoutAgent.GetSearchLocation(query, PortalAgent.CurrentPortal.PortalId, WebStoreConstants.ShippingAddressType), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //Set recipient name in the respective address type of cart.
        public JsonResult SetAddressReceipentNameInCart(string firstName, string lastName, string addressType)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController SetAddressReceipentNameInCart start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(_checkoutAgent.SetAddressRecipientNameInCart(firstName, lastName, addressType), JsonRequestBehavior.AllowGet);
        }

        //Get address details on the basis of address id.
        public JsonResult GetAddressById(int addressId, string addressType = "", bool isCalculateCart = true)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetAddressById start " + addressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (addressId > 0)
            {
                AddressViewModel model = _checkoutAgent.GetAddressById(addressId, addressType, isCalculateCart);
                model.Countries = _userAgent.GetCountries();

                string address = _checkoutAgent.CheckAndAppendAlternateAddress(model);

                if (PortalAgent.CurrentPortal.EnableAddressValidation)
                {
                    AddressListViewModel listViewModel = _checkoutAgent.GetRecommendedAddressList(model);

                    string htmlContent = string.Empty;
                    if (listViewModel?.AddressList?.Count > 0)
                    {
                        htmlContent = RenderRazorViewToString("../Checkout/_RecommendedAddress", listViewModel);
                    }

                    var htmlString = string.Format(WebStore_Resources.AddressHTML, model.DisplayName, model.Address1, address, model.CityName, model.StateName, model.PostalCode);
                    return Json(new
                    {
                        html = htmlString,
                        htmlContent = htmlContent,
                        model = model
                    }
                  , JsonRequestBehavior.AllowGet);
                }

                var html = string.Format(WebStore_Resources.AddressHTML, model.Address1, model.Address2, address, model.CityName, model.StateName, model.PostalCode);
                return Json(new
                {
                    html = html,
                    model = model
                }
              , JsonRequestBehavior.AllowGet);
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetAddressById end " + addressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(new JsonResult(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateOrderNumber(int portalId)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GenerateOrderNumber start portalId" + portalId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(new { orderNumber = _checkoutAgent.GenerateOrderNumber(portalId) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateNewAddress(int AddressId, int otherAddressId, string type, string mode = null, bool isNewAddress = false)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController CreateNewAddress start " + otherAddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            //Address type
            ViewBag.AddressType = type;
            ViewBag.IsNewAddress = isNewAddress;
            ViewBag.HideDisplayName = (mode == "guest") || !(HttpContext.Request.LogonUserIdentity.IsAuthenticated) ? true : false;

            //Get address list
            AddressViewModel model = _userAgent.GetAddressByAddressType(AddressId, type);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController CreateNewAddress end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return PartialView("_CreateAddress", model);
        }


        #region Amazon Payment

        //Get AmazonPay address.
        [HttpGet]
        public virtual ActionResult GetAddressDetails(int? shippingOptionId = 0, int? shippingAddressId = 0, string shippingCode = "", int paymentSettingId = 0, string paymentCode = "", string clientId = "", string sellerId = "", bool testMode = true, string access_token = null)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetAddressDetails start shippingAddressId " + shippingAddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            ViewData["ShippingOptionId"] = shippingOptionId;
            ViewData["ShippingAddressId"] = shippingAddressId;
            ViewData["ShippingCode"] = shippingCode;
            ViewData["PaymentSettingId"] = paymentSettingId;
            ViewData["PaymentCode"] = paymentCode;
            ViewData["ClientId"] = clientId;
            ViewData["SellerId"] = sellerId;
            ViewData["TestMode"] = testMode;
            ViewData["AccessToken"] = access_token;
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController GetAddressDetails end shippingAddressId " + shippingAddressId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return View("_AmazonPayAddress", _cartAgent.CalculateShipping(0, 0, ""));
        }

        //Submit Amazon Pay Order.
        [HttpGet]
        public virtual ActionResult SubmitAmazonOrder(string amazonOrderReferenceId = "", string paymentType = "", int shippingOptionId = 0, int paymentSettingId = 0, string paymentCode = "", string additionalInstruction = "", string captureId = "", string orderNumber = "", string inHandDate = "", string jobName = "", string shippingConstraintCode = "")
        {
            SubmitOrderViewModel submitOrderViewModel = _checkoutAgent.SetAmazonPayDetails(amazonOrderReferenceId, paymentType, shippingOptionId, paymentSettingId, paymentCode, additionalInstruction, captureId, orderNumber, inHandDate, jobName, shippingConstraintCode);
            TempData.Remove(ZnodeConstant.BrowserRefresh);
            var status = SubmitOrder(submitOrderViewModel);
            if (!User.Identity.IsAuthenticated)
            {
                _userAgent.RemoveGuestUserSession();
            }

            _cartAgent.RemoveAllCartItems();
            OrdersViewModel order = (OrdersViewModel)TempData["Order"];
            Response.Cookies["amazon_Login_state_cache"].Expires = DateTime.Now.AddDays(-1);
            TempData["OrderId"] = (order?.OmsOrderId).GetValueOrDefault();
            int omsOrderId = (order?.OmsOrderId).GetValueOrDefault();
            return HelperUtility.IsNull(status) ? RedirectToAction<CheckoutController>(x => x.Index(true)) : RedirectToAction<CheckoutController>(x => x.OrderCheckoutReceipt());
        }

        //Get AmazonPay cart.
        public virtual ActionResult AmazonCartReview(int? shippingOptionId, int? shippingAddressId, string shippingCode, string amazonOrderReferenceId, int paymentSettingId, string total)
        {
            AddressViewModel address = _checkoutAgent.GetAmazonAddress(paymentSettingId, amazonOrderReferenceId, total);
            return PartialView("_CartReview", _cartAgent.CalculateAmazonShipping(shippingOptionId.GetValueOrDefault(), shippingAddressId.GetValueOrDefault(), shippingCode, address));
        }

        //Get Amazon Pay shipping option.
        public virtual ActionResult AmazonShippingOptions(string amazonOrderReferenceId, int paymentSettingId, string total, string accesstoken)
        {
            return CheckoutActionView("AmazonShippingOption", _checkoutAgent.GetAmazonShippingOptions(amazonOrderReferenceId, paymentSettingId, total, null, accesstoken));
        }

        //Get payment options.
        public virtual ActionResult AmazonPaymentOptions(bool isQuote = false)
        {
            ViewData["isQuote"] = isQuote;
            return CheckoutActionView("_AmazonPay", _checkoutAgent.AmazonPaymentSetting());
        }

        //Remove applied Voucher.
        [HttpGet]
        public virtual ActionResult RemoveVoucher(string voucherNumber)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController RemoveVoucher start " + voucherNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            CartViewModel cartViewModel = _cartAgent.RemoveVoucher(voucherNumber);
            cartViewModel.ErrorMessage = string.IsNullOrEmpty(voucherNumber) ? WebStore_Resources.RequiredCouponCode : WebStore_Resources.ErrorCouponCode;
            string totalView = RenderRazorViewToString(TotalTableView, cartViewModel);
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController RemoveVoucher end " + voucherNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return Json(new
            {
                html = totalView,
                coupons = cartViewModel.Coupons,
                giftCardCode = voucherNumber,
                vouchers = cartViewModel.Vouchers,
                isGiftCard = true,
                message = cartViewModel.SuccessMessage,
                encryptedTotalAmount = Encryption.EncryptPaymentToken(Convert.ToString(cartViewModel?.Total).Replace(",", "."))
            }, JsonRequestBehavior.AllowGet);
        }

        //Set mode wise result 
        protected virtual ActionResult CheckoutActionView(string viewName, object model)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController CheckoutActionView1 start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (IsEnableSinglePageCheckout)
            {
                return PartialView(viewName, model);
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController CheckoutActionView1 end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return View(viewName.Remove(0, 1), model);
        }

        //Set mode wise result 
        protected virtual ActionResult CheckoutActionView(string viewName)
        {
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController CheckoutActionView2 start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (IsEnableSinglePageCheckout)
            {
                return PartialView(viewName);
            }
            ZnodeLogging.LogMessage("DSODE-97-CreateOrder checkoutController CheckoutActionView2 end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return View(viewName.Remove(0, 1));
        }

        #endregion
    }
}