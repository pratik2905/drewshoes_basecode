﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Cache
{
    public class WidgetTemplateCache: BaseCache, IWidgetTemplateCache
    {
        #region Private Variable
        private readonly IWidgetTemplateService _service;
        #endregion

        #region Constructor
        public WidgetTemplateCache(IWidgetTemplateService widgetTemplateService)
        {
            _service = widgetTemplateService;
        }
        #endregion

        public virtual string List(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                WidgetTemplateListModel list = _service.GetWidgetTemplateList(Expands, Filters, Sorts, Page);
                if (list?.WidgetTemplates?.Count > 0)
                {
                    WidgetTemplateListResponse response = new WidgetTemplateListResponse { WidgetTemplates = list.WidgetTemplates };
                    response.MapPagingDataFromModel(list);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                    return data;
                }
            }
            return data;
        }

        //Get Widget Template
        public virtual string GetWidgetTemplate(string templateCode, string routeUri, string routeTemplate)
        {
            //Get data from Cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response.
                WidgetTemplateModel model = _service.GetWidgetTemplate(templateCode);
                if (HelperUtility.IsNotNull(model))
                {
                    WidgetTemplateListResponse response = new WidgetTemplateListResponse { widgetTemplate = model };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}
