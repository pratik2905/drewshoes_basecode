﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Api.Cache
{
    class AnalyticsCache : BaseCache, IAnalyticsCache
    {
        #region Private Variable
        private readonly IAnalyticsService _service;
        #endregion

        #region Public Constructor
        public AnalyticsCache(IAnalyticsService service)
        {
            _service = service;
        }
        #endregion

        #region Public Methods
        //Gets analytics dashboard data
        public virtual string GetAnalyticsDashboardData(string routeUri, string routeTemplate)
        {
            //Get data from cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                AnalyticsModel analyticsDashboardData = _service.GetAnalyticsDashboardData();
                if (IsNotNull(analyticsDashboardData))
                    data = InsertIntoCache(routeUri, routeTemplate, new AnalyticsResponse { AnalyticsDetails = analyticsDashboardData });
            }
            return data;
        }

        //Gets analytics JSON key
        public virtual string GetAnalyticsJSONKey(string routeUri, string routeTemplate)
        {
            //Get data from cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                string analyticsDetails = _service.GetAnalyticsJSONKey();
                if (IsNotNull(analyticsDetails))
                    data = InsertIntoCache(routeUri, routeTemplate, new AnalyticsResponse { AnalyticsJSONKey = analyticsDetails });
            }
            return data;
        }
        #endregion
    }
}
