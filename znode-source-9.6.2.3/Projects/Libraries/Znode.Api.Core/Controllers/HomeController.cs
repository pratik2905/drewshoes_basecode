﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Services.Helper;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ZnodeLicenseManager licenseMgr = new ZnodeLicenseManager();
            licenseMgr.Validate();

            if (Equals(licenseMgr.LicenseType, ZnodeLicenseType.Invalid))
            {
                return RedirectToAction("Index", "Activate", new { area = "Activate" });
            }

            if (!Equals(HttpContext.ApplicationInstance.Context.AllErrors, null) && HttpContext.ApplicationInstance.Context.AllErrors.Length > 0)
                return new EmptyResult();
            return View();
        }


        public virtual void ClearApiCache()
        {
            List<string> cacheItemsNotToRemove = ClearCacheHelper.CacheItemNotToRemove();
            //Get API Cache
            IDictionaryEnumerator cacheEnumerator = HttpRuntime.Cache.GetEnumerator();
            //Clear all cached items.
            try
            {
                while (cacheEnumerator.MoveNext())
                {
                    if (cacheEnumerator.Key != null && !cacheItemsNotToRemove.Contains(cacheEnumerator.Key.ToString()))
                        HttpRuntime.Cache.Remove(cacheEnumerator.Key.ToString());
                }
                //Insert cache refresh time into database after refreshing API cache
                ClearCacheHelper.InsertCacheRefreshTimeIntoDatabase(ApplicationCacheTypeEnum.ApiCache.ToString());
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Failed to clear api cache", "CacheLog");
                ZnodeLogging.LogMessage(ex.Message, "CacheLog", TraceLevel.Error, ex);
            }
        }

        public virtual void ClearApiCacheByKey(string key)
        {
            List<string> cacheItemsNotToRemove = ClearCacheHelper.CacheItemNotToRemove();
            //Get API Cache
            IDictionaryEnumerator cacheEnumerator = HttpRuntime.Cache.GetEnumerator();

            ClearCacheHelper.CacheConfig = (CacheConfiguration)ConfigurationManager.GetSection("ZnodeApiCache");

            string template = ClearCacheHelper.GetTemplate(key);

            //Clear  cached items.
            try
            {
                while (cacheEnumerator.MoveNext())
                {
                    if (cacheEnumerator.Key != null)
                    {
                        if (!cacheItemsNotToRemove.Contains(cacheEnumerator.Key.ToString()))
                        {
                            if (cacheEnumerator.Key.ToString().Contains(template))
                                HttpRuntime.Cache.Remove(cacheEnumerator.Key.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Failed to clear api cache", "CacheLog");
                ZnodeLogging.LogMessage(ex.Message, "CacheLog", TraceLevel.Error, ex);
            }
        }
    }
}
