﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{

    public class WidgetTemplateController : BaseController
    {

        #region Private Variables
        private readonly IWidgetTemplateCache _cache;
        private readonly IWidgetTemplateService _service;
        #endregion

        #region Constructor
        public WidgetTemplateController(IWidgetTemplateService service)
        {
            _service = service;
            _cache = new WidgetTemplateCache(_service);
        }
        #endregion

        /// <summary>
        /// Get the List of Widget Template
        /// </summary>
        /// <returns>WidgetTemplateListResponse model</returns>
        [ResponseType(typeof(WidgetTemplateListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.List(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<WidgetTemplateListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Create Widget Template
        /// </summary>
        /// <param name="model">WidgetTemplateCreateModel model</param>
        /// <returns>WidgetTemplateModel model</returns>
        [ResponseType(typeof(WidgetTemplateListResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage Create([FromBody] WidgetTemplateCreateModel model)
        {
            HttpResponseMessage response;
            try
            {
                WidgetTemplateModel widgetModel = _service.CreateWidgetTemplate(model);
                response = !Equals(widgetModel, null) ? CreateCreatedResponse(new WidgetTemplateListResponse { widgetTemplate = widgetModel }) : CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                WidgetTemplateListResponse data = new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                WidgetTemplateListResponse data = new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get Widget Template
        /// </summary>
        /// <param name="templateCode">tempateCode</param>
        /// <returns>WidgetTemplateModel model</returns>
        [ResponseType(typeof(WidgetTemplateListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage Get(string templateCode)
        {
            HttpResponseMessage response;

            try
            {
                //Get template by id.
                string data = _cache.GetWidgetTemplate(templateCode, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<TemplateListResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Update Widget Template
        /// </summary>
        /// <param name="model">WidgetTemplateUpdateModel model</param>
        /// <returns>WidgetTemplateModel model</returns>
        [ResponseType(typeof(WidgetTemplateListResponse))]
        [HttpPut, ValidateModel]
        public virtual HttpResponseMessage Update([FromBody] WidgetTemplateUpdateModel model)
        {
            HttpResponseMessage response;
            try
            {
                WidgetTemplateModel widgetTemplate = _service.UpdateWidgetTemplate(model);
                response = !Equals(widgetTemplate, null) ? CreateCreatedResponse(new WidgetTemplateListResponse { widgetTemplate = widgetTemplate, ErrorCode = 0 }) : CreateInternalServerErrorResponse();

            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new WidgetTemplateListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Delete Widget Template
        /// </summary>
        /// <param name="WidgetTemplateIds">Widget Template Ids</param>
        /// <returns>status</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage Delete(ParameterModel WidgetTemplateIds)
        {
            HttpResponseMessage response;
            try
            {
                bool deleted = _service.DeleteWidgetTemplateById(WidgetTemplateIds);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = deleted });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Delete Widget Template
        /// </summary>
        /// <param name="templateCode">templateCode</param>
        /// <returns>status</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage DeleteTemplateByCode(string templateCode)
        {
            HttpResponseMessage response;
            try
            {
                bool deleted = _service.DeleteWidgetTemplateByCode(templateCode);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = deleted });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Validate if the Widget Template exists
        /// </summary>
        /// <param name="templateCode">Widget Template code</param>
        /// <returns>status</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpGet]
        public virtual HttpResponseMessage IsWidgetTemplateExist(string templateCode)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _service.IsWidgetTemplateExists(templateCode) });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

    }
}
