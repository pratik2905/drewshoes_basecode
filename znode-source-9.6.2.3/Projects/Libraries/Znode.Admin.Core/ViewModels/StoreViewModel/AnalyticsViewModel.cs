﻿using System;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModels
{
    public class AnalyticsViewModel : BaseViewModel
    {
        public int PortalId { get; set; }

        public TagManagerViewModel TagManager { get; set; }

        public PortalTrackingPixelViewModel TrackingPixel { get; set; }

        public string AnalyticsAccessToken { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelAnalyticsJSONKey, ResourceType = typeof(Admin_Resources))]
        public string AnalyticsJSONKey { get; set; }
    }
}
