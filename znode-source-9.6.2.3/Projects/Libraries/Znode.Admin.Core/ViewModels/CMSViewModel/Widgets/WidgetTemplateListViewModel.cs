﻿using System.Collections.Generic;
using Znode.Engine.Admin.Models;

namespace Znode.Engine.Admin.ViewModels
{
    public class WidgetTemplateListViewModel : BaseViewModel
    {
        public List<WidgetTemplateViewModel> WidgetTemplates { get; set; }
        public GridModel GridModel { get; set; }
    }
}
