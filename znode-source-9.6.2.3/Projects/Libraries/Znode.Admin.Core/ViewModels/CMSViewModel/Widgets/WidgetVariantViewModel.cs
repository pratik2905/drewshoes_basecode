﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Admin.Models;


namespace Znode.Engine.Admin.ViewModels
{
    public class WidgetVariantViewModel : BaseViewModel
    {

        public WidgetVariantViewModel()
        {
            Profiles = new List<SelectListItem>();
        }

        public List<SelectListItem> Profiles { get; set; }
        public List<SelectListItem> Locales { get; set; }

        public int? ProfileId { get; set; }
        public int LocaleId { get; set; }
        public int ContentWidgetId { get; set; }
        public string LocaleName { get; set; }
        public string ProfileName { get; set; }
        public string WidgetKey { get; set; }
        public int WidgetProfileVariantId { get; set; }
        public int? WidgetTemplateId { get; set; }

    }


}

