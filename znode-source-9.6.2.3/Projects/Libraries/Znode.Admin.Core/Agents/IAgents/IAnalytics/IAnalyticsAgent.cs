﻿using Znode.Engine.Admin.ViewModels;

namespace Znode.Engine.Admin.Agents
{
    public interface IAnalyticsAgent
    {
        /// <summary>
        /// Method To get analytics dashboard data
        /// </summary>
        /// <returns>string containing analytics access token</returns>
        AnalyticsViewModel GetAnalyticsDashboardData();
    }
}
