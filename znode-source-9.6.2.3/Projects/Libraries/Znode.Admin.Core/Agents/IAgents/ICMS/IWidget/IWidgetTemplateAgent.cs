﻿using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Agents
{
    public interface IWidgetTemplateAgent
    {
        /// <summary>
        /// List of Widget Template
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>WidgetTemplateListViewModel model</returns>
        WidgetTemplateListViewModel List(FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null);

        /// <summary>
        /// Create Widget Template
        /// </summary>
        /// <param name="widgetTemplateViewModel">WidgetTemplateViewModel model</param>
        /// <returns>WidgetTemplateViewModel</returns>
        WidgetTemplateViewModel Create(WidgetTemplateViewModel widgetTemplateViewModel);

        /// <summary>
        /// Get Widget Template
        /// </summary>
        /// <param name="widgetTemplateId">Widget Template Id</param>
        /// <returns>WidgetTemplateViewModel model</returns>
        WidgetTemplateViewModel GetWidgetTemplate(string templateCode);

        /// <summary>
        /// Update Widget Template
        /// </summary>
        /// <param name="model">WidgetTemplateViewModel model</param>
        /// <returns>WidgetTemplateViewModel model</returns>
        WidgetTemplateViewModel Update(WidgetTemplateViewModel model);

        /// <summary>
        /// Delete Widget Template
        /// </summary>
        /// <param name="widgetTemplateIds">Widget Template ids</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>status</returns>
        bool DeleteWidgetTemplate(string widgetTemplateIds, string fileName, out string errorMessage);

        /// <summary>
        /// Copy Widget Template
        /// </summary>
        /// <param name="model">WidgetTemplateViewModel model</param>
        /// <returns>WidgetTemplateViewModel model</returns>
        WidgetTemplateViewModel CopyWidgetTemplate(WidgetTemplateViewModel model);

        /// <summary>
        /// Download Widget Template
        /// </summary>
        /// <param name="widgetTemplateId">Widget Template Id</param>
        /// <param name="fileName">filename</param>
        /// <returns>status</returns>
        string DownloadWidgetTemplate(int widgetTemplateId, string fileName);

        /// <summary>
        /// validate if the Widget Template Exists
        /// </summary>
        /// <param name="name">Widget Template name</param>
        /// <returns>status</returns>
        bool IsWidgetTemplateExist(string templateCode);

    }
}
