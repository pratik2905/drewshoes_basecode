﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Agents
{
    public class WidgetTemplateAgent : BaseAgent, IWidgetTemplateAgent
    {
        #region Private Variables
        private readonly IWidgetTemplateClient _widgetTemplateClient;
        #endregion

        #region Constructor
        public WidgetTemplateAgent(IWidgetTemplateClient widgetTemplateClient)
        {
            _widgetTemplateClient = GetClient<IWidgetTemplateClient>(widgetTemplateClient);
        }
        #endregion

        //List of Widget Template
        public virtual WidgetTemplateListViewModel List(FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            WidgetTemplateListModel widgetTemplateList = _widgetTemplateClient.List(null, filters, sorts, pageIndex, pageSize);
            WidgetTemplateListViewModel listViewModel = new WidgetTemplateListViewModel { WidgetTemplates = widgetTemplateList?.WidgetTemplates.ToViewModel<WidgetTemplateViewModel>()?.ToList() };

            SetListPagingData(listViewModel, widgetTemplateList);
            //Set tool options for grid.
            SetToolMenus(listViewModel);
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return listViewModel?.WidgetTemplates?.Count > 0 ? listViewModel : new WidgetTemplateListViewModel() { WidgetTemplates = new List<WidgetTemplateViewModel>() };
        }

        //Create Widget Template
        public virtual WidgetTemplateViewModel Create(WidgetTemplateViewModel widgetTemplateViewModel)
        {
            try
            {
                ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                if (HelperUtility.IsNotNull(widgetTemplateViewModel))
                {
                    widgetTemplateViewModel.FileName = GetFileNameWithoutExtension(widgetTemplateViewModel?.FilePath?.FileName);
                    WidgetTemplateViewModel model = _widgetTemplateClient.Create(widgetTemplateViewModel.ToModel<WidgetTemplateCreateModel>())?.ToViewModel<WidgetTemplateViewModel>();
                    if (model?.WidgetTemplateId > 0)
                    {
                        SaveFile(widgetTemplateViewModel);
                        return model;
                    }
                }
                ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return widgetTemplateViewModel;
            }
            catch (ZnodeException ex)
            {
                return (WidgetTemplateViewModel)GetViewModelWithErrorMessage(widgetTemplateViewModel, Admin_Resources.ErrorFailedToCreate);
            }
            catch (Exception ex)
            {
                return (WidgetTemplateViewModel)GetViewModelWithErrorMessage(widgetTemplateViewModel, Admin_Resources.ErrorFailedToCreate);
            }
        }

        //Get Widget Template
        public virtual WidgetTemplateViewModel GetWidgetTemplate(string templateCode)
         => _widgetTemplateClient.GetWidgetTemplate(templateCode).ToViewModel<WidgetTemplateViewModel>();

        //Update Widget Template
        public virtual WidgetTemplateViewModel Update(WidgetTemplateViewModel model)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNotNull(model.FilePath))
                model.FileName = GetFileNameWithoutExtension(model?.FilePath?.FileName);

            int widgetTemplateId = _widgetTemplateClient.Update(model?.ToModel<WidgetTemplateUpdateModel>()).WidgetTemplateId;
            if (widgetTemplateId > 0)
            {
                if (HelperUtility.IsNotNull(model.FilePath))
                    SaveFile(model);
                return model;
            }
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return (WidgetTemplateViewModel)GetViewModelWithErrorMessage(model, Admin_Resources.UpdateErrorMessage);
        }

        //Delete Widget Template
        public virtual bool DeleteWidgetTemplate(string widgetTemplateIds, string fileName, out string errorMessage)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            errorMessage = string.Empty;
            try
            {
                bool status = false;
                WidgetTemplateListModel list = _widgetTemplateClient.List(null, null, null, null, null);
                if (!string.IsNullOrEmpty(widgetTemplateIds))
                {
                    status = _widgetTemplateClient.Delete(new ParameterModel { Ids = widgetTemplateIds });
                    if (status && list?.WidgetTemplates?.Count > 0)
                    {
                        foreach (var item in fileName.Split(','))
                        {
                            if (list.WidgetTemplates.FindAll(x => x.FileName == item).Count == 1)
                            {
                                //Gets the actual file path.
                                string actualFilePath = Path.Combine(HttpContext.Current.Server.MapPath(AdminConstants.TemplatesPath), item + ".cshtml");

                                if (File.Exists(actualFilePath))
                                    File.Delete(actualFilePath);
                            }
                        }
                    }
                }
                ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return status;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                errorMessage = Admin_Resources.WidgetTemplateAssociationDeleteError;
                return false;

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                errorMessage = Admin_Resources.ErrorFailedToDelete;
                return false;
            }
        }

        //Validate if the Widget Template Exist
        public virtual bool IsWidgetTemplateExist(string templateCode)
         => !string.IsNullOrEmpty(templateCode) ? _widgetTemplateClient.IsWidgetTemplateExist(templateCode) : true;

        //Copy Widget Template
        public virtual WidgetTemplateViewModel CopyWidgetTemplate(WidgetTemplateViewModel model)
        {
            try
            {
                ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return _widgetTemplateClient.Create(model?.ToModel<WidgetTemplateCreateModel>())?.ToViewModel<WidgetTemplateViewModel>();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                return null;
            }
        }

        //Download Widget Template
        public virtual string DownloadWidgetTemplate(int widgetTemplateId, string fileName)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (File.Exists(Path.Combine(HttpContext.Current.Server.MapPath(AdminConstants.TemplatesPath), fileName + ".cshtml")))
                return HttpContext.Current.Server.MapPath(AdminConstants.TemplatesPath);
            return null;
        }

        //Save file
        protected virtual void SaveFile(WidgetTemplateViewModel model)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNotNull(model?.FilePath))
            {
                string fileName = Path.GetFileName(model?.FilePath.FileName);
                ZnodeLogging.LogMessage("fileName: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new { fileName = fileName });
                try
                {
                    //If folder is not present, then create new folder.
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(AdminConstants.TemplatesPath)))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(AdminConstants.TemplatesPath));

                    //Saves the file in above folder.
                    string actualFilePath = Path.Combine(HttpContext.Current.Server.MapPath(AdminConstants.TemplatesPath), fileName);
                    model.FilePath.SaveAs(actualFilePath);
                    ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                    //In case any exception occurs then delete the file.
                    File.Delete(fileName);
                }
            }
        }

        //Set tools dropdown
        protected virtual void SetToolMenus(WidgetTemplateListViewModel model)
        {
            if (HelperUtility.IsNotNull(model))
            {
                model.GridModel = new Models.GridModel();
                model.GridModel.FilterColumn = new Models.FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<Models.ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new Models.ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('WidgetTemplateDeletePopUp')", ControllerName = "WidgetTemplate", ActionName = "Delete" });
            }
        }

        protected virtual string GetFileNameWithoutExtension(string fileName)
         => !string.IsNullOrEmpty(fileName) ? fileName.Substring(0, fileName.LastIndexOf('.')) : string.Empty;
    }
}
