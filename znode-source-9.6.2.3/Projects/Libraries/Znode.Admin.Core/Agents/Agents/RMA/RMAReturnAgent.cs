﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Agents
{
    public class RMAReturnAgent : BaseAgent, IRMAReturnAgent
    {
        #region Private Variables
        private readonly IRMAReturnClient _rmaReturnClient;
        private readonly IShoppingCartClient _shoppingCartClient;
        private readonly IOrderClient _orderClient;
        private readonly IUserClient _userClient;
        private readonly IPaymentClient _paymentClient;
        private IOrderAgent _orderAgent;
        #endregion

        #region Constructor
        public RMAReturnAgent(IRMAReturnClient rmaReturnClient, IShoppingCartClient shoppingCartClient, IOrderClient orderClient, IUserClient userClient, IPaymentClient paymentClient)
        {
            _rmaReturnClient = GetClient<IRMAReturnClient>(rmaReturnClient);
            _shoppingCartClient = GetClient<IShoppingCartClient>(shoppingCartClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
            _userClient = GetClient<IUserClient>(userClient);
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
        }
        #endregion 

        #region Public methods
        //Get returns list
        public virtual RMAReturnListViewModel GetReturnList(FilterCollectionDataModel model, int portalId = 0, string portalName = null)
        {
            FilterCollection _filters = SetFiltersForReturnList(model?.Filters, portalId);

            RMAReturnListModel returnList = _rmaReturnClient.GetReturnList(null, _filters, model?.SortCollection, model?.Page, model?.RecordPerPage);
            RMAReturnListViewModel returnListViewModel = new RMAReturnListViewModel { ReturnList = returnList?.Returns?.ToViewModel<RMAReturnViewModel>()?.ToList() };
            SetListPagingData(returnListViewModel, returnList);
            if (returnListViewModel?.ReturnList?.Count > 0)
            {
                foreach (RMAReturnViewModel returns in returnListViewModel.ReturnList)
                    SetReturnListData(returns);
            }
            else
                returnListViewModel = new RMAReturnListViewModel();
            BindStoreFilterValues(returnListViewModel, portalId, portalName);
            return returnListViewModel;
        }

        //Manage return
        public virtual RMAReturnViewModel ManageReturn(string returnNumber)
        {
            RMAReturnViewModel returnModel = _rmaReturnClient.GetReturnDetailsForAdmin(returnNumber)?.ToViewModel<RMAReturnViewModel>();
            if (IsNotNull(returnModel) && IsNotNull(returnModel.RMAOrderModel) && returnModel?.RMAOrderModel?.ShoppingCartModel?.ShoppingCartItems.Count > 0)
            {
                List<SelectListItem> orderStatusList = BindManageReturnStatus(new FilterTuple(ZnodeRmaReturnStateEnum.IsReturnLineItemState.ToString()?.ToLower(), FilterOperators.Equals, AdminConstants.True));
                foreach (RMAReturnLineItemViewModel returnLineItem in returnModel?.ReturnLineItems)
                {
                    ShoppingCartItemModel cartLineitem = returnModel.RMAOrderModel?.ShoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == returnLineItem.OmsOrderLineItemsId && ( string.Equals( x.OrderLineItemStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(),StringComparison.InvariantCultureIgnoreCase) || string.Equals( x.OrderLineItemStatus,ZnodeOrderStatusEnum.RETURNED.ToString(),StringComparison.InvariantCultureIgnoreCase)));
                    if (IsNotNull(cartLineitem))
                        BindReturnLineItemData(orderStatusList, returnLineItem, cartLineitem);
                    else
                        throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ShoppingCartNotNull);
                }
                SetReturnHistoryAmountWithCurrency(returnModel.CultureCode, returnModel.ReturnHistoryAndNotesList);
                BindOrderReturnData(returnModel);
                SaveInSession<RMAReturnViewModel>(AdminConstants.RMAReturnSessionKey + returnModel.ReturnNumber, returnModel);
            }
            return IsNotNull(returnModel) ? returnModel : new RMAReturnViewModel();
        }

        //Get Return Status
        public virtual RMAReturnStatusList GetReturnStatusList(string returnStatus)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ReturnStatus = returnStatus });

            if (!string.IsNullOrEmpty(returnStatus))
            {
                List<SelectListItem> statusList;
                statusList = BindManageReturnStatus(new FilterTuple(ZnodeRmaReturnStateEnum.IsReturnState.ToString()?.ToLower(), FilterOperators.Equals, AdminConstants.True));
                return new RMAReturnStatusList()
                {
                    ReturnStatusList = statusList,
                    SelectedItemValue = returnStatus,
                    SelectedItemId = Convert.ToInt32(statusList.FirstOrDefault(x => x.Text == returnStatus).Value)
                };
            }
            else
            {
                return new RMAReturnStatusList()
                {
                    ReturnStatusList = new List<SelectListItem>(),
                    SelectedItemId = 0
                };
            }
        }

        //Submit order return details.
        public virtual RMAReturnViewModel SubmitOrderReturn(string returnNumber, string notes)
        {
            RMAReturnViewModel returnViewModel = GetFromSession<RMAReturnViewModel>(AdminConstants.RMAReturnSessionKey + returnNumber);
            if (IsNotNull(returnViewModel) && IsNotNull(returnViewModel.OrderNumber) && returnViewModel?.ReturnLineItems?.Count > 0)
            {
                _orderAgent = DependencyResolver.Current.GetService<IOrderAgent>();
                if (returnViewModel.ReturnHistory?.Count < 1 && returnViewModel.ReturnLineItemHistory?.Count < 1)
                {
                    if (!string.IsNullOrEmpty(notes))
                    {
                        bool isNotesSaved = SaveReturnNotes(returnViewModel, notes);
                        return isNotesSaved ? new RMAReturnViewModel() { HasError = false, ErrorMessage = Admin_Resources.UpdateMessage } : new RMAReturnViewModel() { HasError = true, ErrorMessage = Admin_Resources.ErrorFailedToUpdate };
                    }
                    return new RMAReturnViewModel() { HasError = false, ErrorMessage = Admin_Resources.UpdateMessage };
                }
                try
                {
                    returnViewModel.IsAdminRequest = true;
                    returnViewModel.Notes = notes;
                    returnViewModel?.ReturnLineItems?.Where(x => x.RmaReasonForReturnId == (int)ZnodeReturnStateEnum.REJECTED)?.ToList()?.ForEach(y => y.ReturnedQuantity = 0);
                    if (returnViewModel?.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || returnViewModel?.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)
                    {
                        OrderModel orderModel = _orderClient.GetOrderById(returnViewModel.OmsOrderId, SetExpandForOrderDetails(true));
                        if (IsNotNull(orderModel) && orderModel?.OmsOrderId > 0 && IsNotNull(orderModel?.ShoppingCartModel))
                        {
                            orderModel.OrderOldValue.OrderLineItems = orderModel.OrderLineItems?.Where(x => !string.Equals(x.OrderLineItemState, ZnodeOrderStatusEnum.RETURNED.ToString(), StringComparison.InvariantCultureIgnoreCase))?.ToList();
                            orderModel.OrderOldValue.OrderLineItems.ForEach(s => s.Quantity = Convert.ToDecimal(s.Quantity.ToInventoryRoundOff()));
                            orderModel.OrderOldValue.ShippingAmount = HelperMethods.FormatPriceWithCurrency(orderModel.ShippingCost, orderModel.CultureCode);
                            orderModel.OrderOldValue.OrderState = orderModel.OrderState;
                            orderModel.OrderOldValue.ShippingId = orderModel.ShippingId;
                            orderModel.ReturnItemList.ReturnItemList.ForEach(x => x.PersonaliseValuesDetail = orderModel.OrderLineItems?.FirstOrDefault(order => order.OmsOrderLineItemsId == x.OmsOrderLineItemsId)?.PersonaliseValuesDetail);
                            _orderAgent.SetUserInformation(orderModel.ShoppingCartModel);
                            UpdateOrderLineItemDetails(orderModel, returnViewModel);

                            if (ValidateOrderUpdate(orderModel))
                            {
                                orderModel.ShoppingCartModel.BillingAddress = orderModel.BillingAddress;
                                orderModel.ShoppingCartModel.ShippingAddress = orderModel.ShippingAddress;
                                orderModel.ShoppingCartModel.UserId = orderModel.UserId;
                                _orderAgent.SetUsersPaymentDetails(Convert.ToInt32(orderModel.PaymentSettingId), orderModel.ShoppingCartModel, (String.Equals(orderModel.PaymentType, ZnodeConstant.COD, StringComparison.OrdinalIgnoreCase)));

                                orderModel.ShoppingCartModel.OrderDate = DateTime.Now;

                                OrderModel updatedOrderModel = _orderClient.UpdateOrder(orderModel);

                                string shippingErrorMesage = updatedOrderModel?.ShoppingCartModel?.Shipping?.ResponseMessage;

                                OrderViewModel updatedOrderViewModel = updatedOrderModel.ToViewModel<OrderViewModel>();
                                if (!string.IsNullOrEmpty(shippingErrorMesage))
                                {
                                    returnViewModel.HasError = true;
                                    returnViewModel.ErrorMessage = shippingErrorMesage;
                                    return returnViewModel;
                                }
                                if (IsNotNull(updatedOrderViewModel) && !updatedOrderViewModel.HasError)
                                {
                                    returnViewModel.OverDueAmount = updatedOrderModel.OverDueAmount;
                                    returnViewModel = _rmaReturnClient.SubmitOrderReturn(returnViewModel.ToModel<RMAReturnModel>())?.ToViewModel<RMAReturnViewModel>();
                                    if (!returnViewModel.HasError)
                                        RemoveInSession(AdminConstants.RMAReturnSessionKey + returnNumber);

                                    return new RMAReturnViewModel() { ErrorMessage = returnViewModel.HasError ? Admin_Resources.ErrorFailedToUpdate : Admin_Resources.UpdateMessage };
                                }
                            }
                        }
                        else
                        {
                            throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);
                        }
                    }
                    else if (returnViewModel?.RmaReturnStateId == (int)ZnodeReturnStateEnum.REFUND_PROCESSED)
                    {
                        if (!returnViewModel.IsRefundProcess)
                        {
                            string errorMessage = returnViewModel?.RMAOrderModel?.PaymentType == ZnodeConstant.COD ? Admin_Resources.PaymentRefundSuccessMessage : string.Empty;
                            string transactionId = returnViewModel?.RMAOrderModel?.PaymentTransactionToken;
                            decimal overDueAmount = IsNotNull(returnViewModel?.OverDueAmount) ? returnViewModel.OverDueAmount : 0;
                            bool status = returnViewModel?.RMAOrderModel?.PaymentType == ZnodeConstant.COD ? true : RefundProcess(returnViewModel.RMAOrderModel, returnViewModel.OverDueAmount, out errorMessage);
                            if (status)
                            {
                                returnViewModel.IsRefundProcess = true;
                                returnViewModel = _rmaReturnClient.SubmitOrderReturn(returnViewModel.ToModel<RMAReturnModel>()).ToViewModel<RMAReturnViewModel>();
                            }
                            else if (!string.IsNullOrEmpty(notes))
                            {
                                SaveReturnNotes(returnViewModel, notes);
                            }
                            CreateRefundHistory(returnViewModel, errorMessage, transactionId, overDueAmount);
                            return new RMAReturnViewModel() { HasError = !status, ErrorMessage = errorMessage };
                        }
                        else
                        {
                            return new RMAReturnViewModel() { HasError = false, ErrorMessage = Admin_Resources.PaymentRefundSuccessMessage };
                        }
                    }
                    else
                    {
                        returnViewModel = _rmaReturnClient.SubmitOrderReturn(returnViewModel.ToModel<RMAReturnModel>()).ToViewModel<RMAReturnViewModel>();
                        return new RMAReturnViewModel() { ErrorMessage = returnViewModel.HasError ? Admin_Resources.ErrorFailedToUpdate : Admin_Resources.UpdateMessage };
                    }
                }
                catch (ZnodeException exception)
                {
                    ZnodeLogging.LogMessage(exception, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
                    switch (exception.ErrorCode)
                    {
                        case ErrorCodes.NotOrderEligibleForReturn:
                            return new RMAReturnViewModel() { HasError = true, ErrorMessage = Admin_Resources.ErrorOrderNotEligibleForReturn };
                        case ErrorCodes.NullModel:
                            return new RMAReturnViewModel() { HasError = true, ErrorMessage = ZnodeConstant.NullModelError };
                        default:
                            return new RMAReturnViewModel() { HasError = true, ErrorMessage = Admin_Resources.ErrorFailedToCreate };
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    return new RMAReturnViewModel() { HasError = true, ErrorMessage = Admin_Resources.ErrorFailedToCreate };
                }
            }
            return new RMAReturnViewModel() { HasError = true, ErrorMessage = Admin_Resources.ErrorFailedToCreate };
        }

        //Update Order Return Line Item
        public virtual RMAReturnViewModel UpdateOrderReturnLineItem(RMAReturnLineItemViewModel orderReturnLineItemModel, string returnNumber)
        {
            RMAReturnViewModel returnViewModel = GetFromSession<RMAReturnViewModel>(AdminConstants.RMAReturnSessionKey + returnNumber);

            RMAReturnLineItemViewModel returnLineItemViewModel = returnViewModel?.ReturnLineItems?.FirstOrDefault(x => x.RmaReturnLineItemsId == orderReturnLineItemModel.RmaReturnLineItemsId);
            bool hasError = true;
            string errorMessage = string.Empty;
            if (IsValidUpdateOrderReturnData(returnLineItemViewModel, orderReturnLineItemModel, ref hasError, ref errorMessage))
            {
                ShoppingCartItemModel shoppingCartItem = returnViewModel?.RMAOrderModel?.ShoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == orderReturnLineItemModel.OmsOrderLineItemsId &&string.Equals( x.OrderLineItemStatus,ZnodeOrderStatusEnum.SHIPPED.ToString(),StringComparison.InvariantCultureIgnoreCase));
                returnLineItemViewModel.ReturnedQuantity = orderReturnLineItemModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.REJECTED ? 0 : orderReturnLineItemModel.ReturnedQuantity;
                returnLineItemViewModel.RefundAmount = orderReturnLineItemModel.RefundAmount;
                returnLineItemViewModel.Total = (decimal)returnLineItemViewModel.ReturnedQuantity * returnLineItemViewModel.Price;
                returnLineItemViewModel.RmaReturnStateId = orderReturnLineItemModel.RmaReturnStateId;
                returnLineItemViewModel.ReturnStatus = orderReturnLineItemModel.ReturnStatus;
                returnLineItemViewModel.IsShippingReturn = orderReturnLineItemModel.IsShippingReturn;
                returnLineItemViewModel.ShippingCost = shoppingCartItem.ShippingCost > 0 && returnLineItemViewModel.ReturnedQuantity != 0 ? shoppingCartItem.ShippingCost / shoppingCartItem?.Quantity * orderReturnLineItemModel.ReturnedQuantity : 0;
                returnLineItemViewModel.TaxCost = shoppingCartItem.TaxCost > 0 && returnLineItemViewModel.ReturnedQuantity != 0 ? (shoppingCartItem.TaxCost / shoppingCartItem.Quantity) * (decimal)orderReturnLineItemModel.ReturnedQuantity : 0;
                UpdateReturnLineItemHistory(returnViewModel, returnLineItemViewModel);
            }
            BindTotalSummaryData(returnViewModel);
            SaveInSession<RMAReturnViewModel>(AdminConstants.RMAReturnSessionKey + returnNumber, returnViewModel);
            returnViewModel.HasError = hasError;
            returnViewModel.ErrorMessage = errorMessage;
            return returnViewModel;
        }

        //Update Order Return Status
        public virtual RMAReturnViewModel UpdateOrderReturnStatus(int returnStatusCode, string returnNumber)
        {
            if (!string.IsNullOrEmpty(returnNumber) && returnStatusCode > 0)
            {
                RMAReturnViewModel returnViewModel = GetFromSession<RMAReturnViewModel>(AdminConstants.RMAReturnSessionKey + returnNumber);
                bool hasError = true;
                string errorMessage = "";
                if (IsNotNull(returnViewModel))
                {
                    UpdateReturnStatus(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);

                    if (!hasError)
                    {
                        RemoveKeyFromDictionaryForReturn(returnViewModel, ZnodeConstant.ReturnUpdatedStatus);
                        if (!Equals(returnViewModel.OldReturnStatus, returnViewModel.ReturnStatus))
                            ReturnHistory(returnViewModel, ZnodeConstant.ReturnUpdatedStatus, returnViewModel.ReturnStatus);
                        SaveInSession<RMAReturnViewModel>(AdminConstants.RMAReturnSessionKey + returnNumber, returnViewModel);
                    }
                    returnViewModel.HasError = hasError;
                    returnViewModel.ErrorMessage = errorMessage;
                    return returnViewModel;
                }
            }
            return new RMAReturnViewModel() { HasError = true, ErrorMessage = Admin_Resources.ErrorUpdateReturnStatus };
        }

        //Print order return receipt by return number
        public virtual RMAReturnViewModel PrintReturnReceipt(string returnNumber)
        {
            RMAReturnViewModel returnViewModel = _rmaReturnClient.GetReturnDetails(returnNumber, SetExpandForReturnDetails())?.ToViewModel<RMAReturnViewModel>();
            SetPortalDetails(returnViewModel);
            return IsNotNull(returnViewModel) ? returnViewModel : new RMAReturnViewModel();
        }
        #endregion

        #region Protected methods    
        //Convert Enumerable list of Order state model to select lit items.
        protected virtual List<SelectListItem> ToReturnStateList(IEnumerable<RMAReturnStateModel> returnStateList)
        {
            List<SelectListItem> returnStateItems = new List<SelectListItem>();
            if (returnStateList?.Count() > 0)
                returnStateItems = (from item in returnStateList
                                    orderby item.DisplayOrder ascending
                                    select new SelectListItem
                                    {
                                        Text = item.ReturnStateName,
                                        Value = item.RmaReturnStateId.ToString()
                                    }).ToList();


            int index = returnStateItems.FindIndex(x => string.Equals(x.Text, ZnodeConstant.ReturnStateNotSubmitted, StringComparison.CurrentCultureIgnoreCase));
            if (index != -1)
                returnStateItems.RemoveAt(index);
            return returnStateItems;
        }

        //Set Return History Amount With Currency
        protected virtual void SetReturnHistoryAmountWithCurrency(string cultureCode, List<RMAReturnHistoryViewModel> returnHistoryList)
        {
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { CultureCode = cultureCode });
            if (IsNotNull(returnHistoryList) && returnHistoryList.Count > 0)
            {
                foreach (RMAReturnHistoryViewModel returnHistory in returnHistoryList)
                {
                    returnHistory.ReturnAmountWithCurrency = IsNull(returnHistory.ReturnAmount) ? string.Empty : HelperMethods.FormatPriceWithCurrency(returnHistory.ReturnAmount, cultureCode);
                    returnHistory.ReturnDateWithTime = Convert.ToDateTime(returnHistory.CreatedDate).ToString(HelperMethods.GetStringDateTimeFormat());
                }
            }
        }

        //Bind Order Return Data
        protected virtual void BindOrderReturnData(RMAReturnViewModel returnModel)
        {
            returnModel.ReturnDateWithTime = Convert.ToDateTime(returnModel.ReturnDate).ToString(HelperMethods.GetStringDateTimeFormat());
            returnModel.PhoneNumber = returnModel.RMAOrderModel?.BillingAddress?.PhoneNumber;
            returnModel.PaymentType = returnModel?.RMAOrderModel?.PaymentType;
            returnModel.PaymentDisplayName = returnModel?.RMAOrderModel?.PaymentDisplayName;
            returnModel.CreditCardNumber = returnModel?.RMAOrderModel?.CreditCardNumber;
            returnModel.OldReturnStatus = returnModel.ReturnStatus;
            returnModel.OldRmaReturnStateId = returnModel.RmaReturnStateId;
            returnModel.OldReturnLineItems?.ForEach(x => { if (x.ReturnedQuantity < 1) x.ReturnedQuantity = x.ExpectedReturnQuantity; });
            BindTotalSummaryData(returnModel);
        }

        //Bind Total Summary Data
        protected virtual void BindTotalSummaryData(RMAReturnViewModel returnViewModel)
        {
            if (returnViewModel != null)
            {
                if (returnViewModel?.ReturnLineItems?.Count > 0 && (returnViewModel?.RmaReturnStateId == (int)ZnodeReturnStateEnum.SUBMITTED ||
                                                                    returnViewModel?.RmaReturnStateId == (int)ZnodeReturnStateEnum.IN_REVIEW))
                {
                    returnViewModel.SubTotal = returnViewModel.ReturnLineItems.Sum(x => x.Total);
                    returnViewModel.ReturnShippingCost = returnViewModel.ReturnLineItems.Where(x => x.IsShippingReturn).Sum(x => x.ShippingCost);
                    returnViewModel.DiscountAmount = (decimal)returnViewModel.ReturnLineItems.Sum(x => x.DiscountAmount) + (decimal)returnViewModel.ReturnLineItems.Sum(x => x.RefundAmount);
                    returnViewModel.ReturnTaxCost = returnViewModel.ReturnLineItems.Sum(x => x.TaxCost);
                    returnViewModel.TotalReturnAmount = (returnViewModel.SubTotal + returnViewModel.ReturnTaxCost + (decimal)returnViewModel.ReturnShippingCost) - returnViewModel.DiscountAmount;
                }
                returnViewModel.ReturnTotalModel = new RMAReturnTotalViewModel()
                {
                    SubTotal = returnViewModel.SubTotal,
                    ReturnTaxCost = returnViewModel.ReturnTaxCost,
                    ReturnShippingCost = returnViewModel.ReturnShippingCost,
                    DiscountAmount = returnViewModel.DiscountAmount,
                    TotalReturnAmount = returnViewModel.TotalReturnAmount
                };
            }
        }

        //Bind Return Line Item Data
        protected virtual void BindReturnLineItemData(List<SelectListItem> orderStatusList, RMAReturnLineItemViewModel returnLineItem, ShoppingCartItemModel cartLineitem)
        {
            returnLineItem.ReturnStatusList = orderStatusList;
            returnLineItem.ImagePath = cartLineitem.ImagePath;
            returnLineItem.ReturnedQuantity = string.IsNullOrEmpty(returnLineItem?.ReturnedQuantity?.ToString()) ? returnLineItem.ExpectedReturnQuantity : returnLineItem.ReturnedQuantity;
            returnLineItem.Total = returnLineItem.Price * (decimal)returnLineItem.ReturnedQuantity;
            returnLineItem.ShippingCost = string.IsNullOrEmpty(returnLineItem?.ShippingCost?.ToString()) ? cartLineitem.ShippingCost / cartLineitem?.Quantity * returnLineItem.ReturnedQuantity ?? 0 : returnLineItem.ShippingCost;
            returnLineItem.TaxCost = cartLineitem.TaxCost > 0 && returnLineItem.ReturnedQuantity != 0 ? (cartLineitem.TaxCost / cartLineitem.Quantity) * (decimal)returnLineItem.ReturnedQuantity : 0;
            returnLineItem.ProductId = cartLineitem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group ? cartLineitem?.GroupProducts?.FirstOrDefault()?.ProductId : 0;
            returnLineItem.PersonaliseValuesDetail = cartLineitem.PersonaliseValuesDetail;
            returnLineItem.PersonaliseValuesList = cartLineitem.PersonaliseValuesList;
        }

        //Is Valid Update Order Return Data
        protected virtual bool IsValidUpdateOrderReturnData(RMAReturnLineItemViewModel returnLineItemViewModel, RMAReturnLineItemViewModel orderReturnLineItemModel, ref bool hasError, ref string errorMessage)
        {
            if (IsNotNull(returnLineItemViewModel) && IsNotNull(orderReturnLineItemModel))
            {
                if (returnLineItemViewModel.ExpectedReturnQuantity < orderReturnLineItemModel.ReturnedQuantity || returnLineItemViewModel.Total < orderReturnLineItemModel.RefundAmount)
                {
                    hasError = true;
                    errorMessage = Admin_Resources.ErrorReturned;
                    return false;
                }
                else
                {
                    hasError = false;
                    errorMessage = string.Empty;
                    return true;
                }
            }
            else
                return false;
        }

        //To update filter with specified details.
        protected virtual FilterCollection SetFiltersForReturnList(FilterCollection filters, int portalId)
        {
            FilterCollection _filters = new FilterCollection();
            _filters.AddRange(filters);
            _filters.RemoveAll(x => string.Equals(x.Item1, FilterKeys.IsFromAdmin.ToString(), StringComparison.InvariantCultureIgnoreCase));
            _filters.Add(new FilterTuple(FilterKeys.IsFromAdmin, FilterOperators.Equals, FilterKeys.ActiveTrueValue));

            //Add portal id in filter collection.
            DependencyResolver.Current.GetService<IOrderAgent>().AddPortalIdInFilters(_filters, portalId);

            DateRangePickerHelper.FormatFilterForDateTimeRange(_filters, DateTimeRange.Last_30_Days.ToString(), DateTimeRange.All_Returns.ToString());
            return _filters;
        }

        //Update return status according to status code
        protected virtual void UpdateReturnStatus(int returnStatusCode, RMAReturnViewModel returnViewModel, ref bool hasError, ref string errorMessage)
        {
            if (IsNotNull(returnViewModel) && returnViewModel?.ReturnLineItems?.Count > 0)
            {
                hasError = true;
                switch (returnStatusCode)
                {
                    case (int)ZnodeReturnStateEnum.SUBMITTED:
                        if (returnViewModel.RmaReturnStateId != returnStatusCode)
                            errorMessage = Admin_Resources.ErrorUpdateSubmittedReturnStatus;
                        else
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                        break;

                    case (int)ZnodeReturnStateEnum.RECEIVED:
                        if (returnViewModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || returnViewModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)
                            errorMessage = Admin_Resources.ErrorUpdateReturnStatusToRefundProcessed;
                        else
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                        break;

                    case (int)ZnodeReturnStateEnum.IN_REVIEW:
                        if (returnViewModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || returnViewModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)
                            errorMessage = Admin_Resources.ErrorUpdateReturnStatusToRefundProcessed;
                        else
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                        break;

                    case (int)ZnodeReturnStateEnum.APPROVED:
                        if (returnViewModel.ReturnLineItems.Any(x => x.RmaReturnStateId != (int)ZnodeReturnStateEnum.APPROVED && x.RmaReturnStateId != (int)ZnodeReturnStateEnum.REJECTED) || !returnViewModel.ReturnLineItems.Any(x => x.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED))
                            errorMessage = Admin_Resources.ErrorUpdateApprovedReturnStatus;
                        else if ((string.Equals(returnViewModel.RMAOrderModel?.PaymentType, ZnodeConstant.CreditCard.ToString(), StringComparison.InvariantCultureIgnoreCase)
                            || string.Equals(returnViewModel.RMAOrderModel?.PaymentType, ZnodeConstant.Amazon_Pay.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            && string.Equals(returnViewModel.RMAOrderModel?.PaymentStatus, ZnodeConstant.AUTHORIZED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            errorMessage = Admin_Resources.ErrorCapturePaymentToApproveReturn;
                        else
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                        break;

                    case (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED:
                        if (returnViewModel.ReturnLineItems.Any(x => x.RmaReturnStateId == (int)ZnodeReturnStateEnum.SUBMITTED || x.RmaReturnStateId == (int)ZnodeReturnStateEnum.RECEIVED) || !returnViewModel.ReturnLineItems.Any(x => x.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED))
                            errorMessage = Admin_Resources.ErrorUpdatePartiallyApprovedReturnStatus;
                        else if ((string.Equals(returnViewModel.RMAOrderModel?.PaymentType, ZnodeConstant.CreditCard.ToString(), StringComparison.InvariantCultureIgnoreCase)
                            || string.Equals(returnViewModel.RMAOrderModel?.PaymentType, ZnodeConstant.Amazon_Pay.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            && string.Equals(returnViewModel.RMAOrderModel?.PaymentStatus, ZnodeConstant.AUTHORIZED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            errorMessage = Admin_Resources.ErrorCapturePaymentToApproveReturn;
                        else
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                        break;

                    case (int)ZnodeReturnStateEnum.REJECTED:
                        if (returnViewModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || returnViewModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)
                            errorMessage = Admin_Resources.ErrorUpdateReturnStatusToRefundProcessed;
                        else if (returnViewModel.ReturnLineItems.Any(x => x.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || x.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED))
                            errorMessage = Admin_Resources.ErrorUpdateRejectedReturnStatus;
                        else
                        {
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                            returnViewModel.ReturnLineItems.ForEach(x => { x.RmaReturnStateId = returnStatusCode; x.ReturnedQuantity = 0; x.Total = 0; x.IsShippingReturn = false; x.ShippingCost = 0; x.RefundAmount = 0; x.TaxCost = 0; });
                            BindTotalSummaryData(returnViewModel);
                        }
                        break;

                    case (int)ZnodeReturnStateEnum.REFUND_PROCESSED:
                        if (returnViewModel.OldRmaReturnStateId != (int)ZnodeReturnStateEnum.APPROVED && returnViewModel.OldRmaReturnStateId != (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)
                            errorMessage = Admin_Resources.ErrorUpdateRefundProcessedReturnStatus;
                        else if ((string.Equals(returnViewModel.RMAOrderModel?.PaymentType, ZnodeConstant.CreditCard.ToString(), StringComparison.InvariantCultureIgnoreCase)
                            || string.Equals(returnViewModel.RMAOrderModel?.PaymentType, ZnodeConstant.Amazon_Pay.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            && string.Equals(returnViewModel.RMAOrderModel?.PaymentStatus, ZnodeConstant.AUTHORIZED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            errorMessage = Admin_Resources.ErrorCapturePaymentToProcessRefund;
                        else
                        {
                            UpdateReturnStatusInViewModel(returnStatusCode, returnViewModel, ref hasError, ref errorMessage);
                            errorMessage = errorMessage + " " + Admin_Resources.MessageSaveChangesToProcessRefund;
                        }
                        break;

                    default:
                        errorMessage = Admin_Resources.ErrorUpdateReturnStatus;
                        break;
                }
            }
        }

        //Update return status in return view model
        protected virtual void UpdateReturnStatusInViewModel(int returnStatusCode, RMAReturnViewModel returnViewModel, ref bool hasError, ref string errorMessage)
        {
            if (IsNotNull(returnViewModel))
            {
                hasError = false;
                errorMessage = Admin_Resources.SuccessUpdateReturnStatus;
                returnViewModel.RmaReturnStateId = returnStatusCode;
                returnViewModel.ReturnStatus = Enum.GetName(typeof(ZnodeReturnStateEnum), returnStatusCode)?.Replace("_", " ")?.ToLower()?.ToProperCase();
            }
        }

        //Update Order Line Item Details
        protected virtual void UpdateOrderLineItemDetails(OrderModel orderModel, RMAReturnViewModel returnViewModel)
        {
            _orderAgent = DependencyResolver.Current.GetService<IOrderAgent>();
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cart = orderModel?.ShoppingCartModel;
            List<RMAReturnLineItemViewModel> returnLineItemList = returnViewModel?.ReturnLineItems?.Where(x => x.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || x.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)?.ToList();
            if (IsNotNull(cart) && returnLineItemList?.Count > 0)
            {
                foreach (RMAReturnLineItemViewModel returnLineItem in returnLineItemList)
                {
                    ManageOrderDataModel orderDataModel = new ManageOrderDataModel()
                    {
                        CustomQuantity = (decimal)returnLineItem.ReturnedQuantity,
                        OrderLineItemStatusId = ZnodeConstant.ReturnOrderLineItemStatusId,
                        OrderLineItemStatus = ZnodeOrderStatusEnum.RETURNED.ToString(),
                        ReasonForReturnId = (int)returnLineItem.RmaReasonForReturnId,
                        ReasonForReturn = returnLineItem.RmaReasonForReturn,
                        OmsOrderLineItemsId = (int)returnLineItem.OmsOrderLineItemsId,
                        ProductId = (int)returnLineItem.ProductId,
                        IsShippingReturn = returnLineItem.IsShippingReturn,
                        ShippingCost = returnLineItem.IsShippingReturn ? (decimal)returnLineItem.ShippingCost : 0,
                    };

                    // Check if item exists.
                    ShoppingCartItemModel cartItem = cart?.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == orderDataModel.OmsOrderLineItemsId);
                    if (IsNull(cartItem))
                        throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ShoppingCartNotNull);

                    _orderAgent.SetCreatedByUser(cart?.UserId);

                    decimal orderlineItemQuantity = GetOrderLineItemQuantity(orderModel, cartItem);
                    orderDataModel.Quantity = orderlineItemQuantity;
                    ZnodeLogging.LogMessage("OrderlineItemQuantity: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderlineItemQuantity = orderlineItemQuantity });

                    if (IsNotNull(cart))
                    {
                        GetUpdatedOrderLineItem(cart, cartItem, orderDataModel, orderModel);

                        cart.ShippingAddress = cart.ShippingAddress;
                        cart.Payment = new PaymentModel() { ShippingAddress = cart.ShippingAddress, PaymentSetting = new PaymentSettingModel() };

                        //Set Gift Card Number and CSR Discount Amount Data For Calculation
                        cart.GiftCardNumber = cart.GiftCardNumber;
                        cart.CSRDiscountAmount = cart.CSRDiscountAmount;
                        
                        if (!string.IsNullOrEmpty(orderDataModel.OrderLineItemStatus) && string.Equals(orderDataModel.OrderLineItemStatus, ZnodeOrderStatusEnum.RETURNED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            ShoppingCartItemModel cartItemModel = GetCustomCartItem(cart.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == orderDataModel.OmsOrderLineItemsId));
                            GetReturnLineItemList(orderDataModel, orderModel, cart, cartItemModel);
                        }

                        //This code will Execute, if ship separately line item return with shipping cost
                        if (!IsNull(orderDataModel) && orderDataModel.IsShippingReturn)
                        {
                            UpdateReturnShippingHistory(orderModel, orderDataModel, returnLineItem, true);
                        }
                    }
                }
                if(cart.GiftCardAmount > 0)
                    cart.IsCalculateVoucher = cart.IsQuoteOrder ? false : true;
                orderModel.ShoppingCartModel = _orderAgent.GetCalculatedShoppingCartForEditOrder(cart);
            }
        }

        //Create return line items list
        protected virtual void GetReturnLineItemList(ManageOrderDataModel orderDataModel, OrderModel orderModel, ShoppingCartModel cart, ShoppingCartItemModel cartItemModel)
        {
            cart.ShoppingCartItems?.Remove(cart.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == orderDataModel.OmsOrderLineItemsId));

            if (orderModel.ReturnItemList?.ReturnItemList?.Count > 0)
                orderModel.ReturnItemList.ReturnItemList.Add(GetReturnLineItem(cartItemModel, orderDataModel));
            else
            {
                orderModel.ReturnItemList = new ReturnOrderLineItemListModel { ReturnItemList = new List<ReturnOrderLineItemModel>() };
                orderModel.ReturnItemList.ReturnItemList.Add(GetReturnLineItem(cartItemModel, orderDataModel));
            }
            orderModel.ReturnItemList.SubTotal = orderModel.ReturnItemList.ReturnItemList.Sum(x => x.ExtendedPrice);
            orderModel.ReturnItemList.DiscountAmount = orderModel.ReturnItemList.ReturnItemList.Sum(x => x.ProductDiscountAmount);

            if (!orderModel.IsTaxCostEdited)
                orderModel.ReturnItemList.TaxCost = orderModel.ReturnItemList.ReturnItemList.Sum(x => x.TaxCost);

            orderModel.ReturnItemList.Total = (orderModel.ReturnItemList.SubTotal + orderModel.ReturnItemList.TaxCost - orderModel.ReturnItemList.DiscountAmount);
            cart.IsLineItemReturned = true;
            cart.ReturnItemList = orderModel.ReturnItemList.ReturnItemList;

            if (orderModel?.ShoppingCartModel.ShoppingCartItems?.Count < 1 && orderModel.ReturnItemList?.ReturnItemList?.Count > 0)
                orderModel.OrderHistory.Add(ZnodeConstant.OrderReturnAllAndCancelStatus, ZnodeOrderStatusEnum.CANCELLED.ToString());
        }

        //Get quantity of ordered line item.
        protected virtual decimal GetOrderLineItemQuantity(OrderModel orderModel, ShoppingCartItemModel cartItemModel)
        {
            return (cartItemModel.GroupProducts?.Count > 0 ?
                    (orderModel.OrderLineItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == cartItemModel.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)?.Quantity).GetValueOrDefault()
                    : !string.IsNullOrEmpty(cartItemModel.ConfigurableProductSKUs) ? (orderModel.OrderLineItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == cartItemModel.OmsOrderLineItemsId)?.Quantity).GetValueOrDefault() :
                    (orderModel.OrderLineItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == cartItemModel.OmsOrderLineItemsId)?.Quantity).GetValueOrDefault());
        }

        //Update custom data for cart items.
        protected virtual void GetUpdatedOrderLineItem(ShoppingCartModel cartModel, ShoppingCartItemModel shoppingCartItem, ManageOrderDataModel orderDataModel, OrderModel orderModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            string sku = string.Empty;
            OrderLineItemHistoryModel orderLineItemHistoryModel = new OrderLineItemHistoryModel();

            if (IsNotNull(shoppingCartItem))
            {
                sku = orderDataModel.ProductId > 0 ? shoppingCartItem.ProductName + "-" + Convert.ToString(shoppingCartItem.GroupProducts?.Where(y => y.ProductId == orderDataModel.ProductId).Select(s => s.Sku).FirstOrDefault()) : sku = shoppingCartItem.SKU;

                //Set sku of the actual product irrespective of any product type
                orderLineItemHistoryModel.SKU = (orderDataModel.ProductId > 0)
                                    ? shoppingCartItem.GroupProducts?.FirstOrDefault(y => y.ProductId == orderDataModel.ProductId)?.Sku
                                    : !string.IsNullOrEmpty(shoppingCartItem.ConfigurableProductSKUs) ? shoppingCartItem.ConfigurableProductSKUs : shoppingCartItem.SKU;

                ZnodeLogging.LogMessage("SKU of the actual product: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { SKU = orderLineItemHistoryModel.SKU });

                DependencyResolver.Current.GetService<IOrderAgent>().RemoveKeyFromDictionary(orderModel, sku, true);

                if (orderDataModel.OrderLineItemStatusId > 0 && !Equals(shoppingCartItem.OmsOrderStatusId, orderDataModel.OrderLineItemStatusId) && !string.IsNullOrEmpty(orderDataModel.OrderLineItemStatus))
                {
                    orderLineItemHistoryModel.OrderUpdatedStatus = orderDataModel.OrderLineItemStatus;
                    shoppingCartItem.IsItemStateChanged = true;
                }

                if (HelperUtility.IsNotNull(orderDataModel.PartialRefundAmount) && orderDataModel.PartialRefundAmount > 0 && orderDataModel.PartialRefundAmount != shoppingCartItem.PartialRefundAmount)
                {
                    shoppingCartItem.PartialRefundAmount = orderDataModel.PartialRefundAmount;
                    orderLineItemHistoryModel.PartialRefundAmount = HelperMethods.FormatPriceWithCurrency(orderDataModel.PartialRefundAmount ?? 0, orderModel.CultureCode);
                }

                if (IsNotNull(orderLineItemHistoryModel))
                {
                    orderLineItemHistoryModel.ProductName = shoppingCartItem.ProductName;
                    orderLineItemHistoryModel.Quantity = orderDataModel.CustomQuantity > 0 ? orderDataModel.CustomQuantity.ToInventoryRoundOff() : (orderDataModel.Quantity > shoppingCartItem.Quantity || orderDataModel.Quantity < shoppingCartItem.Quantity) ? orderDataModel.Quantity.ToInventoryRoundOff() : shoppingCartItem.Quantity.ToInventoryRoundOff();
                    orderLineItemHistoryModel.OmsOrderLineItemsId = shoppingCartItem.OmsOrderLineItemsId;
                    orderLineItemHistoryModel.TaxCost = shoppingCartItem.TaxCost / orderDataModel?.Quantity * Convert.ToDecimal(orderLineItemHistoryModel.Quantity) ?? 0;
                    orderLineItemHistoryModel.ReturnShippingAmount = orderDataModel.IsShippingReturn ? orderDataModel.ShippingCost.ToInventoryRoundOff() : string.Empty;
                    orderLineItemHistoryModel.DiscountAmount = Convert.ToDecimal(shoppingCartItem.DiscountAmount) / orderDataModel?.Quantity * Convert.ToDecimal(orderLineItemHistoryModel.Quantity) ?? 0;
                    orderLineItemHistoryModel.SubTotal = Convert.ToDecimal(orderLineItemHistoryModel.Quantity) * shoppingCartItem.UnitPrice;
                    OrderLineItemHistory(orderModel, sku, orderLineItemHistoryModel);
                }

                if (orderDataModel.OrderLineItemStatusId > 0 && orderDataModel.CustomQuantity > 0 && orderDataModel.Quantity > orderDataModel.CustomQuantity)
                {
                    ShoppingCartItemModel customCartItem = GetCustomCartItem(shoppingCartItem, orderDataModel, cartModel.TaxCost);
                    customCartItem.GroupProducts = GetCustomGroupProductItems(shoppingCartItem, orderDataModel);

                    cartModel.ShoppingCartItems.Insert(cartModel.ShoppingCartItems.Count, customCartItem);

                    if (shoppingCartItem.GroupProducts?.Count > 0)
                        shoppingCartItem.GroupProducts?.ForEach(x => x.Quantity = orderDataModel.Quantity - orderDataModel.CustomQuantity);
                    else
                        shoppingCartItem.Quantity = orderDataModel.Quantity - orderDataModel.CustomQuantity;
                }
                else
                {
                    shoppingCartItem.TrackingNumber = orderDataModel.TrackingNumber;
                    shoppingCartItem.Quantity = orderDataModel.Quantity;
                    shoppingCartItem.OmsOrderStatusId = orderDataModel.OrderLineItemStatusId > 0 ? orderDataModel.OrderLineItemStatusId : shoppingCartItem.OmsOrderStatusId;
                    shoppingCartItem.OrderLineItemStatus = !string.IsNullOrEmpty(orderDataModel.OrderLineItemStatus) && orderDataModel.OrderLineItemStatusId > 0 ? orderDataModel.OrderLineItemStatus : shoppingCartItem.OrderLineItemStatus;
                    shoppingCartItem.ExtendedPrice = IsNotNull(orderDataModel.UnitPrice) ? orderDataModel.UnitPrice.GetValueOrDefault() * orderDataModel.Quantity : shoppingCartItem.ExtendedPrice;
                    shoppingCartItem.IsShippingReturn = orderDataModel.IsShippingReturn;
                    shoppingCartItem.PartialRefundAmount = IsNotNull(orderDataModel.PartialRefundAmount) ? orderDataModel.PartialRefundAmount : shoppingCartItem.PartialRefundAmount;
                }

                cartModel.ShoppingCartItems.Insert(cartModel.ShoppingCartItems.FindIndex(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemsId), shoppingCartItem);

                if (orderModel.ReturnItemList?.ReturnItemList?.Count > 0)
                    cartModel.ShoppingCartItems.Remove(cartModel.ShoppingCartItems.LastOrDefault(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemsId));
                else
                    cartModel.ShoppingCartItems.Remove(cartModel.ShoppingCartItems.LastOrDefault(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemsId));

                ZnodeLogging.LogMessage("ShoppingCartItem model with CustomUnitPrice, OmsOrderStatusId and ExternalId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { CustomUnitPrice = shoppingCartItem.CustomUnitPrice, OmsOrderStatusId = shoppingCartItem.OmsOrderStatusId, ExternalId = shoppingCartItem.ExternalId });
            }
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Get Custom Cart Item
        protected virtual ShoppingCartItemModel GetCustomCartItem(ShoppingCartItemModel itemModel, ManageOrderDataModel orderDataModel = null, decimal? cartTaxCost = null)
       => IsNotNull(itemModel) ? new ShoppingCartItemModel
       {
           Description = itemModel.Description,
           ExtendedPrice = IsNotNull(orderDataModel?.UnitPrice) ? orderDataModel.UnitPrice.GetValueOrDefault() * orderDataModel.CustomQuantity : itemModel.ExtendedPrice,
           ExternalId = itemModel.ExternalId,
           ProductId = itemModel.ProductId,
           ParentProductId = itemModel.ParentProductId,
           Quantity = itemModel.GroupProducts?.Count > 0 ? itemModel.Quantity : orderDataModel?.CustomQuantity > 0 ? orderDataModel.CustomQuantity : itemModel.Quantity,
           ShippingCost = itemModel.ShippingCost,
           CustomShippingCost = itemModel.CustomShippingCost,
           ProductDiscountAmount = itemModel.ProductDiscountAmount,
           ShippingOptionId = itemModel.ShippingOptionId,
           SKU = itemModel.SKU,
           IsActive = itemModel.IsActive,
           UnitPrice = itemModel.UnitPrice,
           CustomUnitPrice = itemModel.CustomUnitPrice,
           InsufficientQuantity = itemModel.InsufficientQuantity,
           CartDescription = itemModel.CartDescription,
           CurrencyCode = itemModel.CurrencyCode,
           ImagePath = itemModel.ImagePath,
           MediaConfigurationId = itemModel.MediaConfigurationId,
           ProductName = itemModel.ProductName,
           ProductType = itemModel.ProductType,
           ImageMediumPath = itemModel.ImageMediumPath,
           MaxQuantity = itemModel.MaxQuantity,
           MinQuantity = itemModel.MinQuantity,
           AddOnProductSKUs = itemModel.AddOnProductSKUs,
           BundleProductSKUs = itemModel.BundleProductSKUs,
           ConfigurableProductSKUs = itemModel.ConfigurableProductSKUs,
           GroupProducts = IsNotNull(orderDataModel) ? null : itemModel.GroupProducts,
           QuantityOnHand = itemModel.QuantityOnHand,
           SeoPageName = itemModel.SeoPageName,
           ProductCode = itemModel.ProductCode,
           TrackingNumber = itemModel.TrackingNumber,
           UOM = itemModel.UOM,
           TrackInventory = itemModel.TrackInventory,
           AllowBackOrder = itemModel.AllowBackOrder,
           IsEditStatus = itemModel.IsEditStatus,
           IsSendEmail = itemModel.IsSendEmail,
           ShipSeperately = itemModel.ShipSeperately,
           OmsQuoteId = itemModel.OmsQuoteId,
           OmsQuoteLineItemId = itemModel.OmsQuoteLineItemId,
           ChildProductId = itemModel.ChildProductId,
           OmsOrderLineItemsId = itemModel.OmsOrderLineItemsId,
           OmsOrderStatusId = orderDataModel?.OrderLineItemStatusId > 0 ? orderDataModel.OrderLineItemStatusId : itemModel.OmsOrderStatusId,
           OrderLineItemStatus = orderDataModel?.OrderLineItemStatusId > 0 ? orderDataModel.OrderLineItemStatus : itemModel.OrderLineItemStatus,
           ParentOmsQuoteLineItemId = itemModel.ParentOmsQuoteLineItemId,
           PersonaliseValuesDetail = itemModel.PersonaliseValuesDetail,
           OrderLineItemRelationshipTypeId = itemModel.OrderLineItemRelationshipTypeId,
           Sequence = itemModel.Sequence,
           CustomText = itemModel.CustomText,
           CartAddOnDetails = itemModel.CartAddOnDetails,
           Product = itemModel.Product,
           ShippingAddress = itemModel.ShippingAddress,
           MultipleShipToAddress = itemModel.MultipleShipToAddress,
           TaxCost = IsNotNull(orderDataModel) && cartTaxCost > 0 ? (itemModel.TaxCost / orderDataModel.Quantity) * orderDataModel.CustomQuantity : itemModel.TaxCost,
           PersonaliseValuesList = itemModel.PersonaliseValuesList,
           IsShippingReturn = IsNotNull(orderDataModel) ? orderDataModel.IsShippingReturn : itemModel.IsShippingReturn,
           PartialRefundAmount = IsNotNull(orderDataModel?.PartialRefundAmount) ? orderDataModel.PartialRefundAmount : itemModel.PartialRefundAmount,
           Custom1 = itemModel.Custom1,
           Custom2 = itemModel.Custom2,
           Custom3 = itemModel.Custom3,
           Custom4 = itemModel.Custom4,
           Custom5 = itemModel.Custom5,
           AdditionalCost = itemModel.AdditionalCost
       } : new ShoppingCartItemModel();

        //Get Return Line Item
        protected virtual ReturnOrderLineItemModel GetReturnLineItem(ShoppingCartItemModel cartItemModel, ManageOrderDataModel orderDataModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(cartItemModel))
            {
                ReturnOrderLineItemModel returnLitem = new ReturnOrderLineItemModel();
                returnLitem.Description = cartItemModel.Description;
                returnLitem.ExtendedPrice = orderDataModel?.CustomQuantity > 0 ? cartItemModel.UnitPrice * orderDataModel.CustomQuantity : cartItemModel.ExtendedPrice;
                returnLitem.ProductId = cartItemModel.ProductId;
                returnLitem.Quantity = orderDataModel?.CustomQuantity > 0 ? orderDataModel.CustomQuantity : cartItemModel.Quantity;
                returnLitem.ShippingCost = cartItemModel.ShippingCost / orderDataModel?.Quantity * returnLitem.Quantity ?? 0;
                returnLitem.ProductDiscountAmount = cartItemModel.ProductDiscountAmount / orderDataModel?.Quantity * returnLitem.Quantity ?? 0;;
                returnLitem.ShippingOptionId = cartItemModel.ShippingOptionId;
                returnLitem.SKU = cartItemModel.SKU;
                returnLitem.UnitPrice = cartItemModel.UnitPrice;
                returnLitem.ExternalId = cartItemModel.ExternalId;
                returnLitem.CartDescription = cartItemModel.CartDescription;
                returnLitem.CurrencyCode = cartItemModel.CurrencyCode;
                returnLitem.ImagePath = cartItemModel.ImagePath;
                returnLitem.MediaConfigurationId = cartItemModel.MediaConfigurationId;
                returnLitem.ProductName = cartItemModel.ProductName;
                returnLitem.ProductType = cartItemModel.ProductType;
                returnLitem.ImageMediumPath = cartItemModel.ImageMediumPath;
                returnLitem.AddOnProductSKUs = cartItemModel.AddOnProductSKUs;
                returnLitem.BundleProductSKUs = cartItemModel.BundleProductSKUs;
                returnLitem.ConfigurableProductSKUs = cartItemModel.ConfigurableProductSKUs;
                returnLitem.ProductCode = cartItemModel.ProductCode;
                returnLitem.TrackingNumber = cartItemModel.TrackingNumber;
                returnLitem.UOM = cartItemModel.UOM;
                returnLitem.ChildProductId = cartItemModel.ChildProductId;
                returnLitem.IsEditStatus = cartItemModel.IsEditStatus;
                returnLitem.ShipSeperately = cartItemModel.ShipSeperately;
                returnLitem.OmsOrderStatusId = cartItemModel.OmsOrderStatusId;
                returnLitem.OrderLineItemStatus = !string.IsNullOrEmpty(orderDataModel.OrderLineItemStatus) ? orderDataModel.OrderLineItemStatus : cartItemModel.OrderLineItemStatus;
                returnLitem.CustomText = cartItemModel.CustomText;
                returnLitem.TaxCost = cartItemModel.TaxCost;
                returnLitem.PersonaliseValuesList = cartItemModel.PersonaliseValuesList;
                returnLitem.PersonaliseValuesDetail = cartItemModel.PersonaliseValuesDetail;
                returnLitem.ReasonForReturn = orderDataModel.ReasonForReturn;
                returnLitem.ReasonForReturnId = orderDataModel.ReasonForReturnId;
                returnLitem.GroupProducts = GetCustomGroupProductItems(cartItemModel, orderDataModel);
                returnLitem.OmsOrderLineItemsId = cartItemModel.OmsOrderLineItemsId;
                returnLitem.AddOnLineItemId = cartItemModel.AddOnLineItemId;
                returnLitem.Custom1 = cartItemModel.Custom1;
                returnLitem.Custom2 = cartItemModel.Custom2;
                returnLitem.Custom3 = cartItemModel.Custom3;
                returnLitem.Custom4 = cartItemModel.Custom4;
                returnLitem.Custom5 = cartItemModel.Custom5;
                returnLitem.IsShippingReturn = orderDataModel.IsShippingReturn;
                returnLitem.PartialRefundAmount = orderDataModel.PartialRefundAmount;
                returnLitem.AdditionalCost = cartItemModel.AdditionalCost;
                ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return returnLitem;
            }
            return null;
        }

        //Get the list of Group product for custom cart item.
        protected virtual List<AssociatedProductModel> GetCustomGroupProductItems(ShoppingCartItemModel shoppingCartItem, ManageOrderDataModel orderDataModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("ShoppingCartItemModel with GroupProducts count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { GroupProductsCount = shoppingCartItem?.GroupProducts?.Count });
            List<AssociatedProductModel> groupProducts = new List<AssociatedProductModel>();
            if (shoppingCartItem?.GroupProducts?.Count > 0)
            {
                foreach (var groupProduct in shoppingCartItem.GroupProducts)
                {
                    AssociatedProductModel product = new AssociatedProductModel();
                    product.Sequence = groupProduct.Sequence;
                    product.ProductId = groupProduct.ProductId;
                    product.OmsQuoteId = groupProduct.OmsQuoteId;
                    product.OmsTemplateId = groupProduct.OmsTemplateId;
                    product.OmsQuoteLineItemId = groupProduct.OmsQuoteLineItemId;
                    product.OmsTemplateLineItemId = groupProduct.OmsTemplateLineItemId;
                    product.ParentOmsQuoteLineItemId = groupProduct.ParentOmsQuoteLineItemId;
                    product.ParentOmsTemplateLineItemId = groupProduct.ParentOmsTemplateLineItemId;
                    product.OrderLineItemRelationshipTypeId = groupProduct.OrderLineItemRelationshipTypeId;
                    product.Quantity = orderDataModel?.CustomQuantity > 0 ? orderDataModel.CustomQuantity : groupProduct.Quantity;
                    product.UnitPrice = groupProduct.UnitPrice;
                    product.MinimumQuantity = groupProduct.MinimumQuantity;
                    product.MaximumQuantity = groupProduct.MaximumQuantity;
                    product.Sku = groupProduct.Sku;
                    product.ExternalId = groupProduct.ExternalId;
                    product.CustomText = groupProduct.CustomText;
                    product.ProductName = groupProduct.ProductName;
                    product.CurrencyCode = groupProduct.CurrencyCode;
                    product.InStockMessage = groupProduct.InStockMessage;
                    product.CartAddOnDetails = groupProduct.CartAddOnDetails;
                    product.BackOrderMessage = groupProduct.BackOrderMessage;
                    product.InventoryMessage = groupProduct.InventoryMessage;
                    product.OutOfStockMessage = groupProduct.OutOfStockMessage;
                    groupProducts.Add(product);
                }
            }
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return groupProducts;
        }

        //Set Expand For Order Details.
        protected virtual ExpandCollection SetExpandForOrderDetails(bool isManageOrder = false)
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString());
            if (!isManageOrder)
                expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString());
            expands.Add(ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.ZnodeUser);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString());
            expands.Add(ExpandKeys.IsFromOrderReceipt);
            ZnodeLogging.LogMessage("ExpandForOrderDetails: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { Expands = expands });
            return expands;
        }

        //Validate Order Update
        protected virtual bool ValidateOrderUpdate(OrderModel orderModel)
        {
            if (orderModel.OrderLineItemHistory.Count > 0 || orderModel.OrderHistory.Count > 0 || !string.IsNullOrEmpty(orderModel.AdditionalInstructions) || !string.IsNullOrEmpty(orderModel.ExternalId))
            {
                if (orderModel.ShoppingCartModel?.ShoppingCartItems?.Count > 0)
                    return true;
                else if (orderModel.ShoppingCartModel?.ShoppingCartItems?.Count < 1 && orderModel.ReturnItemList?.ReturnItemList?.Count > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }

        //Map store filter values in view model
        protected virtual void BindStoreFilterValues(RMAReturnListViewModel returnListViewModel, int portalId, string portalName)
        {
            returnListViewModel.PortalName = string.IsNullOrEmpty(portalName) ? Admin_Resources.DefaultAllStores : portalName;
            returnListViewModel.PortalId = portalId;
        }

        //Set return list data in ReturnListViewModel.
        protected virtual void SetReturnListData(RMAReturnViewModel returns)
        {
            if (IsNotNull(returns))
            {
                returns.ReturnDateWithTime = returns.ReturnDate.ToTimeFormat();
                returns.ReturnTotalWithCurrency = HelperMethods.FormatPriceWithCurrency(returns.TotalReturnAmount, returns.CultureCode);
            }
        }

        // Remove key from dictionary.
        protected virtual void RemoveKeyFromDictionaryForReturn(RMAReturnViewModel returnViewModel, string key, bool isFromLineItem = false)
        {
            if (IsNotNull(returnViewModel?.ReturnHistory) && !isFromLineItem)
            {
                if (returnViewModel.ReturnHistory.ContainsKey(key))
                    returnViewModel.ReturnHistory.Remove(key);
            }
            else
            {
                if (IsNotNull(returnViewModel.ReturnLineItemHistory) && returnViewModel.ReturnLineItemHistory.ContainsKey(key))
                    returnViewModel.ReturnLineItemHistory.Remove(key);
            }
        }

        // Add key and value in dictionary for return.
        protected virtual void ReturnHistory(RMAReturnViewModel returnViewModel, string settingType, string newValue = "")
            => returnViewModel.ReturnHistory?.Add(settingType, newValue);

        // Add key and value in return line dictionary.
        protected virtual void ReturnLineItemHistory(RMAReturnViewModel returnViewModel, string key, RMAReturnLineItemHistoryModel lineItemHistory)
            => returnViewModel.ReturnLineItemHistory?.Add(key, lineItemHistory);

        // Add key and value in order line dictionary.
        protected virtual void OrderLineItemHistory(OrderModel orderModel, string key, OrderLineItemHistoryModel lineHistory) => orderModel.OrderLineItemHistory?.Add(key, lineHistory);

        // Update Return line item history
        protected virtual void UpdateReturnLineItemHistory(RMAReturnViewModel returnViewModel, RMAReturnLineItemViewModel returnLineItemModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(returnLineItemModel) && IsNotNull(returnViewModel))
            {
                string lineitemHistorykey = returnLineItemModel.Sku + "_" + returnLineItemModel.RmaReturnLineItemsId;

                RMAReturnLineItemViewModel oldReturnLineItemModel = returnViewModel.OldReturnLineItems?.FirstOrDefault(x => x.RmaReturnLineItemsId == returnLineItemModel.RmaReturnLineItemsId);
                if (IsNotNull(oldReturnLineItemModel))
                {
                    RemoveKeyFromDictionaryForReturn(returnViewModel, lineitemHistorykey, true);

                    RMAReturnLineItemHistoryModel returnLineItemHistoryModel = new RMAReturnLineItemHistoryModel();

                    returnLineItemHistoryModel.SKU = returnLineItemModel.Sku;
                    returnLineItemHistoryModel.RmaReturnLineItemsId = returnLineItemModel.RmaReturnLineItemsId;
                    returnLineItemHistoryModel.ProductName = returnLineItemModel.ProductName + " " + returnLineItemModel?.Description.Replace("<br />", " ");
                    returnLineItemHistoryModel.Total = returnLineItemModel.Total;
                    returnLineItemHistoryModel.ReturnShippingAmount = returnLineItemModel.ShippingCost ?? 0;

                    if (oldReturnLineItemModel.RmaReturnStateId != returnLineItemModel.RmaReturnStateId)
                        returnLineItemHistoryModel.ReturnUpdatedStatus = returnLineItemModel.ReturnStatus;

                    if (IsNull(oldReturnLineItemModel.ReturnedQuantity) && oldReturnLineItemModel.ExpectedReturnQuantity != returnLineItemModel.ReturnedQuantity && returnLineItemModel.RmaReturnStateId != (int)ZnodeReturnStateEnum.REJECTED)
                        returnLineItemHistoryModel.ReturnedQuantity = returnLineItemModel.ReturnedQuantity.ToString();
                    else if (IsNotNull(oldReturnLineItemModel.ReturnedQuantity) && oldReturnLineItemModel.ReturnedQuantity != returnLineItemModel.ReturnedQuantity && returnLineItemModel.RmaReturnStateId != (int)ZnodeReturnStateEnum.REJECTED)
                        returnLineItemHistoryModel.ReturnedQuantity = returnLineItemModel.ReturnedQuantity.ToString();

                    if (oldReturnLineItemModel.RefundAmount != returnLineItemModel.RefundAmount)
                        returnLineItemHistoryModel.PartialRefundAmount = HelperMethods.FormatPriceWithCurrency(returnLineItemModel.RefundAmount, returnViewModel.CultureCode);

                    if (oldReturnLineItemModel.IsShippingReturn != returnLineItemModel.IsShippingReturn)
                        returnLineItemHistoryModel.IsShippingReturn = returnLineItemModel.IsShippingReturn ? ZnodeConstant.True : ZnodeConstant.False;

                    ReturnLineItemHistory(returnViewModel, lineitemHistorykey, returnLineItemHistoryModel);
                }
            }
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }
        //Refund Process
        protected virtual bool RefundProcess(OrderModel orderModel, decimal overDueAmount, out string errorMessage)
        {
            errorMessage = string.Empty;
            bool status = false;

            if (IsNotNull(orderModel))
            {
                _orderAgent = DependencyResolver.Current.GetService<IOrderAgent>();

                if (String.Equals(orderModel.PaymentType, ZnodeConstant.CreditCard, StringComparison.OrdinalIgnoreCase) &&
                    String.Equals(orderModel.OrderState, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    //Void or Refund transactions
                    status = _orderAgent.VoidRefundPayment(orderModel, false, out errorMessage, orderModel.OmsOrderId);
                }

                if (String.Equals(orderModel.PaymentType, ZnodeConstant.CreditCard, StringComparison.OrdinalIgnoreCase) && overDueAmount < 0)
                {
                    //Refund order line item transactions
                    status = _orderAgent.RefundPaymentByAmount(orderModel.OmsOrderDetailsId, orderModel.PaymentTransactionToken, overDueAmount, out errorMessage);
                    errorMessage = status ? Admin_Resources.PaymentRefundSuccessMessage : errorMessage;
                }

                if (String.Equals(orderModel.PaymentType, ZnodeConstant.PAYPAL_EXPRESS, StringComparison.OrdinalIgnoreCase) &&
                    string.Equals(orderModel.OrderState, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    status = _orderAgent.RefundPaymentByGiftCard(orderModel.OmsOrderDetailsId, orderModel.PaymentTransactionToken, overDueAmount);
                }

                //Amazon refund.
                if (String.Equals(orderModel.PaymentType, ZnodeConstant.Amazon_Pay, StringComparison.OrdinalIgnoreCase) && overDueAmount < 0)
                {
                    //Refund order line item transactions
                    status = _orderAgent.RefundPaymentByAmount(orderModel.OmsOrderDetailsId, orderModel.PaymentTransactionToken, overDueAmount, out errorMessage);
                    errorMessage = status ? Admin_Resources.PaymentRefundSuccessMessage : errorMessage;
                }

                //Amazon refund for cancel order.
                if (String.Equals(orderModel.PaymentType, ZnodeConstant.Amazon_Pay, StringComparison.OrdinalIgnoreCase) &&
                   String.Equals(orderModel.OrderState, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    //Void or Refund transactions
                    status = _orderAgent.VoidRefundPayment(orderModel, false, out errorMessage, orderModel.OmsOrderId);
                }
            }
            return status;
        }

        //Create Refund History
        protected virtual void CreateRefundHistory(RMAReturnViewModel returnViewModel, string errorMessage, string transactionId, decimal overDueAmount)
        {
            List<RMAReturnHistoryModel> returnHistoryModelList = new List<RMAReturnHistoryModel>();
            returnHistoryModelList.Add(new RMAReturnHistoryModel
            {
                RmaReturnDetailsId = returnViewModel.RmaReturnDetailsId,
                Message = errorMessage,
                TransactionId = transactionId,
                ReturnAmount = overDueAmount < 0 ? overDueAmount : -(overDueAmount)
            });
            _rmaReturnClient.CreateReturnHistory(returnHistoryModelList);
        }

        //Set expand for Return Details.
        protected virtual ExpandCollection SetExpandForReturnDetails()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ExpandKeys.ReturnItemList);
            expands.Add(ExpandKeys.ReturnShippingDetails);
            expands.Add(ExpandKeys.ReturnProductImages);
            expands.Add(ExpandKeys.ReturnBarcode);
            ZnodeLogging.LogMessage("Expand for return details: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { Expands = expands });
            return expands;
        }

        //Save return notes
        protected virtual bool SaveReturnNotes(RMAReturnViewModel returnViewModel, string notes)
        {
            if (IsNotNull(returnViewModel) && !string.IsNullOrEmpty(notes))
            {
                int userId = Convert.ToInt32(SessionProxyHelper.GetUserDetails()?.UserId);
                return _rmaReturnClient.SaveReturnNotes(new RMAReturnNotesModel()
                {
                    RmaReturnDetailsId = returnViewModel.RmaReturnDetailsId,
                    Notes = notes,
                    CreatedBy = userId,
                    ModifiedBy = userId,
                });
            }
            else
                return false;
        }

        //Set portal details in return view model
        protected virtual void SetPortalDetails(RMAReturnViewModel returnViewModel)
        {
            if (IsNotNull(returnViewModel) && returnViewModel?.PortalId > 0)
            {
                PortalModel portalModel = GetClient<PortalClient>().GetPortal(returnViewModel.PortalId, null);
                returnViewModel.CustomerServiceEmail = portalModel?.CustomerServiceEmail;
                returnViewModel.CustomerServicePhoneNumber = portalModel?.CustomerServicePhoneNumber;
                returnViewModel.StoreName = portalModel?.StoreName;
            }
        }

        //Update Return Shipping
        protected virtual void UpdateReturnShippingHistory(OrderModel orderModel, ManageOrderDataModel orderDataModel, RMAReturnLineItemViewModel returnLineItem, bool isInsert = false)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderModel = orderModel, ManageOrderDataModel = orderDataModel, IsInsert = isInsert });

            List<OrderLineItemHistoryModel> orderLineHistoryList = orderModel.OrderLineItemHistory.Select(s => (OrderLineItemHistoryModel)s.Value).ToList();

            if (IsNotNull(orderModel) && IsNotNull(orderDataModel))
            {
                decimal? shippingCost = orderDataModel?.ShippingCost;

                OrderLineItemHistoryModel orderLineItemHistoryModel = new OrderLineItemHistoryModel()
                {
                    OmsOrderLineItemsId = orderDataModel.OmsOrderLineItemsId,
                    OrderLineQuantity = string.Empty,
                    OrderTrackingNumber = string.Empty,
                    Quantity = string.Empty,
                    PartialRefundAmount = string.Empty,
                    IsShippingReturn = true,
                    ProductName = (returnLineItem?.ProductName ?? string.Empty),
                    ReturnShippingAmount = (shippingCost ?? 0).ToString(),
                    SKU = (returnLineItem?.Sku ?? string.Empty).ToString(),
                };

                orderLineItemHistoryModel.ReturnShippingAmount = HelperMethods.FormatPriceWithCurrency((isInsert == true) ? (shippingCost ?? 0) : 0, orderModel.CultureCode);

                if (orderLineHistoryList?.Count() == 0 || IsNull(orderLineHistoryList))
                {
                    if (isInsert && shippingCost > 0)
                        OrderLineItemHistory(orderModel, returnLineItem.Sku, orderLineItemHistoryModel);
                }
                else
                {
                    DependencyResolver.Current.GetService<IOrderAgent>().RemoveKeyFromDictionary(orderModel, "Shipping_" + returnLineItem.Sku, true);
                    if (isInsert && shippingCost > 0)
                        OrderLineItemHistory(orderModel, "Shipping_" + returnLineItem.Sku, orderLineItemHistoryModel);
                }
            }
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Bind Default return status and line item status on the basis of return state and return line item state
        protected virtual List<SelectListItem> BindManageReturnStatus(FilterTuple filter = null)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            FilterCollection filters = new FilterCollection();
            if (filter != null) { filters.Add(filter); }
            ZnodeLogging.LogMessage("Filters to get order states: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { Filters = filters });
            return ToReturnStateList(_rmaReturnClient.GetReturnStatusList(null, filters, null, null, null)?.ReturnStates);
        }
        #endregion
    }
}