﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Controllers
{
    public class ContentWidgetController : BaseController
    {

        #region Private Variables
        private readonly IContentWidgetAgent _contentWidgetAgent;
        private readonly IGlobalAttributeEntityAgent _globalAttributeEntityAgent;
        #endregion

        #region Public Constructor
        public ContentWidgetController(IContentWidgetAgent contentWidgetAgent, IGlobalAttributeEntityAgent globalAttributeEntiyAgent)
        {
            _contentWidgetAgent = contentWidgetAgent;
            _globalAttributeEntityAgent = globalAttributeEntiyAgent;

        }
        #endregion

        //Get the List of content Widgets
        public virtual ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int entityId = 0, string entityType = null)
        {
            //Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies(GridListType.ZnodeCMSContentWidget.ToString(), model);

            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeCMSContentWidget.ToString(), model);

            //Get the list of Content Widget list.
            ContentWidgetListViewModel contentWidgetList = _contentWidgetAgent.List(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //Get the grid model.
            contentWidgetList.GridModel = FilterHelpers.GetDynamicGridModel(model, contentWidgetList.ContentWidgets, GridListType.ZnodeCMSContentWidget.ToString(), string.Empty, null, true, true, contentWidgetList?.GridModel?.FilterColumn?.ToolMenuList);
            contentWidgetList.GridModel.TotalRecordCount = contentWidgetList.TotalResults;

            //Returns the content widget list.
            return ActionView(AdminConstants.ListView, contentWidgetList);

        }

        //Creat content Widget
        [HttpGet]
        public virtual ActionResult Create()
        {
            ContentWidgetViewModel model = new ContentWidgetViewModel();
            _contentWidgetAgent.BindContentWidgetModel(model);
            return ActionView(AdminConstants.CreateEdit, model);
        }

        //Create content Widget
        [HttpPost]
        public virtual ActionResult Create(ContentWidgetViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _contentWidgetAgent.Create(model);
                TempData[AdminConstants.Notifications] = GenerateNotificationMessages(Admin_Resources.ContentWidgetCreate, NotificationType.success);
                return RedirectToAction<ContentWidgetController>(x => x.Edit(model.WidgetKey));
            }
            TempData[AdminConstants.Notifications] = GenerateNotificationMessages(Admin_Resources.ContentWidgetCreateError, NotificationType.error);
            return ActionView(AdminConstants.CreateEdit, model);
        }

        //Edit Content Widget
        public virtual ActionResult Edit(string WidgetKey)
        {
            ActionResult action = GotoBackURL();
            if (action != null)
                return action;

            return ActionView(AdminConstants.CreateEdit, _contentWidgetAgent.Edit(WidgetKey));
        }

        //Edit Content Widget
        [HttpPost]
        public virtual ActionResult Edit(ContentWidgetViewModel model)
        {
            ContentWidgetViewModel viewModel = _contentWidgetAgent.Update(model);
            TempData[AdminConstants.Notifications] = viewModel.HasError
                ? GenerateNotificationMessages(Admin_Resources.ContentWidgetUpdateError, NotificationType.error)
                : GenerateNotificationMessages(Admin_Resources.ContentWidgetUpdate, NotificationType.success);
            return RedirectToAction<ContentWidgetController>(x => x.Edit(viewModel.WidgetKey));
        }

        //Variants Associated to the Content Widget
        public virtual ActionResult GetVariants(int contentWidgetId)
        {
            WidgetVariantViewModel variant = new WidgetVariantViewModel();
            variant.Locales = _contentWidgetAgent.GetAvailableLocales();
            variant.ContentWidgetId = contentWidgetId;
            return ActionView("AssociateVariant", variant);
        }

        //Get Profiles which are added as a variant for widget
        public virtual JsonResult GetUnassociatedProfiles(string widgetKey, int localeId)
        {
            List<SelectListItem> profile = _contentWidgetAgent.GetUnassociatedProfiles(widgetKey, localeId);
            return Json(new { ProfileList = profile }, JsonRequestBehavior.AllowGet);
        }

        //Associate Variant to a widget
        [HttpPost]
        public virtual JsonResult AssociateVariants(WidgetVariantViewModel variant)
        {
            List<WidgetVariantViewModel> variants = _contentWidgetAgent.AssociateVariant(variant);
            return Json(new { VariantList = variants }, JsonRequestBehavior.AllowGet);

        }

        [HttpPut]
        public virtual JsonResult AssociateWidgetTemplate(int variantId, int widgetTemplateId)
        {
            return Json(new { status = _contentWidgetAgent.AssociateWidgetTemplate(variantId, widgetTemplateId) }, JsonRequestBehavior.AllowGet);

        }

        //Get Associated Attribute Details
        public virtual ActionResult GetEntityAttributeDetails(int entityId, string entityType)
            => ActionView("WidgetAttributes", _globalAttributeEntityAgent.GetEntityAttributeDetails(entityId, entityType));

        //Save Attribute Details
        [HttpPost]
        public virtual ActionResult SaveEntityDetails([ModelBinder(typeof(ControlsModelBinder))] BindDataModel model)
        {

            string errorMessage = string.Empty;

            EntityAttributeViewModel entityAttributeViewModel = _globalAttributeEntityAgent.SaveEntityAttributeDetails(model, out errorMessage);
            SetNotificationMessage(entityAttributeViewModel.IsSuccess ? GetSuccessNotificationMessage(Admin_Resources.UpdateMessage) : GetErrorNotificationMessage(errorMessage));

            return RedirectToAction<ContentWidgetController>(x => x.GetEntityAttributeDetails(entityAttributeViewModel.EntityValueId, entityAttributeViewModel.EntityType));
        }

        //Delete associated widget variant
        public virtual JsonResult DeleteAssociatedVariant(int variantId, string widgetKey)
        {
            string message = string.Empty;
            if (variantId > 0)
            {
                bool status = _contentWidgetAgent.DeleteAssociatedVariant(variantId, widgetKey, out message);
                return Json(new { status = status, message = status ? Admin_Resources.DeleteMessage : message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, message = message }, JsonRequestBehavior.AllowGet);
        }

        //Delete Content Widget
        public virtual JsonResult Delete(string contentWidgetId)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(contentWidgetId))
            {
                bool status = _contentWidgetAgent.DeleteContentWidget(contentWidgetId, out message);
                return Json(new { status = status, message = status ? Admin_Resources.SuccessContentWidgetDelete : message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, message = message }, JsonRequestBehavior.AllowGet);
        }

        //Verify if the widget Exist
        [HttpGet]
        public virtual ActionResult IsWidgetExist(string widgetKey)
           => Json(new { data = _contentWidgetAgent.IsWidgetExist(widgetKey) }, JsonRequestBehavior.AllowGet);
    }
}
