﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Controllers
{
    public class WidgetTemplateController : BaseController
    {
        #region Private Variable
        private readonly IWidgetTemplateAgent _widgetTemplateAgent;
        #endregion

        #region Constructor
        public WidgetTemplateController(IWidgetTemplateAgent widgetTemplateAgent)
        {
            _widgetTemplateAgent = widgetTemplateAgent;
        }
        #endregion

        //List of Widget Template
        public virtual ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            FilterHelpers.GetSetFiltersFromCookies(GridListType.ZnodeCMSWidgetTemplate.ToString(), model);

            FilterHelpers.GetDefaultView(GridListType.ZnodeCMSWidgetTemplate.ToString(), model);

            WidgetTemplateListViewModel widgetTemplateList = _widgetTemplateAgent.List(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            widgetTemplateList.GridModel = FilterHelpers.GetDynamicGridModel(model, widgetTemplateList.WidgetTemplates, GridListType.ZnodeCMSWidgetTemplate.ToString(), string.Empty, null, true, true, widgetTemplateList?.GridModel?.FilterColumn?.ToolMenuList);

            widgetTemplateList.GridModel.TotalRecordCount = widgetTemplateList.TotalResults;
            return ActionView(widgetTemplateList);
        }

        //Create Widget Template
        [HttpGet]
        public virtual ActionResult Create()
            => View(AdminConstants.CreateEdit, new WidgetTemplateViewModel());

        //Create Widget Template
        [HttpPost]
        public virtual ActionResult Create(WidgetTemplateViewModel widgetTemplateViewModel)
        {
            if (ModelState.IsValid)
            {
                if (HelperUtility.IsNull(widgetTemplateViewModel.FilePath))
                {
                    SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorRequiredFile));
                    return View(AdminConstants.CreateEdit, widgetTemplateViewModel);
                }

                widgetTemplateViewModel = _widgetTemplateAgent.Create(widgetTemplateViewModel);
                if (widgetTemplateViewModel?.WidgetTemplateId > 0)
                {
                    SetNotificationMessage(GetSuccessNotificationMessage(Admin_Resources.WidgetTemplateCreate));
                    return RedirectToAction<WidgetTemplateController>(x => x.Edit(widgetTemplateViewModel.Code));
                }
            }
            SetNotificationMessage(GetErrorNotificationMessage(widgetTemplateViewModel.ErrorMessage));
            return View(AdminConstants.CreateEdit, widgetTemplateViewModel);
        }

        //Get Widget Template
        [HttpGet]
        public virtual ActionResult Edit(string code)
        {
            ActionResult action = GotoBackURL();
            if (action != null)
                return action;
            return View(AdminConstants.CreateEdit, _widgetTemplateAgent.GetWidgetTemplate(code));
        }

        //Update Widget Template
        [HttpPost]
        public virtual ActionResult Edit(WidgetTemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                
                model = _widgetTemplateAgent.Update(model);
                if (model?.WidgetTemplateId > 0)
                {
                    SetNotificationMessage(GetSuccessNotificationMessage(Admin_Resources.WidgetTemplateUpdate));
                    return RedirectToAction<WidgetTemplateController>(x => x.Edit(model.Code));
                }
            }
            return View(AdminConstants.CreateEdit, model);
        }

        //Delete Widget Template
        public virtual JsonResult Delete(string widgetTemplateId, string fileName)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(widgetTemplateId))
            {
                bool status = _widgetTemplateAgent.DeleteWidgetTemplate(widgetTemplateId, fileName, out message);
                return Json(new { status = status, message = status ? Admin_Resources.DeleteWidgetTemplate : message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, message = message }, JsonRequestBehavior.AllowGet);
        }

        //Copy Widget Template
        [HttpGet]
        public virtual ActionResult Copy(string code)
        {
            if (string.IsNullOrEmpty(code))
                return RedirectToAction<WidgetTemplateController>(x => x.List(null));

            WidgetTemplateViewModel templateViewModel = _widgetTemplateAgent.GetWidgetTemplate(code);
            
            ModelState.Clear();
            templateViewModel.Name = "Copy of " + templateViewModel.Name;
            templateViewModel.Code = string.Empty;
            templateViewModel.WidgetTemplateId = 0;
            return ActionView(AdminConstants.CreateEdit, templateViewModel);
        }

        //Copy Widget Template
        [HttpPost]
        public virtual ActionResult Copy(WidgetTemplateViewModel model)
        {
            ModelState.Remove("FilePath");
            if (ModelState.IsValid)
            {
                model = _widgetTemplateAgent.CopyWidgetTemplate(model);
                if (model?.WidgetTemplateId > 0)
                {
                    SetNotificationMessage(GetSuccessNotificationMessage(Admin_Resources.CopyMessage));
                    return RedirectToAction<WidgetTemplateController>(x => x.Edit(model.Code));
                }
            }
            SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.TemplateNameAlreadyExist));
            return ActionView(AdminConstants.CreateEdit, model);
        }

        //Download Widget Template
        public virtual ActionResult DownloadWidgetTemplate(int widgetTemplateId, string fileName)
        {
            string filePath = _widgetTemplateAgent.DownloadWidgetTemplate(widgetTemplateId, fileName);
            if (!string.IsNullOrEmpty(filePath))
            {
                // Read the contents of file in byte array and download the file.
                byte[] fileBytes = System.IO.File.ReadAllBytes($"{filePath}/{fileName}.cshtml");
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName + ".cshtml");
            }
            SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorDownloadTemplate));
            return RedirectToAction<WidgetTemplateController>(x => x.List(null));
        }

        //Validate Widget Template
        [HttpGet]
        public virtual ActionResult IsWidgetTemplateExist(string code)
            => Json(new { data = _widgetTemplateAgent.IsWidgetTemplateExist(code) }, JsonRequestBehavior.AllowGet);
    }
}
