﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;

namespace Znode.Engine.Admin.Controllers
{
    public class AnalyticsController : BaseController
    {
        #region Private Variables
        private readonly IAnalyticsAgent _analyticsAgent;
        #endregion

        #region Public Constructor
        public AnalyticsController(IAnalyticsAgent analyticsAgent)
        {
            _analyticsAgent = analyticsAgent;
        }
        #endregion       

        #region Public Methods
        //Method to get analytics dashboard
        public virtual ActionResult AnalyticsDashboard() => View(_analyticsAgent.GetAnalyticsDashboardData());
        #endregion
    }
}
