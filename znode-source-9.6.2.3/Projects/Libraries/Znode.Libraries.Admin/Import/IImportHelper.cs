﻿using GenericParsing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Libraries.Admin.Import
{
   public interface IImportHelper
    {

        #region Public Methods
        //This method will process the data uploaded from the file.
        int ProcessData(ImportModel model, int userId);

        //Update the data of ZnodeImportHead table
        int UpdateTemplateData(ImportModel model);

        //check the import status
        bool CheckImportStatus();

        //This method will process the data uploaded from the file.
        int ProcessProductUpdateData(ImportModel model, int userId, out string importedGuid);
        #endregion
    }
}
