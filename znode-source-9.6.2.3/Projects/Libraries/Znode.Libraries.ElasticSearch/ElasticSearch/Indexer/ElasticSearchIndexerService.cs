﻿using Nest;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Admin;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Search;

using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Libraries.ElasticSearch
{
    public class ElasticSearchIndexerService : IElasticSearchIndexerService
    {
        #region Protected Variables

        protected readonly ISearchProductService searchProductService;

        #endregion Private Variables

        #region Public Properties

        public int PageLength { get; set; }

        #endregion Public Properties

        #region Public constructor

        public ElasticSearchIndexerService()
        {
            searchProductService = GetService<ISearchProductService>(); 
        }

        public ElasticSearchIndexerService(int pageLength = 1000)
        {
            searchProductService = GetService<ISearchProductService>(); 
            PageLength = pageLength;
        }

        #endregion Public constructor

        #region Public Methods

        //Returns the product descriptor.
        public virtual CreateIndexDescriptor AddProductDocuments(ElasticClient elasticClient, IndexName indexName, int publishCatalogId)
        {
            CreateIndexDescriptor productDescriptor = GetCreateIndexDescriptor(indexName, publishCatalogId);
            return productDescriptor;
        }

        //Create elastic index for the catalog data.
        public virtual void CreateIndex(ElasticClient elasticClient, IndexName indexName, SearchParameterModel searchParameterModel)
        {
            ZnodeLogging.LogMessage("Create index execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            int pageIndex = 1;
            int totalRecordCount = 0;
            decimal totalPages = 0;
            int pageSize = ZnodeApiSettings.PublishProductFetchChunkSize;

            //Ensure that the elastic index schema exist, if not exist then create it. #Step 1
            EnsureIndexSchemaExists(elasticClient, indexName, searchParameterModel);

            //Get all active locales. #Step 2
            int[] localeIds = searchParameterModel.ActiveLocales?.Select(x => x.LocaleId).ToArray();

            //Get all version id of the current catalog id for all locales. #Step 3
            int[] latestVersionIds = searchProductService.GetLatestVersionId(searchParameterModel.CatalogId, searchParameterModel.revisionType, localeIds);

            //Get all categories id associated to the catalog. #Step 4
            IEnumerable<int> publishcategoryIds = GetService<ISearchCategoryService>().GetPublishCategoryList(searchParameterModel.CatalogId, latestVersionIds)?.Select(x => x.ZnodeCategoryId)?.Distinct();

            //Get all publish product count. #Step 5
            totalRecordCount = searchProductService.GetAllProductCount(searchParameterModel.CatalogId, latestVersionIds);
            
            totalPages = totalRecordCount != 0 ? totalRecordCount < pageSize ? 1 : Math.Ceiling((decimal)totalRecordCount / pageSize) : 0;

            ZnodeLogging.LogMessage($"Total products record count : {totalRecordCount} and total page count: {totalPages} for indexing ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            SearchHelper searchHelper = new SearchHelper();

            try
            {
                //Save InProgress status for the server name. #Step 6
                searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexInProgressStatus });

                while (pageIndex <= totalPages)
                {
                    List<dynamic> productsData = new List<dynamic>();

                    //Get publish product data in the require form for the elastic search indexing process. #Step 7
                    productsData = GetPublishProductData(searchParameterModel, latestVersionIds, publishcategoryIds, pageIndex, pageSize);

                    //Generate elastic search index for the given data. #Step 8
                    GenerateElasticSearchIndex(elasticClient, searchParameterModel, indexName, productsData);

                    pageIndex++;
                }

                //Save complete status for the server name. #Step 9
                searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexCompleteStatus });
                
                ZnodeLogging.LogMessage("Create index execution done", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("DSODE-202-Publish Elastic PageIndex check End =:" + pageIndex, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error, ex);
                searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexFailedStatus });
                ZnodeLogging.LogMessage("Search index server status updated to Failed", ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                throw;
            }
        }

        //Ensure that the elastic index schema exist, if not exist then create it
        protected virtual void EnsureIndexSchemaExists(ElasticClient elasticClient, IndexName indexName, SearchParameterModel searchParameterModel)
        {
            ZnodeLogging.LogMessage("Ensure index execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            SearchHelper searchHelper = new SearchHelper();
            if (!IsIndexExists(elasticClient, indexName))
            {
                CreateIndexDescriptor createIndexDescriptor = AddProductDocuments(elasticClient, indexName, searchParameterModel.CatalogId);
                CreateIndexResponse indexResponse = elasticClient.Indices.Create(indexName, i => createIndexDescriptor);

                if (!indexResponse.IsValid)
                {
                    searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexFailedStatus });
                    throw new Exception($"Elastic index schema creataion is failed. {indexResponse.OriginalException.Message}");
                }
            }
            ZnodeLogging.LogMessage("Ensure index execution done", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Get publish product data in the require form for the elastic search indexing process
        public virtual List<dynamic> GetPublishProductData(SearchParameterModel searchParameterModel, int[] latestVersionIds, IEnumerable<int> publishcategoryIds, int pageIndex, int pageSize)
        {
            ZnodeLogging.LogMessage("Get publish product data execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("latestVersionIds, pageIndex, pageSize : ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { latestVersionIds, pageIndex, pageSize });

            //Get all product associated to the catalog categories. #Step 1
            List<SearchProduct> searchProductList = searchProductService.GetAllProducts(searchParameterModel.CatalogId, latestVersionIds, publishcategoryIds, searchParameterModel.IndexStartTime, pageIndex, pageSize);

            //Convert product list to appropriate model for elastic search. #Step 2
            List<dynamic> productsData = ConvertProductDataToElasticModel(searchProductList);

            ZnodeLogging.LogMessage("Get publish product data execution done", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            return productsData;
        }

        //Generate elastic search index for the data
        public virtual void GenerateElasticSearchIndex(ElasticClient elasticClient, SearchParameterModel searchParameterModel, IndexName indexName, List<dynamic> productsData)
        {
            ZnodeLogging.LogMessage("Elastic search index process execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            try
            {
                //Get Elastic indexing wait time. #Step 1
                double elasticIndexProcessWaitTime = ZnodeApiSettings.ElasticCatalogIndexWaitTimeMinute;

                int chunkSize = Convert.ToInt32(ZnodeApiSettings.SearchIndexChunkSize);
                int processorCount = Math.Max(1, Convert.ToInt32(Environment.ProcessorCount / 2));
                CancellationTokenSource tokenSource = new CancellationTokenSource();
                ConcurrentBag<dynamic> failedDocuments = new ConcurrentBag<dynamic>();

                /*Insert all product document in elastic search
                 * Wait for the completion of process using Wait function #Step 2 */

                BulkAllObservable<dynamic> observableBulkResponse = elasticClient.BulkAll(productsData, f => f
                                   .MaxDegreeOfParallelism(processorCount)
                                   .ContinueAfterDroppedDocuments()
                                   .DroppedDocumentCallback((item, product) => failedDocuments.Add(product))
                                   .BackOffTime(TimeSpan.FromMilliseconds(10))
                                   .BackOffRetries(1)
                                   .Size(chunkSize)
                                   .RefreshOnCompleted()
                                   .Index(indexName), tokenSource.Token);

                //Wait till completion of elastic bulk index operation. #Step 3
                observableBulkResponse.Wait(TimeSpan.FromMinutes(elasticIndexProcessWaitTime), x => { });

                ZnodeLogging.LogMessage("Elastic search index process execution done", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error, ex);
                throw new Exception("Elastic search index creation process is failed", ex);
            }
        }

        public virtual void CreateDocuments(List<ZnodePublishProductEntity> pagedProducts, ElasticClient elasticClient, string indexName)
        {
            if (pagedProducts?.Count > 0)
            {
                long indexStartTime = DateTime.Now.Ticks;
                List<dynamic> productsData = ConvertProductDataToElasticModel(searchProductService.GetElasticProducts(pagedProducts, indexStartTime));
                if (productsData?.Count > 0)
                {
                    List<List<dynamic>> productDataInChunks = HelperUtility.SplitCollectionIntoChunks<dynamic>(productsData, Convert.ToInt32(ZnodeApiSettings.SearchIndexChunkSize));

                    Parallel.For(0, productDataInChunks.Count(), index =>
                    {
                        BulkResponse indexResponse = elasticClient.IndexMany<dynamic>(productDataInChunks[index], indexName);

                        if (!indexResponse.IsValid)
                            throw new Exception($"Create Index failed. {indexResponse.ItemsWithErrors?.First()?.Error?.CausedBy?.Reason}");
                    });
                }
            }
        }


        public virtual List<dynamic> ConvertProductDataToElasticModel(List<SearchProduct> pagedProducts)
        {
            List<dynamic> productsData = new List<dynamic>();

            if (pagedProducts?.Count > 0)
            {
                DataTable products = ToDataTable(pagedProducts.OrderBy(x => x.name).ToList());

                foreach (var item in products.AsEnumerable())
                {
                    // Expando objects are IDictionary<string, object>
                    IDictionary<string, object> dn = new ExpandoObject();

                    foreach (var column in products.Columns.Cast<DataColumn>())
                        dn[column.ColumnName] = !string.IsNullOrEmpty(item[column].ToString()) ? item[column] : null;

                    productsData.Add(dn);
                }
            }
            return productsData;
        }

        //Delete elastic search index.
        public virtual bool DeleteIndex(ElasticClient elasticClient, string indexName)
        {
            DeleteIndexResponse response = elasticClient.Indices.Delete(indexName);
            if (response.IsValid)
                return true;
            else
            {
                ZnodeLogging.LogMessage(response.ServerError.Error.ToString(), ZnodeLogging.Components.Search.ToString());
                if (response.ServerError.Status == 404)
                    throw new ZnodeException(ErrorCodes.NotFound, "Index does not exist.");
                return false;
            }
        }

        public virtual bool WriteSynonymsFile(ElasticClient elasticClient, int publishCatalogId, string indexName, bool isSynonymsFile, bool isAllDeleted = false)
        {
            bool result = true;
            NodesInfoResponse nodeInfo = elasticClient.Nodes.Info();
            var pathDynamicValues = nodeInfo.Nodes[(nodeInfo.Nodes.FirstOrDefault().Key)].Settings["path"];
            string configPath = pathDynamicValues["home"].Value.ToString() + Path.DirectorySeparatorChar + "config";

            if (isSynonymsFile)
                result = WriteSynonymsTextFile(elasticClient, publishCatalogId, indexName, configPath, isAllDeleted);
            return result;
        }

        //Delete unaffected products from search index
        public virtual bool DeleteProductData(ElasticClient elasticClient, IndexName indexName, long indexStartTime)
        {
            //Delete request for delete documents which does not match to current create index time.
            DeleteByQueryRequest request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    MustNot = new QueryContainer[] { new TermQuery { Field = "timestamp", Value = indexStartTime.ToString() } },
                })
            };

            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected products from search index
        public virtual bool DeleteProductDataByRevisionType(ElasticClient elasticClient, IndexName indexName, string revisionType, long indexStartTime)
        {
            //Delete request for delete documents which does not match to current create index time.
            DeleteByQueryRequest request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new MatchQuery { Field = "revisiontype", Query = revisionType } },
                    MustNot = new QueryContainer[] { new TermQuery { Field = "timestamp", Value = indexStartTime.ToString() } },
                }
                )
            };

            var response = elasticClient.DeleteByQuery(request);

            return response?.IsValid ?? false; ;
        }

        public virtual bool RenameIndexData(ElasticClient elasticClient, int catalogIndexId, string oldIndexName, string newIndexName)
        {
            if (IsIndexExists(elasticClient, oldIndexName))
            {
                {
                    var reindexResponse = elasticClient.ReindexOnServer(r => r
          .Source(s => s
              .Index(oldIndexName)
           )
          .Destination(d => d
              .Index(oldIndexName)
          )
          .WaitForCompletion(true)
      );

                    DeleteIndexResponse deleteIndex = elasticClient.Indices.Delete(oldIndexName);
                    return true;
                }
            }
            else
                return false;
        }

        //Delete unaffected products from search index
        public virtual bool DeleteProduct(ElasticClient elasticClient, IndexName indexName, string znodeProductIds, string revisionType)
        {
            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new TermQuery { Field = ZnodeConstant.ZnodeProductId, Value = znodeProductIds } },
                })
            };

            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected products from search index
        public virtual bool DeleteProducts(ElasticClient elasticClient, IndexName indexName, IEnumerable<object> productIds, string revisionType, string versionId)
        {
            ///Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new TermsQuery { Field = ZnodeConstant.ZnodeProductId, Terms = productIds } ,
                    new MatchQuery { Field = "revisiontype", Query = revisionType } ,
                    new MatchQuery { Field = "versionid", Query = versionId } },
                })
            };
            var response = elasticClient.DeleteByQuery(request);
            return response?.IsValid ?? false;
        }

        //Delete unaffected category name from search index
        public virtual bool DeleteCategoryForGivenIndex(ElasticClient elasticClient, IndexName indexName, int categoryId)
        {
            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new TermQuery { Field = ZnodeConstant.PublishCategoryId.ToLower(), Value = categoryId.ToString() } },
                })
            };
            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected catalog category products from search index
        public virtual bool DeleteCatalogCategoryProducts(ElasticClient elasticClient, IndexName indexName, int publishCatalogId, List<int> publishCategoryIds, string revisionType, string versionId, long indexStartTime = 0)
        {
            IEnumerable<object> categoryIds = publishCategoryIds.Cast<object>();

            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new MatchQuery { Field = "versionid", Query = versionId }, new MatchQuery { Field = "revisiontype", Query = revisionType }, new TermsQuery { Field = ZnodeConstant.PublishCategoryId, Terms = categoryIds } },
                    MustNot = new QueryContainer[] { new TermQuery { Field = "timestamp", Value = indexStartTime.ToString() } },
                })
            };
            DeleteByQueryResponse deleteByQueryResponse = elasticClient.DeleteByQuery(request);
            return deleteByQueryResponse?.IsValid ?? false;
        }


        //Checks if the index exists.
        public virtual bool IsIndexExists(ElasticClient elasticClient, IndexName indexName)
        {
            IndexExistsRequest request = new IndexExistsRequest(indexName);
            ExistsResponse result = elasticClient.Indices.Exists(indexName);
            return result.Exists;
        }

        //Set Analyzer for search settings
        public virtual void SetAnalyzers(List<string> tokenFilters, IndexSettings indexsettings)
        {
            SetSearchTimeAnalyzers(tokenFilters, indexsettings);
            SetIndexTimeAnalyzers(tokenFilters, indexsettings);
        }

        //Set search time analyzers for index.
        public virtual void SetSearchTimeAnalyzers(List<string> tokenFilters, IndexSettings indexsettings)
        {
            indexsettings.Analysis.Analyzers.Add("standard_search_synonym_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("standard", tokenFilters));
            indexsettings.Analysis.Analyzers.Add("autocomplete_search_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("keyword", new List<string> { "lowercase" }));
        }

        //Set index time analyzers for index.
        public virtual void SetIndexTimeAnalyzers(List<string> tokenFilters, IndexSettings indexsettings)
        {
            List<string> ngramAnlyzerFilter = tokenFilters.ToList();
            ngramAnlyzerFilter.Insert(ngramAnlyzerFilter.FindIndex(x => x == "edgeNgramTokenFilter"), "ngramtokenfilter");
            indexsettings.Analysis.Analyzers.Add("standard_ngram_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("standard", ngramAnlyzerFilter));
            indexsettings.Analysis.Analyzers.Add("didYouMean_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("standard", tokenFilters, new List<string> { "html_strip" }));
            indexsettings.Analysis.Analyzers.Add("lowercase_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("keyword", new List<string> { "lowercase" }));
            indexsettings.Analysis.Analyzers.Add("autocomplete_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("ngram", new List<string> { "lowercase" }));
        }

        //Set filter for analyzers
        public virtual void SetFilterForAnalyzers(List<string> tokenFilters, IndexSettings indexsettings, int publishCatalogId)
        {
            SetSynonymTokenFilter(tokenFilters, indexsettings, publishCatalogId);

            SetStopWordsFilter(tokenFilters, indexsettings);

            SearchIndexSettingHelper.SetNgramTokenFilter(indexsettings);

            SearchIndexSettingHelper.SetEdgeNgramFilter(tokenFilters, indexsettings);

            SearchIndexSettingHelper.SetShingleTokenFilter(tokenFilters, indexsettings);

            SetAutoCompleteFilter(indexsettings);
        }

        //Set Stopwords filter for analyzers.
        public virtual void SetStopWordsFilter(List<string> tokenFilters, IndexSettings indexsettings)
        {
            if (ZnodeApiSettings.EnableStopWordsForSearchIndex)
            {
                indexsettings.Analysis.TokenFilters.Add("stopwords", new StopTokenFilter
                {
                    IgnoreCase = true,
                    StopWords = "_english_"
                });
                tokenFilters.Add("stopwords");
            }
        }

        //Set  edge ngram token filter for autocomplete filter.
        public virtual void SetAutoCompleteFilter(IndexSettings indexsettings)
        {
            indexsettings.Analysis.TokenFilters.Add("autocomplete_filter", new EdgeNGramTokenFilter
            {
                MinGram = 1,
                MaxGram = 10
            });
        }


        //Delete elastic search index.
        public virtual bool DeleteElasticSearchIndex(ElasticClient elasticClient, string indexName)
        {
            if (IsIndexExists(elasticClient, indexName))
            {
                DeleteIndexResponse response = elasticClient.Indices.Delete(indexName);

                if (response.IsValid)
                    ZnodeLogging.LogMessage($"{indexName} index deleted successfully", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
                else
                    ZnodeLogging.LogMessage($"{indexName} index deletion failed", ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);

                return response.IsValid;
            }
            else
            {
                ZnodeLogging.LogMessage($"{indexName} index does not exist", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            }
            return true;
        }
        //Creates create index descriptor.
        public virtual CreateIndexDescriptor GetCreateIndexDescriptor(IndexName indexName, int publishCatalogId)
        {
            IndexSettings settings = CreateIndexSettings(publishCatalogId);

            //We have set the field limit to 5000.
            settings.Add("index.mapping.total_fields.limit", 5000);

            return ConfigureIndex(indexName, settings);
        }

        public virtual CreateIndexDescriptor ConfigureIndex(IndexName indexName, IndexSettings settings)
        {
            return new CreateIndexDescriptor(indexName)
          .InitializeUsing(new IndexState() { Settings = settings })
          .Mappings(mappings => mappings.Map<dynamic>("_default_", s => s.DateDetection(false).DynamicTemplates(dts => dts
          .DynamicTemplate("strings", dt => dt.Match("*")
          .PathUnmatch("attributes.*")
          .MatchMappingType("string").Mapping(t => t
          .Text(d => d
          .Fields(fi => fi.Text(te => te.Name("lowercase")
          .Analyzer("lowercase_analyzer")).Keyword(ss => ss.Name("keyword")).Text(at => at.Name("autocomplete").Analyzer("autocomplete_analyzer").Fielddata(true))))))
          .DynamicTemplate("nested", nest => nest.PathMatch("attributes.*").MatchMappingType("string").Mapping(x => x.Keyword(te => te.Index(false)))))
          .Properties(propes => propes.Text(productname => productname.Name("productname")
           .Fields(f => f.Keyword(ss => ss.Name("keyword")).Text(te => te.Name("lowercase").Analyzer("lowercase_analyzer").Fielddata(true)).Text(at => at.Name("autocomplete").Analyzer("autocomplete_analyzer").Fielddata(true)))
           .Analyzer("standard_ngram_analyzer")
          .SearchAnalyzer("standard_search_synonym_analyzer")).Text(sku => sku.Name("sku").Fields(f => f.Keyword(ss => ss.Name("keyword"))
          .Text(te => te.Name("lowercase")
          .Analyzer("lowercase_analyzer")).Text(at => at.Name("autocomplete").Analyzer("autocomplete_analyzer").Fielddata(true)))
           .Analyzer("standard_ngram_analyzer")
          .SearchAnalyzer("standard_search_synonym_analyzer")).Text(brand => brand.Name("brand").Fields(f => f.Keyword(ss => ss.Name("keyword"))
          .Text(te => te.Name("lowercase")
          .Analyzer("lowercase_analyzer")).Text(at => at.Name("autocomplete").Analyzer("autocomplete_analyzer").Fielddata(true)))
          .Analyzer("standard_ngram_analyzer")
          .SearchAnalyzer("standard_search_synonym_analyzer")).Text(did => did.Name("didyoumean").Analyzer("didYouMean_analyzer")))));
        }

        //Creates index setting.
        public virtual IndexSettings CreateIndexSettings(int publishCatalogId)
        {
            //Get default token filters.
            List<string> tokenFilters = ZnodeApiSettings.DefaultTokenFilters.Split(',').ToList();

            //create analysis object
            IndexSettings indexsettings = new IndexSettings
            {
                Analysis = new Analysis
                {
                    TokenFilters = new TokenFilters(),
                    Analyzers = new Analyzers(),
                    Tokenizers = new Tokenizers()
                }
            };
            //The default difference between MinNGram and MaxNGram is set as 1 to set max difference need to add difference in index setting. 
            indexsettings.Add(UpdatableIndexSettings.MaxNGramDiff, 39);

            //The default difference between MaxShingleDiff and MaxShingleDiff is set as 1 to set max difference need to add difference in index setting. 
            indexsettings.Add(UpdatableIndexSettings.MaxShingleDiff, 10);

            SetFilterForAnalyzers(tokenFilters, indexsettings, publishCatalogId);

            SearchIndexSettingHelper.SetTokenizers(indexsettings);

            SetAnalyzers(tokenFilters, indexsettings);

            return indexsettings;
        }

        //Gets synonym token filter.
        public virtual void SetSynonymTokenFilter(List<string> tokenFilters, IndexSettings indexsettings, int publishCatalogId = 0, bool isPublishSynonyms = false, bool isAllDeleted = false)
        {
            List<ZnodeSearchSynonym> synonymList = searchProductService.GetSynonymsData(publishCatalogId);

            List<string> formattedSynonymList = new List<string>();

            if (isAllDeleted == false && isPublishSynonyms && synonymList.Count <= 0)
                throw new ZnodeException(ErrorCodes.ProcessingFailed, "no record found for processing synonyms.");

            if (isAllDeleted)
                formattedSynonymList.Add("");

            if (synonymList.Count > 0)
            {
                foreach (ZnodeSearchSynonym item in synonymList)
                {
                    if (item.IsBidirectional.GetValueOrDefault())
                    {
                        formattedSynonymList.Add($"{item.OriginalTerm},{item.ReplacedBy}");
                    }
                    else
                        formattedSynonymList.Add($"{item.OriginalTerm} => {item.ReplacedBy}");
                }
            }

            if (formattedSynonymList?.Count > 0 && Equals(ZnodeApiSettings.UseSynonym, "true") && formattedSynonymList?.Count > 0)
            {
                //Create synonym token filter.
                SynonymTokenFilter synonymFilter = new SynonymTokenFilter();
                synonymFilter.Synonyms = formattedSynonymList;
                //Add synonym token filter in indexsetting.
                indexsettings.Analysis.TokenFilters = new TokenFilters();
                indexsettings.Analysis.TokenFilters.Add("synonym", synonymFilter);
                tokenFilters.Add("synonym");
            }
        }

        //Write Synonyms file
        public virtual bool WriteSynonymsTextFile(ElasticClient elasticClient, int publishCatalogId, string indexName, string configPath, bool isAllDeleted = false)
        {
            try
            {
                if (IsIndexExists(elasticClient, indexName))
                {

                    IndexSettings indexsettings = new IndexSettings
                    {
                        Analysis = new Analysis
                        {
                            TokenFilters = new TokenFilters(),
                            Analyzers = new Analyzers(),
                        }
                    };

                    //Get default token filters.
                    List<string> tokenFilters = ZnodeApiSettings.DefaultTokenFilters.Split(',').ToList();

                    SetSynonymTokenFilter(tokenFilters, indexsettings, publishCatalogId, true, isAllDeleted);

                    SetStopWordsFilter(tokenFilters, indexsettings);

                    SearchIndexSettingHelper.SetEdgeNgramFilter(tokenFilters, indexsettings);

                    SearchIndexSettingHelper.SetShingleTokenFilter(tokenFilters, indexsettings);

                    elasticClient.Indices.Close(indexName);

                    indexsettings.Analysis.Analyzers.Add("my_standard_synonym_analyzer", SearchIndexSettingHelper.SetCustomAnalyzer("standard", tokenFilters));


                    UpdateIndexSettingsResponse updateSettings = elasticClient.Indices.UpdateSettings(new UpdateIndexSettingsRequest(indexName) { IndexSettings = indexsettings });

                    elasticClient.Indices.Open(indexName);

                    return true;
                }
                else
                    throw new ZnodeException(ErrorCodes.NotFound, "Search index does not exist.");
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error, ex);
                throw;
            }
        }

        public virtual DataTable ToDataTable(List<SearchProduct> products)
        {
            DataTable tmp = new DataTable();

            //Tables created with columns
            foreach (SearchProduct product in products)
            {
                foreach (SearchAttributes attribute in product.searchableattributes)
                {
                    DataColumnCollection columns = tmp.Columns;
                    if (!columns.Contains(attribute.attributecode))
                    {
                        if (attribute.isfacets)
                        {
                            tmp.Columns.Add(attribute.attributecode.ToLower(), typeof(string[]));
                            continue;
                        }
                        else if (attribute.attributetypename == "Number")
                        {
                            tmp.Columns.Add(attribute.attributecode.ToLower(), typeof(double));
                            continue;
                        }
                        else
                            tmp.Columns.Add(attribute.attributecode.ToLower());
                    }
                }
            }
            tmp.Columns.Add("znodecatalogid", typeof(int));
            tmp.Columns.Add("localeid", typeof(int));
            tmp.Columns.Add("categoryid", typeof(int));
            tmp.Columns.Add("categoryname");
            tmp.Columns.Add("znodeproductid", typeof(int));
            tmp.Columns.Add(ZnodeConstant.productPrice, typeof(decimal));
            tmp.Columns.Add("isactive", typeof(bool));
            tmp.Columns.Add("productindex", typeof(int));
            tmp.Columns.Add("displayorder", typeof(int));
            tmp.Columns.Add("timestamp", typeof(long));
            tmp.Columns.Add("autocompleteproductname");
            tmp.Columns.Add("autocompletesku");
            tmp.Columns.Add("didyoumean");
            tmp.Columns.Add("name");
            tmp.Columns.Add("versionid", typeof(int));
            tmp.Columns.Add("rating", typeof(decimal));
            tmp.Columns.Add("totalreviewcount", typeof(int));
            tmp.Columns.Add("attributes", typeof(object));
            tmp.Columns.Add("revisiontype", typeof(string));
            tmp.Columns.Add("brands", typeof(string[]));
            tmp.Columns.Add("salesprice");
            tmp.Columns.Add("retailprice");
            tmp.Columns.Add("culturecode");
            tmp.Columns.Add("currencycode");
            tmp.Columns.Add("currencysuffix");
            tmp.Columns.Add("seodescription");
            tmp.Columns.Add("seokeywords");
            tmp.Columns.Add("seotitle");
            tmp.Columns.Add("seourl");
            tmp.Columns.Add("imagesmallpath");
            //Add data of each product
            foreach (SearchProduct product in products)
            {
                try
                {
                    DataRow dr = tmp.NewRow();
                    foreach (SearchAttributes attribute in product.searchableattributes)
                    {
                        try
                        {
                            foreach (DataColumn column in tmp.Columns)
                            {
                                if (
                                    (attribute.attributecode.Equals(ZnodeConstant.OutOfStockOptions, StringComparison.InvariantCultureIgnoreCase) && column.ColumnName.Equals(ZnodeConstant.OutOfStockOptions, StringComparison.InvariantCultureIgnoreCase))
                                    ||
                                    (attribute.attributecode.Equals(ZnodeConstant.ProductType, StringComparison.InvariantCultureIgnoreCase) && column.ColumnName.Equals(ZnodeConstant.ProductType, StringComparison.InvariantCultureIgnoreCase)))
                                {
                                    if (attribute.selectvalues.Count > 0)
                                        dr[column.ColumnName] = attribute.selectvalues.FirstOrDefault()?.code;
                                    continue;
                                }
                                if (attribute.attributecode.Equals(ZnodeConstant.Brand, StringComparison.InvariantCultureIgnoreCase)
                                    && column.ColumnName.Equals(ZnodeConstant.Brand, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    dr[column.ColumnName] = attribute.selectvalues?.Select(y => y.code).ToArray();
                                    continue;
                                }// Added if condition to add brandcode in place of brandname to fetch product using brandcode 

                                if (attribute.attributecode.Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    if (attribute.selectvalues?.Count > 0)
                                    {
                                        if (column.DataType == typeof(string[]))
                                            dr[column.ColumnName] = attribute.selectvalues.Select(x => x.value).ToArray();
                                        else
                                            dr[column.ColumnName] = attribute.selectvalues.FirstOrDefault().value;
                                    }
                                    else if (column.DataType == typeof(double))
                                        dr[column.ColumnName] = string.IsNullOrEmpty(attribute.attributevalues) ? 0 : Convert.ToDouble(attribute.attributevalues);
                                    else
                                        dr[column.ColumnName] = attribute.attributevalues?.TrimEnd();
                                    continue;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ZnodeLogging.LogMessage($"Error occurred for product sku: {product.sku} and attribute code : {attribute.attributecode}, error details are {ex.Message}", "Elasticsearch", TraceLevel.Error);
                            ZnodeLogging.LogMessage(ex.Message, "Elasticsearch", TraceLevel.Error, ex);
                        }
                    }

                    if (product.brands.Count > 0) dr["brands"] = product.brands.Select(x => x.brandcode).ToArray();
                    dr["znodecatalogid"] = product.catalogid;
                    dr["localeid"] = product.localeid;
                    dr["categoryid"] = product.categoryid;
                    dr["categoryname"] = product.categoryname;
                    dr["znodeproductid"] = product.znodeproductid;
                    dr["isactive"] = product.isactive;
                    dr["productindex"] = product.productindex;
                    dr["rating"] = product.rating;
                    dr["totalreviewcount"] = product.totalreviewcount;
                    dr["displayorder"] = product.displayorder;
                    dr["name"] = product.name;
                    dr["timestamp"] = product.timestamp;
                    dr["autocompleteproductname"] = product.name;
                    dr["autocompletesku"] = product.sku;
                    dr["didyoumean"] = product.name;
                    dr["versionid"] = product.version;
                    dr["attributes"] = product.attributes;
                    dr["revisiontype"] = product.revisionType;
                    dr[ZnodeConstant.productPrice] = product.productprice;
                    dr["salesprice"] = product.salesprice;
                    dr["retailprice"] = product.retailprice;
                    dr["culturecode"] = product.culturecode;
                    dr["currencycode"] = product.currencycode;
                    dr["currencysuffix"] = product.currencysuffix;
                    dr["seodescription"] = product.seodescription;
                    dr["seokeywords"] = product.seokeywords;
                    dr["seotitle"] = product.seotitle;
                    dr["seourl"] = product.seourl;
                    dr["imagesmallpath"] = product.imagesmallpath;
                    tmp.Rows.Add(dr);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage($"Error occurred for product sku: {product.sku}, error details are {ex.Message}", "Elasticsearch", TraceLevel.Error);
                    ZnodeLogging.LogMessage(ex.Message, "Elasticsearch", TraceLevel.Error, ex);
                }
            }

            return tmp;
        }

        #endregion Public Methods
    }
}