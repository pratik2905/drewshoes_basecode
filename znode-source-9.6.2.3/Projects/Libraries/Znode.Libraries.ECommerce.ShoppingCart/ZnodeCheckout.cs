﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;


namespace Znode.Libraries.ECommerce.ShoppingCart
{
    // Order Checkout Class - Order the checkout process
    public class ZnodeCheckout : ZnodeBusinessBase, IZnodeCheckout
    {
        #region Member Variables
        private readonly int _ShippingID = 0;
        private readonly IZnodeOrderHelper orderHelper;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly Dictionary<int, string> publishCategory = new Dictionary<int, string>();
        #endregion

        #region Constructor
        public ZnodeCheckout()
        {
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }

        // Initializes a new instance of the ZNodeCheckout class.
        public ZnodeCheckout(UserAddressModel userAccount, ZnodePortalCart shoppingCart)
        {

            this.UserAccount = userAccount;
            this.ShoppingCart = shoppingCart;
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }

        #endregion

        #region Public Properties
        // Gets or sets a value indicating whether it is success or not
        public bool IsSuccess { get; set; }

        // Gets or sets the PaymentResponse Text
        public string PaymentResponseText { get; set; }

        // Gets or sets the user account 
        public UserAddressModel UserAccount { get; set; }

        // Gets or sets the shopping cart 
        public ZnodePortalCart ShoppingCart { get; set; }

        // Gets or sets the Customer Additional instructions for this order
        public string AdditionalInstructions { get; set; }

        // Gets or sets the purchase order number applied by customer,
        public string PurchaseOrderNumber { get; set; }

        // Gets or sets the purchase order document name uploaded by customer,
        public string PoDocument { get; set; }

        // Gets or sets the shipping id
        public int ShippingID { get; set; }

        // Gets or sets the portal id
        public int PortalID { get; set; }

        #endregion

        #region  public virtual Methods

        // to submits order
        public virtual ZnodeOrderFulfillment SubmitOrder(SubmitOrderModel model, ShoppingCartModel shoppingCartModel, bool isTaxCostUpdated = true)
        {

            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder ZnodeOrderFulfillment started =:" + ShoppingCart.PortalID+"OrderNo "+ model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            int portalID = ShoppingCart.PortalID;
            int orderId = model?.OrderId.GetValueOrDefault() ?? 0;
            int orderDetailId = 0;
            ZnodeOrderFulfillment order = this.GetOrderFullfillment(this.UserAccount, this.ShoppingCart, portalID);
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder GetOrderFullfillment  =:" + order.AccountNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            order.Order.OmsOrderDetailsId = model?.OmsOrderDetailsId == null ? 0 : Convert.ToInt32(model?.OmsOrderDetailsId);
            order.AccountNumber = shoppingCartModel?.Shipping?.AccountNumber;
            order.ShippingMethod = shoppingCartModel?.Shipping?.ShippingMethod;
            if (IsNotNull(shoppingCartModel?.Payment?.PaymentStatusId))
                order.PaymentStatusID = shoppingCartModel.Payment.PaymentStatusId;
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder  Order PaymentStatusID   =:" + shoppingCartModel?.Payment?.PaymentStatusId + "OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (orderId > 0)
                SetOrderStateTrackingNumber(order, model);

            //start transaction
            using (SqlConnection connection = new SqlConnection(Data.Helpers.HelperMethods.ConnectionString))
            {
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder ZnodeOrderFulfillment sql connection start =:"+ "OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                connection.Open();     // create order object
                SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

                try
                {
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder ZnodeOrderFulfillment try start =:" + "OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    if (orderId > 0 && !CancelExistingOrder(order, orderId))
                        return order;

                    SetOrderAdditionalDetails(order, model);

                    bool paymentIsSuccess = SetPaymentDetails(order);
                    order.Order.OrderNumber = model?.OrderNumber;

                    // Add the order and line items to database
                    order.AddOrderToDatabase(order, shoppingCartModel);
                    if (orderId > 0)
                    {
                        //to save return items in data base for selected order
                        if (IsNotNull(model?.ReturnOrderLineItems))
                        {
                            this.IsSuccess = SaveReturnItems(order.Order.OmsOrderDetailsId, model.ReturnOrderLineItems);
                        }
                        ZnodeLogging.LogMessage($"Updated existing order for Order Id:{orderId }", ZnodeLogging.Components.OMS.ToString());
                    }

                    //to get current orderdetailid for verifing  order process
                    orderDetailId = order?.Order?.OmsOrderDetailsId ?? 0;

                    //Set Order Shipment Details to order Line Item.
                    SetOrderShipmentDetails(order);

                    if (paymentIsSuccess)
                    {
                        SetOrderDetailsToShoppingCart(order);
                        //to apply promotion, taxes and shipping calculation 
                        this.ShoppingCart.PostSubmitOrderProcess(orderId, shoppingCartModel.ShippingAddress.IsGuest, isTaxCostUpdated);

                        int? userId = this.ShoppingCart.GetUserId();

                        //to save referral commission and gift card history
                        this.SaveReferralCommissionAndGiftCardHistory(order, userId);

                        //reduce product inventory this code block move out of transaction because all db call of entity type and this is sp call
                        if (this.ManageOrderInventory(order, this.ShoppingCart))
                        {
                            transaction.Commit();
                            this.IsSuccess = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            this.IsSuccess = false;
                        }
                    }
                    else
                    {
                        //// payment submission failed so rollback transaction
                        transaction.Rollback();
                        this.IsSuccess = false;
                    }
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder ZnodeOrderFulfillment try end =:" + "OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage("DSODE-97-SubmitOrder ZnodeOrderFulfillment catch ex =:"+ex + "OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    ZnodeLogging.LogMessage(ex.Message, string.Empty, TraceLevel.Error, ex);

                    if (IsNotNull(ex.InnerException))
                        ZnodeLogging.LogMessage(ex.InnerException.ToString(), string.Empty, TraceLevel.Error); // log exception

                    transaction.Rollback();
                    this.IsSuccess = false;

                    VerifySubmitOrderProcess(orderDetailId);

                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            VerifySubmitOrderProcess(orderDetailId);
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder End =:" + order.Order.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return order;
        }

        //This constructor initializes the order and OrderLineItem entity objects.
        public virtual ZnodeOrderFulfillment GetOrderFullfillment(UserAddressModel userAccount, ZnodePortalCart shoppingCart, int portalId)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetOrderFullfillment Started =:" + portalId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            ZnodeOrderFulfillment order = new ZnodeOrderFulfillment(shoppingCart);

            order.PortalId = portalId;

            SetOrderDetails(order, shoppingCart, userAccount);

            foreach (ZnodeMultipleAddressCart addressCart in shoppingCart.AddressCarts)
            {
                int? shippingId = addressCart.Shipping.ShippingID != 0 ? addressCart.Shipping.ShippingID : this._ShippingID;
                AddressModel address = userAccount?.ShippingAddress;
                address = IsNull(address) || Equals(address.AddressId, 0) ? shoppingCart.Payment?.ShippingAddress : address;
                addressCart.OrderShipmentID = CreateOrderShipment(address, shippingId, userAccount?.Email);

                SetOrderLineItems(order, addressCart);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetOrderFullfillment End =:" + order.PortalId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return order;
        }

        #endregion

        #region  public virtual Methods

        //to add order level discount to discount model
        public virtual void SetOrderDiscount(OrderModel model, decimal discountAmount, string discountCode, int discountType)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderDiscount Started OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (IsNotNull(model))
            {
                if (IsNull(model.OrdersDiscount) || model.OrdersDiscount?.Count == 0)
                {
                    model.OrdersDiscount = new System.Collections.Generic.List<OrderDiscountModel>();
                }
                model.OrdersDiscount.Add(new OrderDiscountModel
                {
                    OmsDiscountTypeId = discountType,
                    DiscountCode = discountCode,
                    DiscountAmount = discountAmount,
                    OriginalDiscount = discountAmount
                });
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderDiscount end OrderNo " + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to save OrderShipment data and return OrderShipmentId
        public virtual int CreateOrderShipment(AddressModel shippingAddress, int? shippingId, string emailId)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout CreateOrderShipment Start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return orderHelper.SaveShippingAddress(shippingAddress, shippingId, emailId);
        }

        //to set order details
        public virtual void SetOrderDetails(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart, UserAddressModel userAccount)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderDetails Start orderno " + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            order.UserID = userAccount.UserId;
            order.Created = GetDateTime();
            order.Modified = GetDateTime();
            order.CreatedBy = userAccount.UserId;
            order.ModifiedBy = userAccount.UserId;
            order.TaxCost = shoppingCart.TaxCost;
            order.VAT = shoppingCart.VAT;
            order.SalesTax = shoppingCart.SalesTax;
            order.HST = shoppingCart.HST;
            order.PST = shoppingCart.PST;
            order.GST = shoppingCart.GST;
            order.Email = userAccount.Email;
            order.CurrencyCode = shoppingCart.CurrencyCode;
            order.CultureCode = shoppingCart.CultureCode;
            order.ShippingCost = shoppingCart.ShippingCost;
            order.ShippingDifference = shoppingCart.ShippingDifference;
            order.SubTotal = shoppingCart.SubTotal;
            order.Total = shoppingCart.Total;
            order.ExternalId = shoppingCart.ExternalId;
            order.DiscountAmount = shoppingCart.Discount;
            order.BillingAddress = userAccount.BillingAddress;
            order.ShippingAddress = userAccount.ShippingAddress;
            order.OrderDate = shoppingCart.OrderDate.GetValueOrDefault();
            order.PaymentTrancationToken = shoppingCart.Token;
            order.CreditCardNumber = shoppingCart.CreditCardNumber;
            order.CardType = shoppingCart.CardType;
            order.CreditCardExpMonth = shoppingCart.CreditCardExpMonth;
            order.CreditCardExpYear = shoppingCart.CreditCardExpYear;
            order.IsShippingCostEdited = IsNotNull(shoppingCart.CustomShippingCost);
            order.IsTaxCostEdited = IsNotNull(shoppingCart.CustomTaxCost);
            order.Custom1 = shoppingCart.Custom1;
            order.Custom2 = shoppingCart.Custom2;
            order.Custom3 = shoppingCart.Custom3;
            order.Custom4 = shoppingCart.Custom4;
            order.Custom5 = shoppingCart.Custom5;
            order.PublishStateId = shoppingCart.PublishStateId;
            order.EstimateShippingCost = shoppingCart.EstimateShippingCost;
            order.IpAddress = shoppingCart.IpAddress;
            order.InHandDate = shoppingCart.InHandDate;
            order.JobName = shoppingCart.JobName;
            order.ShippingConstraintCode = shoppingCart.ShippingConstraintCode;

            order.PaymentDisplayName = shoppingCart?.Payment?.PaymentDisplayName;
            order.PaymentExternalId = shoppingCart?.Payment?.PaymentExternalId;
            foreach (ZnodeCoupon coupon in shoppingCart.Coupons)
            {
                if (coupon.CouponApplied && coupon.CouponValid)
                {
                    order.CouponCode = (!string.IsNullOrEmpty(order.CouponCode)) ? order.CouponCode += ZnodeConstant.CouponCodeSeparator + coupon.Coupon : coupon.Coupon;
                }
            }

            if(shoppingCart?.Vouchers?.Count > 0)
            {
                order.GiftCardNumber = string.Empty;
                foreach (ZnodeVoucher voucher in shoppingCart.Vouchers)
                {
                    if (voucher.IsVoucherApplied && voucher.IsVoucherValid)
                    {
                        order.GiftCardNumber = (!string.IsNullOrEmpty(order.GiftCardNumber)) ? order.GiftCardNumber += ZnodeConstant.CouponCodeSeparator + voucher.VoucherNumber : voucher.VoucherNumber;
                    }
                }
            }           
            SetOrderModel(order);
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderDetails end orderno " + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to set order model to be insert into database
        public virtual void SetOrderModel(ZnodeOrderFulfillment order)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderModel Start orderno " + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            OrderModel model = new OrderModel();
            model.UserId = order.UserID;
            model.CreatedDate = GetDateTime();
            model.ModifiedDate = GetDateTime();
            model.OrderDate = order.OrderDate;
            model.CreatedBy = order.UserID;
            model.ModifiedBy = order.UserID;
            model.TaxCost = order.TaxCost;
            model.BillingAddress.EmailAddress = order.Email;
            model.ShippingCost = order.ShippingCost;
            model.ShippingDifference = order.ShippingDifference;
            model.SubTotal = order.SubTotal;
            model.Total = order.Total;
            model.OverDueAmount = order.OrderOverDueAmount;
            model.DiscountAmount = order.DiscountAmount;
            model.BillingAddress.CityName = order.BillingAddress?.CityName;
            model.BillingAddress.DisplayName = order.BillingAddress?.DisplayName;
            model.BillingAddress.CountryName = order.BillingAddress?.CountryName;
            model.BillingAddress.FirstName = order.BillingAddress?.FirstName;
            model.BillingAddress.LastName = order.BillingAddress?.LastName;
            model.BillingAddress.PhoneNumber = order.BillingAddress?.PhoneNumber;
            model.BillingAddress.PostalCode = order.BillingAddress?.PostalCode;
            model.BillingAddress.StateCode = order.BillingAddress?.StateName;
            model.BillingAddress.Address1 = order.BillingAddress?.Address1;
            model.BillingAddress.Address2 = order.BillingAddress?.Address2;
            model.AdditionalInstructions = order.AdditionalInstructions;
            model.CouponCode = order.CouponCode;
            model.PortalId = order.PortalId;
            model.IsActive = true;
            model.PaymentTransactionToken = order.PaymentTrancationToken;
            model.CurrencyCode = order.CurrencyCode;
            model.CultureCode = order.CultureCode;
            model.PublishStateId = order.PublishStateId;
            //Set order tax to order.
            model.SalesTax = order.SalesTax;
            model.GST = order.GST;
            model.HST = order.HST;
            model.VAT = order.VAT;
            model.PST = order.PST;
            model.CreditCardNumber = order.CreditCardNumber;
            model.CardType = order.CardType;
            model.CreditCardExpMonth = order.CreditCardExpMonth;
            model.CreditCardExpYear = order.CreditCardExpYear;
            model.IsShippingCostEdited = order.IsShippingCostEdited;
            model.IsTaxCostEdited = order.IsTaxCostEdited;
            model.ExternalId = order.ExternalId;
            model.Custom1 = order.Custom1;
            model.Custom2 = order.Custom2;
            model.Custom3 = order.Custom3;
            model.Custom4 = order.Custom4;
            model.Custom5 = order.Custom5;
            model.EstimateShippingCost = order.EstimateShippingCost;

            model.PaymentDisplayName = order.PaymentDisplayName;
            model.PaymentExternalId = order.PaymentExternalId;
            model.IpAddress = order.IpAddress;
            model.InHandDate = order.InHandDate;
            model.JobName = order.JobName;
            model.ShippingConstraintCode = order.ShippingConstraintCode;
            //to set gift card discount to order model
            if (order.GiftCardAmount > 0)
            {
                SetOrderDiscount(model, order.GiftCardAmount, order.GiftCardNumber, (int)OrderDiscountTypeEnum.GIFTCARD);
            }

            //to set gift csr discount to order model
            if (order.CSRDiscountAmount > 0)
            {
                SetOrderDiscount(model, order.CSRDiscountAmount, order.CSRDiscountDescription, (int)OrderDiscountTypeEnum.CSRDISCOUNT);
            }

            //to set shipping discount to order model
            if (order.ShippingDiscount > 0)
            {
                SetOrderDiscount(model, order.ShippingDiscount, order.ShippingDiscountDescription, order.ShippingDiscountType);
            }

            order.Order.TotalAdditionalCost = order.Cart.TotalAdditionalCost;
            order.Order = model;
            model.GiftCardNumber = order.GiftCardNumber;
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderModel end orderno " + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to set order lineitems 
        public virtual void SetOrderLineItems(ZnodeOrderFulfillment order, ZnodeMultipleAddressCart addressCart)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderLineItems start orderno =:" + order.PurchaseOrderNumber + addressCart.ShoppingCartItems.Count, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            GetDistinctCategoryIdsforCartItem(addressCart);
            List<Data.DataModel.ZnodeOmsOrderLineItem> lineItemShippingDateList = orderHelper.GetLineItemShippingDate(order.Order.OmsOrderDetailsId);
            DateTime? ShipDate = DateTime.Now;
            // loop through cart and add line items
            foreach (ZnodeShoppingCartItem shoppingCartItem in addressCart.ShoppingCartItems)
            {
                OrderLineItemModel orderLineItem;

                if (string.IsNullOrEmpty(shoppingCartItem.GroupId))
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }
                else
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.Product.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }

                bool addNewOrderItem = false;
                if (IsNull(orderLineItem))
                {
                    addNewOrderItem = true;
                    orderLineItem = new OrderLineItemModel();
                    orderLineItem.OmsOrderShipmentId = addressCart.OrderShipmentID;
                    if (string.IsNullOrEmpty(shoppingCartItem.Product.ShoppingCartDescription) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.ShoppingCartDescription))
                        if (string.IsNullOrEmpty(shoppingCartItem.Product.Description) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.Description))
                            orderLineItem.Description = shoppingCartItem.Description;
                        else
                            orderLineItem.Description = shoppingCartItem.Product.Description;
                    else
                        orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                    orderLineItem.ProductName = shoppingCartItem.Product.Name;
                    orderLineItem.Sku = shoppingCartItem.Product.SKU;
                    orderLineItem.Quantity = ((shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) || (shoppingCartItem?.Product?.ZNodeGroupProductCollection.Count > 0)) ? 0 : shoppingCartItem.Quantity;
                    orderLineItem.Price = (shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) ? 0 : GetParentProductPrice(shoppingCartItem);
                    orderLineItem.DiscountAmount = GetLineItemDiscountAmount(shoppingCartItem.Product.DiscountAmount, shoppingCartItem.Quantity);
                    orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                    orderLineItem.ParentOmsOrderLineItemsId = null;
                    orderLineItem.DownloadLink = shoppingCartItem.Product.DownloadLink;
                    orderLineItem.GroupId = shoppingCartItem.GroupId;
                    orderLineItem.IsActive = true;
                    orderLineItem.Vendor = shoppingCartItem.Product.VendorCode;
                    orderLineItem.OrderLineItemStateId = shoppingCartItem.OrderStatusId;
                    orderLineItem.IsItemStateChanged = shoppingCartItem.IsItemStateChanged;
                    if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase) && shoppingCartItem.IsItemStateChanged)
                        orderLineItem.ShipDate = ShipDate;
                    else if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase))
                        orderLineItem.ShipDate = lineItemShippingDateList.Count > 0 ? lineItemShippingDateList.Find(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemId).ShipDate : null;
                    if (!string.IsNullOrEmpty(shoppingCartItem.TrackingNumber))
                        orderLineItem.TrackingNumber = shoppingCartItem.TrackingNumber;
                    //to apply custom tax/shipping cost
                    orderLineItem.IsLineItemShippingCostEdited = order.IsShippingCostEdited;
                    orderLineItem.IsLineItemTaxCostEdited = order.IsTaxCostEdited;
                    orderLineItem.PartialRefundAmount = shoppingCartItem.PartialRefundAmount;
                    //Assign Auto-add-on SKUs.
                    orderLineItem.AutoAddonSku = string.IsNullOrEmpty(shoppingCartItem.AutoAddonSKUs) ? null : shoppingCartItem.AutoAddonSKUs;

                    //to set order line item attributes
                    orderLineItem.Attributes = SetLineItemAttributes(shoppingCartItem?.Product?.Attributes, shoppingCartItem?.Product.ProductCategoryIds);

                    // then make a shipping cost entry in orderlineItem table.             
                    orderLineItem.ShippingCost = orderLineItem.IsLineItemShippingCostEdited ? 0 : shoppingCartItem.ShippingCost;

                    //Set order tax to order line item.
                    orderLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.HST;
                    orderLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.PST;
                    orderLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.GST;
                    orderLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.VAT;
                    orderLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.SalesTax;
                    orderLineItem.TaxTransactionNumber = shoppingCartItem.TaxTransactionNumber;
                    orderLineItem.TaxRuleId = shoppingCartItem.TaxRuleId;
                    orderLineItem.IsConfigurableProduct = shoppingCartItem.Product.ZNodeConfigurableProductCollection.Count > 0 ? true : false;
                    orderLineItem.Custom1 = shoppingCartItem.Custom1;
                    orderLineItem.Custom2 = shoppingCartItem.Custom2;
                    orderLineItem.Custom3 = shoppingCartItem.Custom3;
                    orderLineItem.Custom4 = shoppingCartItem.Custom4;
                    orderLineItem.Custom5 = shoppingCartItem.Custom5;

                    if (shoppingCartItem.Product.RecurringBillingInd)
                    {
                        orderLineItem.RecurringBillingAmount = shoppingCartItem.Product.RecurringBillingInitialAmount;
                        orderLineItem.RecurringBillingCycles = shoppingCartItem.Product.RecurringBillingTotalCycles;
                        orderLineItem.RecurringBillingFrequency = shoppingCartItem.Product.RecurringBillingFrequency;
                        orderLineItem.RecurringBillingPeriod = shoppingCartItem.Product.RecurringBillingPeriod;
                        orderLineItem.IsRecurringBilling = true;
                    }

                    orderLineItem.OrdersDiscount = shoppingCartItem.Product.OrdersDiscount;
                    //To add personalize attribute list
                    orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;
                    orderLineItem.PersonaliseValuesDetail = shoppingCartItem.PersonaliseValuesDetail;
                    orderLineItem.AdditionalCost = shoppingCartItem.AdditionalCost;
                    orderLineItem.OmsOrderLineItemsId = shoppingCartItem.OmsOrderLineItemId;
                }

                AddSimpleItemInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add add-on items in order line item
                AddAddOnsItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add bundle items in order line item
                AddBundleItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Configurable item in order line item                
                AddConfigurableItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Group item in order line item
                AddGroupItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //To add personalise attribute list
                orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;

                if (addNewOrderItem)
                    order.OrderLineItems.Add(orderLineItem);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderLineItems end orderno =:" + order.PurchaseOrderNumber + addressCart.ShoppingCartItems.Count, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to add add-on items in order line item
        public virtual void AddAddOnsItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderLineItems start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            foreach (ZnodeProductBaseEntity addonItems in shoppingCartItem.Product.ZNodeAddonsProductCollection)
            {
                AddLineItemsInOrderLineItem(orderLineItem, addonItems, shoppingCartItem, ZnodeCartItemRelationshipTypeEnum.AddOns);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SetOrderLineItems end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to add bundle items in order line item
        public virtual void AddBundleItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddBundleItemsInOrderLineItem start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            foreach (ZnodeProductBaseEntity bundleItems in shoppingCartItem.Product.ZNodeBundleProductCollection)
            {
                AddLineItemsInOrderLineItem(orderLineItem, bundleItems, shoppingCartItem, ZnodeCartItemRelationshipTypeEnum.Bundles);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddBundleItemsInOrderLineItem end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to add configurable items in order line item
        public virtual void AddConfigurableItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddConfigurableItemsInOrderLineItem start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            foreach (ZnodeProductBaseEntity configurableItems in shoppingCartItem.Product.ZNodeConfigurableProductCollection)
            {
                AddLineItemsInOrderLineItem(orderLineItem, configurableItems, shoppingCartItem, ZnodeCartItemRelationshipTypeEnum.Configurable);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddConfigurableItemsInOrderLineItem end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        public virtual void AddSimpleItemInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddSimpleItemInOrderLineItem start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (shoppingCartItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Simple)
            {
                AddSimpleProductLineItemsInOrderLineItem(orderLineItem, ZnodeCartItemRelationshipTypeEnum.Simple);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddSimpleItemInOrderLineItem end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to add Group items in order line item
        public virtual void AddGroupItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddGroupItemsInOrderLineItem start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            foreach (ZnodeProductBaseEntity groupItems in shoppingCartItem.Product.ZNodeGroupProductCollection)
            {
                AddLineItemsInOrderLineItem(orderLineItem, groupItems, shoppingCartItem, ZnodeCartItemRelationshipTypeEnum.Group);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddGroupItemsInOrderLineItem end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to save the refereal commission only if order is placed using affiliate account.
        public virtual bool SaveReferralCommission(ZnodeOrderFulfillment order)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SaveReferralCommission start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            bool isSaved = false;
            if (IsNotNull(order.ReferralUserId) && order.ReferralUserId > 0)
            {
                decimal commission = (order.SubTotal - (order.DiscountAmount + order.GiftCardAmount));
                return orderHelper.SaveReferralCommission(order.ReferralUserId, order?.Order?.OmsOrderDetailsId ?? 0, string.Empty, commission);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout SaveReferralCommission end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return isSaved;
        }

        // To manage order inventory         
        public virtual bool ManageOrderInventory(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout ManageOrderInventory start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            bool isSuccess = false;
            if (IsNotNull(order) && order?.OrderLineItems.Count > 0)
            {
                OrderWarehouseModel orderWarehouse = new OrderWarehouseModel();
                orderWarehouse.OrderId = order.Order.OmsOrderDetailsId;
                orderWarehouse.UserId = order.UserID;
                orderWarehouse.PortalId = order.PortalId;

                List<OrderWarehouseLineItemsModel> skusInventory = SetSKUInventorySetting(shoppingCart);
                foreach (OrderLineItemModel item in order.OrderLineItems)
                {
                    bool allowBackOrder = false;
                    string inventoryTracking = GetInventoryTrackingBySKU(item.Sku, skusInventory, out allowBackOrder);
                    orderWarehouse.LineItems.Add(new OrderWarehouseLineItemsModel { OrderLineItemId = item.OmsOrderLineItemsId, SKU = item.Sku, Quantity = item.Quantity, InventoryTracking = inventoryTracking, AllowBackOrder = allowBackOrder });

                    foreach (OrderLineItemModel childitem in item.OrderLineItemCollection)
                    {
                        if (childitem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Bundles)
                        {
                            inventoryTracking = GetInventoryTrackingBySKU(childitem.Sku, skusInventory, out allowBackOrder);
                            orderWarehouse.LineItems.Add(new OrderWarehouseLineItemsModel { OrderLineItemId = childitem.OmsOrderLineItemsId, SKU = childitem.Sku, Quantity = childitem.Quantity, InventoryTracking = inventoryTracking, AllowBackOrder = allowBackOrder });
                        }
                    }
                }
                List<OrderWarehouseLineItemsModel> productInventoryList = new List<OrderWarehouseLineItemsModel>();

                isSuccess = orderHelper.ManageOrderInventory(orderWarehouse, out productInventoryList);

                GetLowInventoryProducts(shoppingCart, productInventoryList);
            }
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout ManageOrderInventory end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return isSuccess;
        }

        protected void GetLowInventoryProducts(ZnodePortalCart shoppingCart, List<OrderWarehouseLineItemsModel> productInventoryList)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetLowInventoryProducts start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            shoppingCart.LowInventoryProducts = new List<OrderWarehouseLineItemsModel>();

            shoppingCart.LowInventoryProducts = productInventoryList?.Where(x => x.Quantity < 0 && x.AllowBackOrder == false)?.ToList();
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetLowInventoryProducts end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to add child line items in order line item model as per product type
        public virtual void AddLineItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeProductBaseEntity product, ZnodeShoppingCartItem shoppingCartItem, ZnodeCartItemRelationshipTypeEnum cartItemProductType)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddLineItemsInOrderLineItem start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            OrderLineItemModel childLineItem = new OrderLineItemModel();
            childLineItem.OmsOrderShipmentId = orderLineItem.OmsOrderShipmentId;
            childLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
            childLineItem.ProductName = product.Name;
            childLineItem.Sku = product.SKU;
            childLineItem.Quantity = GetLineItemQuantity(cartItemProductType, product.SelectedQuantity, shoppingCartItem.Quantity);
            childLineItem.Price = IsNotNull(shoppingCartItem?.CustomUnitPrice) && !Equals(cartItemProductType, ZnodeCartItemRelationshipTypeEnum.Bundles) && !Equals(cartItemProductType, ZnodeCartItemRelationshipTypeEnum.AddOns) && !Equals(cartItemProductType, ZnodeCartItemRelationshipTypeEnum.Group) ? shoppingCartItem.CustomUnitPrice.GetValueOrDefault() : GetOrderLineItemPrice(cartItemProductType, product, shoppingCartItem);
            childLineItem.DiscountAmount = GetLineItemDiscountAmount(product.DiscountAmount, childLineItem.Quantity);
            childLineItem.ShipSeparately = product.ShipSeparately;
            childLineItem.ParentOmsOrderLineItemsId = orderLineItem.OmsOrderLineItemsId;
            childLineItem.OrderLineItemRelationshipTypeId = (int)cartItemProductType;
            childLineItem.DownloadLink = product.DownloadLink;
            childLineItem.IsActive = true;
            childLineItem.OrdersDiscount = product.OrdersDiscount;
            childLineItem.OrderLineItemStateId = orderLineItem.OrderLineItemStateId;
            childLineItem.TrackingNumber = orderLineItem.TrackingNumber;
            //Set order tax to order line item.
            childLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.HST;
            childLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.PST;
            childLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.GST;
            childLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.VAT;
            if (cartItemProductType.Equals(ZnodeCartItemRelationshipTypeEnum.Configurable) || cartItemProductType.Equals(ZnodeCartItemRelationshipTypeEnum.Group))
            {
                childLineItem.SalesTax = shoppingCartItem.TaxCost;
            }
            else
            {
                childLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.SalesTax;
            }
            childLineItem.TaxTransactionNumber = orderLineItem.TaxTransactionNumber;
            childLineItem.TaxRuleId = orderLineItem.TaxRuleId;
            childLineItem.PartialRefundAmount = orderLineItem.PartialRefundAmount;
            childLineItem.Custom1 = shoppingCartItem.Custom1;
            childLineItem.Custom2 = shoppingCartItem.Custom2;
            childLineItem.Custom3 = shoppingCartItem.Custom3;
            childLineItem.Custom4 = shoppingCartItem.Custom4;
            childLineItem.Custom5 = shoppingCartItem.Custom5;
            childLineItem.ParentProductSKU = shoppingCartItem.ParentProductSKU;
            childLineItem.Attributes = SetLineItemAttributes(product?.Attributes, product?.ProductCategoryIds);

            if (product.ShipSeparately)
                childLineItem.ShippingCost = orderLineItem.IsLineItemShippingCostEdited ? 0 : product.ShippingCost;

            if (product.RecurringBillingInd)
            {
                childLineItem.RecurringBillingAmount = product.RecurringBillingInitialAmount;
                childLineItem.RecurringBillingCycles = product.RecurringBillingTotalCycles;
                childLineItem.RecurringBillingFrequency = product.RecurringBillingFrequency;
                childLineItem.RecurringBillingPeriod = product.RecurringBillingPeriod;
                childLineItem.IsRecurringBilling = true;
            }
            orderLineItem.OrderLineItemCollection.Add(childLineItem);
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddLineItemsInOrderLineItem end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to add child line items in order line item model as per product type
        public virtual void AddSimpleProductLineItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeCartItemRelationshipTypeEnum cartItemProductType)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddSimpleProductLineItemsInOrderLineItem start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            OrderLineItemModel childLineItem = new OrderLineItemModel();
            childLineItem.OmsOrderShipmentId = orderLineItem.OmsOrderShipmentId;
            childLineItem.Description = orderLineItem.Description;
            childLineItem.ProductName = orderLineItem.ProductName;
            childLineItem.Sku = orderLineItem.Sku;
            childLineItem.Quantity = orderLineItem.Quantity;
            childLineItem.Price = orderLineItem.Price;
            childLineItem.DiscountAmount = orderLineItem.DiscountAmount;
            childLineItem.ShipSeparately = orderLineItem.ShipSeparately;
            childLineItem.ParentOmsOrderLineItemsId = orderLineItem.OmsOrderLineItemsId;
            childLineItem.OrderLineItemRelationshipTypeId = (int)cartItemProductType;
            childLineItem.DownloadLink = orderLineItem.DownloadLink;
            childLineItem.IsActive = true;
            childLineItem.OrdersDiscount = orderLineItem.OrdersDiscount;
            childLineItem.OrderLineItemStateId = orderLineItem.OrderLineItemStateId;
            childLineItem.TrackingNumber = orderLineItem.TrackingNumber;
            //Set order tax to order line item.
            childLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.HST;
            childLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.PST;
            childLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.GST;
            childLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.VAT;
            childLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.SalesTax;
            childLineItem.TaxTransactionNumber = orderLineItem.TaxTransactionNumber;
            childLineItem.TaxRuleId = orderLineItem.TaxRuleId;
            childLineItem.PartialRefundAmount = orderLineItem.PartialRefundAmount;
            childLineItem.Custom1 = orderLineItem.Custom1;
            childLineItem.Custom2 = orderLineItem.Custom2;
            childLineItem.Custom3 = orderLineItem.Custom3;
            childLineItem.Custom4 = orderLineItem.Custom4;
            childLineItem.Custom5 = orderLineItem.Custom5;

            childLineItem.Attributes = orderLineItem.Attributes;

            childLineItem.ShippingCost = orderLineItem.ShippingCost;


            childLineItem.RecurringBillingAmount = orderLineItem.RecurringBillingAmount;
            childLineItem.RecurringBillingCycles = orderLineItem.RecurringBillingCycles;
            childLineItem.RecurringBillingFrequency = orderLineItem.RecurringBillingFrequency;
            childLineItem.RecurringBillingPeriod = orderLineItem.RecurringBillingPeriod;
            childLineItem.IsRecurringBilling = orderLineItem.IsRecurringBilling;

            childLineItem.PersonaliseValueList = orderLineItem.PersonaliseValueList;
            childLineItem.PersonaliseValuesDetail = orderLineItem.PersonaliseValuesDetail;
            childLineItem.ProductImagePath = orderLineItem.ProductImagePath;

            orderLineItem.OrderLineItemCollection.Add(childLineItem);
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout AddSimpleProductLineItemsInOrderLineItem end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to get main product price depends upon product type
        public virtual decimal GetParentProductPrice(ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetParentProductPrice start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            decimal price = shoppingCartItem.UnitPrice - (shoppingCartItem?.Product?.AddOnPrice ?? 0);

            //if parent product price set to null or zero then associate configurable product price else  parent product price 
            if (GetProductPrice(shoppingCartItem.Product).Equals(0))
                price = price - (shoppingCartItem?.Product?.ConfigurableProductPrice ?? 0);

            //if product type is group then price will be zero 
            if (shoppingCartItem?.Product?.GroupProductPrice > 0)
                return 0;
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetParentProductPrice end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return price;
        }

        //to set line item price as per cartitem producttype
        public virtual decimal GetOrderLineItemPrice(ZnodeCartItemRelationshipTypeEnum cartItemProductType, ZnodeProductBaseEntity product, ZnodeShoppingCartItem shoppingCartItem)
        {
            ZnodeLogging.LogMessage("DSODE-97-Znodecheckout GetOrderLineItemPrice start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            decimal? price = 0;
            switch (cartItemProductType)
            {
                case ZnodeCartItemRelationshipTypeEnum.AddOns:
                    return product.FinalPrice;
                case ZnodeCartItemRelationshipTypeEnum.Bundles:
                    return 0;
                case ZnodeCartItemRelationshipTypeEnum.Configurable:
                    price = GetParentProductPrice(shoppingCartItem);
                    price = price > 0 ? price : shoppingCartItem?.Product?.ConfigurableProductPrice;
                    return price.GetValueOrDefault();
                case ZnodeCartItemRelationshipTypeEnum.Group:
                    return product.FinalPrice;
                default:
                    return product.FinalPrice;
            }
        }

        //to product price if SalePrice is present else RetailPrice
        public virtual decimal GetProductPrice(ZnodeProductBaseEntity product)
       => product.SalePrice > 0 ? product.SalePrice.GetValueOrDefault() : product.RetailPrice;

        //to get line item quantity
        public virtual decimal GetLineItemQuantity(ZnodeCartItemRelationshipTypeEnum productType, decimal groupProductQuantity, decimal cartQuantity)
        => productType.Equals(ZnodeCartItemRelationshipTypeEnum.Group) ? groupProductQuantity : cartQuantity;

        //to get inventory tracking of sku by supplied sku
        public virtual string GetInventoryTrackingBySKU(string sku, List<OrderWarehouseLineItemsModel> skusInventory, out bool allowBackOrder)
        {
            allowBackOrder = skusInventory.FirstOrDefault(c => c.SKU == sku).AllowBackOrder;
            return skusInventory.FirstOrDefault(c => c.SKU == sku).InventoryTracking;
        }

        //to set all items in shopping cart to dictionary
        public virtual List<OrderWarehouseLineItemsModel> SetSKUInventorySetting(ZnodePortalCart shoppingCart)
        {
            List<OrderWarehouseLineItemsModel> SKUInventorySetting = new List<OrderWarehouseLineItemsModel>();
            foreach (ZnodeShoppingCartItem item in shoppingCart.ShoppingCartItems)
            {
                AddSkuInventoryTracking(item.Product, SKUInventorySetting);

                foreach (ZnodeProductBaseEntity addon in item.Product.ZNodeAddonsProductCollection)
                {
                    AddSkuInventoryTracking(addon, SKUInventorySetting);
                }

                foreach (ZnodeProductBaseEntity bundle in item.Product.ZNodeBundleProductCollection)
                {
                    AddSkuInventoryTracking(bundle, SKUInventorySetting);
                }

                foreach (ZnodeProductBaseEntity config in item.Product.ZNodeConfigurableProductCollection)
                {
                    AddSkuInventoryTracking(config, SKUInventorySetting, item.Product.AllowBackOrder);
                }

                foreach (ZnodeProductBaseEntity group in item.Product.ZNodeGroupProductCollection)
                {
                    AddSkuInventoryTracking(group, SKUInventorySetting);
                }
            }
            return SKUInventorySetting;
        }

        //to add Sku and its inventorytracking in dictionary
        public virtual void AddSkuInventoryTracking(ZnodeProductBaseEntity product, List<OrderWarehouseLineItemsModel> skuInventoryTracking, bool allowBackOrder = false)
        {
            if (skuInventoryTracking.Where(x => x.SKU == product.SKU).Count() == 0)
            {
                skuInventoryTracking.Add(new OrderWarehouseLineItemsModel { SKU = product.SKU, InventoryTracking = product.InventoryTracking, AllowBackOrder = allowBackOrder ? allowBackOrder : product.AllowBackOrder });
            }
        }

        //to get line item discount amount based on quantity
        public virtual decimal GetLineItemDiscountAmount(decimal discountAmount, decimal quantity)
           => (discountAmount * quantity);

        //to set payment details 
        public virtual bool SetPaymentDetails(ZnodeOrderFulfillment order)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetPaymentDetails start =:" + order.PaymentStatusID, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            ZnodeLogging log = new ZnodeLogging();
            //bool isPreAuthorize = _ShoppingCart.Payment.IsPreAuthorize;
            log.LogActivityTimerStart();

            string paymentStatus = Enum.GetName(typeof(ZnodePaymentStatus), order.PaymentStatusID);
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetPaymentDetails =:" + paymentStatus, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (!string.Equals(ShoppingCart?.Payment?.PaymentTypeName, ZnodeConstant.PAYPAL_EXPRESS, StringComparison.OrdinalIgnoreCase))
                paymentStatus = GetPaymentType(ShoppingCart?.Payment?.PaymentTypeName);

            // update transaction id and status
            order.PaymentTrancationToken = ShoppingCart.Token;
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetPaymentDetails =:" + order.PaymentTrancationToken, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (order.OrderID == 0 || order.OrderOverDueAmount > 0)
                order.PaymentStatusID = orderHelper.GetPaymentStatusId(paymentStatus);

            order.PaymentTypeId = ShoppingCart.Payment.PaymentTypeId;
            order.PaymentSettingID = ShoppingCart.Payment.PaymentSettingId;
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetPaymentDetails end =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return IsNotNull(order.PaymentStatusID);
        }

        //to Save giftcardhistory &  the referral commission
        public virtual void SaveReferralCommissionAndGiftCardHistory(ZnodeOrderFulfillment order, int? userId = 0)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SaveReferralCommissionAndGiftCardHistory start =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            // to apply voucher history 
            order.AddToVoucherHistory(order, userId);

            // Save the referral commission
            this.SaveReferralCommission(order);
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SaveReferralCommissionAndGiftCardHistory end =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //Set Order Shipment Details to order Line Item.
        public virtual void SetOrderShipmentDetails(ZnodeOrderFulfillment order)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetOrderShipmentDetails start =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            List<int> orderShipmentIds = ShoppingCart?.AddressCarts?.Select(x => x.OrderShipmentID)?.Distinct().ToList();
            // Check to get the order shipment for the item  
            if (orderShipmentIds?.Count > 0)
            {
                List<OrderShipmentModel> orderShipments = orderHelper.GetOrderShipmentDetailsByIds(orderShipmentIds);
                if (orderShipments?.Count > 0)
                {
                    List<OrderLineItemModel> orderLineItems = order.OrderLineItems?.Where(x => orderShipments.Select(y => y.OmsOrderShipmentId).Contains(x.OmsOrderShipmentId)).ToList();
                    if (orderLineItems?.Count > 0)
                    {
                        foreach (OrderLineItemModel orderLineItemModel in orderLineItems)
                            orderLineItemModel.ZnodeOmsOrderShipment = orderShipments.FirstOrDefault(x => x.OmsOrderShipmentId == orderLineItemModel.OmsOrderShipmentId);
                    }                    
                }
            }
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetOrderShipmentDetails end =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        // to set order additional details like shippingid,purchaseordernumber & referraluserid etc
        public virtual void SetOrderAdditionalDetails(ZnodeOrderFulfillment order, SubmitOrderModel model)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetOrderAdditionalDetails start =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            order.ShippingId = this.ShoppingCart.Shipping.ShippingID != 0 ? this.ShoppingCart.Shipping.ShippingID : this._ShippingID;
            order.AdditionalInstructions = this.AdditionalInstructions;
            order.PurchaseOrderNumber = this.PurchaseOrderNumber;
            order.PODocument = this.PoDocument;
            order.ReferralUserId = this.UserAccount.ReferralUserId;
            if (IsNotNull(order.Order) && model?.OrderId > 0)
            {
                order.Order.CreatedBy = model.CreatedBy;
                order.Order.ModifiedBy = model.ModifiedBy;
            }
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetOrderAdditionalDetails end =:" + order.PurchaseOrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        //to cancel existing order
        public virtual bool CancelExistingOrder(ZnodeOrderFulfillment order, int orderId)
        {
            ZnodeLogging.LogMessage($"Cancel Order process is initiated for Order Id:{orderId }", ZnodeLogging.Components.OMS.ToString());
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder CancelExistingOrder satrt =:" + orderId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            //to get existing order total amount       
            decimal orderOldTotal = orderHelper.GetOrderTotalById(orderId);
            if (!orderHelper.CancelOrderById(orderId, order.GiftCardNumber))
            {
                this.PaymentResponseText = this.ShoppingCart.ErrorMessage;
                this.IsSuccess = false;
                ZnodeLogging.LogMessage($"Failed to Cancel Order :{orderId }", ZnodeLogging.Components.OMS.ToString());
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder CancelExistingOrder =:" + orderId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                return false;
            }
            ZnodeLogging.LogMessage($"Order Cancel successfully :{orderId }", ZnodeLogging.Components.OMS.ToString());
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder CancelExistingOrder =:" + orderId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            order.OrderOverDueAmount = (order.Total - orderOldTotal);
            order.OrderID = orderId;
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder CancelExistingOrder End=:" + order.OrderID, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return true;
        }

        //to set line item attribute by code specified in order attribute to store in ZnodeOmsOrderAttribute
        public virtual List<OrderAttributeModel> SetLineItemAttributes(List<OrderAttributeModel> allAttributes, int[] productCategoryIds)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetLineItemAttributes satrt =:", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            string attributeCodes = ShoppingCart.OrderAttribute;
            List<OrderAttributeModel> selectedAttributes = new List<OrderAttributeModel>();
            if (!string.IsNullOrEmpty(attributeCodes) && allAttributes?.Count > 0)
            {
                //to get all attributeCodes in string array 
                string[] attributeList = attributeCodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                //to distinct attribute Codes & its value 
                var filteredAttributes = allAttributes.Where(x => attributeList.Contains(x.AttributeCode)).Distinct().ToList();
                if (filteredAttributes?.Count > 0)
                    selectedAttributes.AddRange(filteredAttributes);
            }

            //to add category associated with product to line item
            if (productCategoryIds.Length > 0 && publishCategory.Count > 0)
            {
                foreach (int categoryId in productCategoryIds)
                {
                    selectedAttributes.Add(new OrderAttributeModel { AttributeCode = ZnodeConstant.CategoryName, AttributeValue = GetCategoryNameByCategoryId(categoryId), AttributeValueCode = ZnodeConstant.CategoryCode });
                }
            }
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetLineItemAttributes end =:", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return selectedAttributes;
        }

        //to get all product and its child item category ids
        public virtual void GetDistinctCategoryIdsforCartItem(ZnodeMultipleAddressCart addressCart)
        {
            List<int> _productCategoryIds = new List<int>();
            // loop through cart and add line items
            foreach (ZnodeShoppingCartItem shoppingCartItem in addressCart.ShoppingCartItems)
            {
                GetProductCategoryIds(shoppingCartItem.Product.ProductCategoryIds, _productCategoryIds);

                foreach (ZnodeProductBaseEntity addon in shoppingCartItem.Product.ZNodeAddonsProductCollection)
                {
                    GetProductCategoryIds(addon.ProductCategoryIds, _productCategoryIds);
                }

                foreach (ZnodeProductBaseEntity bundle in shoppingCartItem.Product.ZNodeBundleProductCollection)
                {
                    GetProductCategoryIds(bundle.ProductCategoryIds, _productCategoryIds);
                }

                foreach (ZnodeProductBaseEntity config in shoppingCartItem.Product.ZNodeConfigurableProductCollection)
                {
                    GetProductCategoryIds(config.ProductCategoryIds, _productCategoryIds);
                }

                foreach (ZnodeProductBaseEntity group in shoppingCartItem.Product.ZNodeGroupProductCollection)
                {
                    GetProductCategoryIds(group.ProductCategoryIds, _productCategoryIds);
                }
            }

            //to get category name by category Id
            if (IsNotNull(_productCategoryIds) || _productCategoryIds?.Count > 0)
                GetCategoryNameByCategoryIds(_productCategoryIds);
        }

        //to get all distinct CategoryIds from shopping cart product
        public virtual void GetProductCategoryIds(int[] productCategoryIds, List<int> categoryIds)
        {
            foreach (int categoryId in productCategoryIds)
            {
                if (!categoryIds.Contains(categoryId))
                    categoryIds.Add(categoryId);
            }
        }

        //to get category name by category ids
        public virtual void GetCategoryNameByCategoryIds(List<int> categoryIds)
        {
            int localeId = ShoppingCart.LocalId;
            int publishCatalogId = ShoppingCart.PublishedCatalogId;
            if (IsNotNull(categoryIds) || categoryIds?.Count > 0)
            {
                int? versionId = publishProductHelper.GetCatalogVersionId(publishCatalogId);
                IZnodeRepository<ZnodePublishCategoryEntity> _publishCategoryEntity = new ZnodeRepository<ZnodePublishCategoryEntity>(HelperMethods.Context);

                List<CategoryModel> categories = _publishCategoryEntity.Table.Where(x => x.LocaleId == localeId && x.VersionId == versionId && categoryIds.Contains(x.ZnodeCategoryId))?.ToModel<CategoryModel>()?.ToList();
                         
                if (categories?.Count > 0)
                {
                    categories.ForEach(category =>
                    {
                        if (!string.IsNullOrEmpty(category.CategoryName) && !publishCategory.ContainsKey(category.PublishCategoryId))
                            publishCategory.Add(category.PublishCategoryId, category.CategoryName);
                    });
                }
            }
        }

        //to get category name by Category id
        public virtual string GetCategoryNameByCategoryId(int categoryId)
        {
            return publishCategory.Where(c => c.Key == categoryId).Select(c => c.Value).FirstOrDefault();
        }

        //to rollback order from database if submitting order is failed
        public virtual void VerifySubmitOrderProcess(int orderDetailId)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder VerifySubmitOrderProcess start =:" + orderDetailId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (!this.IsSuccess)
            {
                orderHelper.RollbackFailedOrder(orderDetailId);
            }
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder VerifySubmitOrderProcess end =:" + orderDetailId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
        }

        // to set order state and tracking number
        public virtual void SetOrderStateTrackingNumber(ZnodeOrderFulfillment order, SubmitOrderModel model)
        {
            if (IsNotNull(model?.OrderId))
            {
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetOrderStateTrackingNumber start OrderNo. =:" + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                order.OrderStateID = model.OrderStateId.GetValueOrDefault();
                order.Order.TrackingNumber = model.TrackingNumber;
                if (IsNotNull(model?.PaymentStateId))
                    order.Order.OmsPaymentStateId = model.PaymentStateId.GetValueOrDefault();
                ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SetOrderStateTrackingNumber end OrderNo. =:" + model.OrderNumber, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
        }

        //to save return items in database
        public virtual bool SaveReturnItems(int orderDetailId, ReturnOrderLineItemListModel model)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SaveReturnItems start =:" + orderDetailId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            bool isSuccess = true;
            if (model?.ReturnItemList?.Count > 0)
            {
                foreach (ReturnOrderLineItemModel item in model?.ReturnItemList)
                {
                    item.OrderDetailId = orderDetailId;
                    isSuccess = orderHelper.ReturnOrderLineItems(item);
                }
            }
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder SaveReturnItems end =:" + orderDetailId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return isSuccess;
        }

        //Set Order Details To Shopping Cart. b
        public virtual void SetOrderDetailsToShoppingCart(ZnodeOrderFulfillment order)
        {
            this.ShoppingCart.OrderId = order?.OrderID;
        }

        //Get Payment Type
        public virtual string GetPaymentType(string paymentType)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder GetPaymentType start =:", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (string.Equals(paymentType, ZnodeConstant.CreditCard, StringComparison.OrdinalIgnoreCase)
                  || string.Equals(paymentType, ZnodeConstant.Amazon_Pay, StringComparison.OrdinalIgnoreCase))
            {
                return ZnodeConstant.AUTHORIZED;
            }
            else
            {
                return ZnodeConstant.PENDING;
            }
        }
        [Obsolete]
        public virtual string GetPaymentType(string paymentType, bool isPreAuthorize)
        {
            ZnodeLogging.LogMessage("DSODE-97-SubmitOrder GetPaymentType start =:", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (string.Equals(paymentType, ZnodeConstant.CreditCard, StringComparison.OrdinalIgnoreCase)
                  || string.Equals(paymentType, ZnodeConstant.PAYPAL_EXPRESS, StringComparison.OrdinalIgnoreCase)
                  || string.Equals(paymentType, ZnodeConstant.Amazon_Pay, StringComparison.OrdinalIgnoreCase))
            {
                return isPreAuthorize ? ZnodeConstant.AUTHORIZED : ZnodeConstant.CAPTURED;
            }
            else
            {
                return ZnodeConstant.PENDING;
            }
        }
        #endregion
    }
}
