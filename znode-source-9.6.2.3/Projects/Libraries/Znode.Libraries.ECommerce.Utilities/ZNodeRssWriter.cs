﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using Znode.Libraries.Framework.Business;

namespace Znode.Libraries.ECommerce.Utilities
{
    public class ZnodeRssWriter : ZnodeBusinessBase
    {
        #region Private Variables
        private readonly int recCount = Convert.ToInt32(ZnodeApiSettings.ProductFeedRecordCount);
        private int fileCount = 0;
        #endregion

        #region Public Methods

        /// <summary>
        /// Creates the XMLSite Map
        /// </summary>
        /// <param name="datasetValues">data set values</param>
        /// <param name="rootTag">Root Tag for the XML</param>
        /// <param name="rootTagValue">Root Tag value</param>
        /// <param name="xmlFileName">FileName of the XML</param>
        /// <returns>Returns the boolean value if it created or not.</returns>
        public int CreateGoogleSiteMap(DataSet datasetValues, string rootTagValue, string xmlFileName, string googleFeedTitle, string googleFeedLink, string googleFeedDesc)
        {
            try
            {
                int recordsCount = datasetValues.Tables[0].Rows.Count;
                int loopCnt = 0;

                for (; loopCnt < recordsCount;)
                {
                    string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";

                    string[] rootTagValues = rootTagValue.Split(',');

                    // Construct the XML for the Site Map creation
                    XmlDocument requestXMLDoc = new XmlDocument();

                    XmlNode rss = requestXMLDoc.CreateElement("rss");
                    XmlAttribute version = requestXMLDoc.CreateAttribute("version");
                    version.Value = "2.0";
                    rss.Attributes.Append(version);

                    XmlAttribute nameSpace = requestXMLDoc.CreateAttribute("xmlns:g");
                    nameSpace.Value = rootTagValues[0];
                    rss.Attributes.Append(nameSpace);

                    XmlNode channel = requestXMLDoc.CreateElement("channel");

                    XmlNode Title = requestXMLDoc.CreateElement("title");
                    Title.InnerText = googleFeedTitle;
                    channel.AppendChild(Title);

                    XmlNode link = requestXMLDoc.CreateElement("link");
                    link.InnerText = googleFeedLink;
                    channel.AppendChild(link);

                    XmlNode description = requestXMLDoc.CreateElement("description");
                    description.InnerText = googleFeedDesc;
                    channel.AppendChild(description);

                    // Loop thro the dataset values.
                    do
                    {
                        XmlElement itemtElement = requestXMLDoc.CreateElement("item");
                        DataRow dr = datasetValues.Tables[0].Rows[loopCnt];

                        foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                        {
                            if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                            {
                                itemtElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()?.Trim()));
                            }
                        }

                        channel.AppendChild(itemtElement);

                        loopCnt++;
                    }
                    while (loopCnt < recordsCount && (loopCnt + 1) % recCount != 0);

                    rss.AppendChild(channel);
                    requestXMLDoc.AppendChild(rss);

                    ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);

                    // Increment the file count if the file has to be splitted.
                    this.fileCount++;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }

            return this.fileCount;
        }

        // Creates the XMLSite Map which Returns the boolean value if it created or not
        public int CreateXMLSiteMap(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                if (datasetValues != null)
                {
                    int recordsCount = datasetValues.Tables[0].Rows.Count;
                    int loopCnt = 0;

                    for (; loopCnt < recordsCount;)
                    {
                        string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                        string[] rootTagValues = rootTagValue.Split(',');
                        // Construct the XML for the Site Map creation
                        requestXMLDoc = new XmlDocument();
                        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                        XmlElement urlsetElement = null;
                        urlsetElement = requestXMLDoc.CreateElement(rootTag);
                        if (rootTagValues?.Count() > 0)
                        {
                            string[] values = rootTagValues[0].Split('=');
                            urlsetElement.SetAttribute(values[0], values[1]);
                        }

                        requestXMLDoc.AppendChild(urlsetElement);

                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[0].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement(datasetValues.Tables[0].TableName);                           
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()?.Trim()));
                            }

                            loopCnt++;
                        }
                        while (loopCnt < recordsCount && (loopCnt + 1) % recCount != 0);
                        ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                        // Increment the file count if the file has to be splitted.
                        this.fileCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }

        // Creates the XMLSite Map which Returns the count of files generated.
        public int CreateXMLSiteMapForAll(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName, string priority)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                int tableCount = datasetValues.Tables.Count;
                string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                string[] rootTagValues = rootTagValue.Split(',');
                // Construct the XML for the Site Map creation
                requestXMLDoc = new XmlDocument();
                requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                XmlElement urlsetElement = null;
                urlsetElement = requestXMLDoc.CreateElement(rootTag);
                if (rootTagValues?.Count() > 0)
                {
                    string[] values = rootTagValues[0].Split('=');
                    urlsetElement.SetAttribute(values[0], values[1]);
                }
                requestXMLDoc.AppendChild(urlsetElement);
                for (int i = 0; i < tableCount; i++)
                {
                    int recordsCount = datasetValues.Tables[i].Rows.Count;
                    int loopCnt = 0;
                    if (datasetValues.Tables[i].TableName == "Content" || datasetValues.Tables[i].TableName == "Category")
                    {
                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[i].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[i].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                            }

                            loopCnt++;
                        }
                        while (loopCnt < recordsCount);
                    }
                    else if (datasetValues.Tables[i].TableName == "Product")
                    {
                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[i].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[i].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) && Equals(dc.ColumnName, "loc"))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                            }
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                            loopCnt++;
                        }
                        while (loopCnt < recordsCount);
                    }
                    ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                }
                // Increment the file count.
                this.fileCount++;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }

        // Creates and Returns an Element with the specified tagName with the value 
        private XmlElement MakeElement(XmlDocument doc, string tagName, string tagValue)
        {
            XmlElement elem = tagName.Contains("g:") ? doc.CreateElement("g", tagName.Split(':')[1], "http://base.google.com/ns/1.0") : doc.CreateElement(tagName);
            elem.InnerText = tagValue;
            return elem;
        }

        public string GenerateGoogleSiteMapIndexFiles(int fileNameCount, string txtXMLFileName, string priority)
        {
            string rootTag = "sitemapindex";
            string roottagxmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            // Construct the XML for the Site Map creation
            XmlDocument requestXMLDoc = new XmlDocument();
            requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
            XmlElement urlsetElement = null;
            urlsetElement = requestXMLDoc.CreateElement(rootTag);
            urlsetElement.SetAttribute("xmlns", roottagxmlns);
            requestXMLDoc.AppendChild(urlsetElement);

            for (int i = 0; i < fileNameCount; i++)
            {
                XmlElement urlElement = requestXMLDoc.CreateElement("sitemap");
                string fileName = ZnodeStorageManager.HttpPath($"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{txtXMLFileName.Trim()}_{i}{".xml"}");
                if (fileName.StartsWith("~/")) fileName = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace(System.Web.HttpContext.Current.Request.Url.AbsolutePath, string.Empty) + fileName.Substring(1);
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "loc", fileName));
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy-MM-ddhh-mm-ss")));

                if (!string.IsNullOrEmpty(priority))
                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                urlsetElement.AppendChild(urlElement);
            }
            string strSiteMapIndexFile = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{txtXMLFileName.Trim()}_{DateTime.Now.ToString("yyyy-MM-ddhh-mm-ss")}{".xml"}";
            ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, strSiteMapIndexFile);

            return strSiteMapIndexFile;
        }

        public int CreateProductSiteMap(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName, string priority)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                if (datasetValues != null)
                {
                    int recordsCount = datasetValues.Tables[0].Rows.Count;
                    int loopCnt = 0;

                    for (; loopCnt < recordsCount;)
                    {
                        string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                        string[] rootTagValues = rootTagValue.Split(',');
                        // Construct the XML for the Site Map creation
                        requestXMLDoc = new XmlDocument();
                        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                        XmlElement urlsetElement = null;
                        urlsetElement = requestXMLDoc.CreateElement(rootTag);
                        if (rootTagValues?.Count() > 0)
                        {
                            string[] values = rootTagValues[0].Split('=');
                            urlsetElement.SetAttribute(values[0], values[1]);
                        }
                        requestXMLDoc.AppendChild(urlsetElement);

                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[0].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) && Equals(dc.ColumnName, "loc"))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()?.Trim()));
                            }
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                            loopCnt++;
                        }
                        while (loopCnt < recordsCount && (loopCnt + 1) % recCount != 0);
                        ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                        // Increment the file count if the file has to be splitted.
                        this.fileCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }
        #endregion
    }
}
