﻿using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Tasks
{
    public class ERPTaskScheduler
    {

        #region Public Methods
        /// <summary>
        /// To create a task schedule to run on specific time.
        /// </summary>
        public bool ScheduleTask(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            bool status = false;
            ZnodeLogging.LogMessage("ScheduleTask Start :");
            try
            {
                // Get the service on the local machine
                using (TaskService taskService = new TaskService())
                {
                    // Create a new task definition and assign properties
                    TaskDefinition taskDefinition = taskService.NewTask();


                    if (Znode.Libraries.ECommerce.Utilities.ZnodeApiSettings.CreateSchedulerOnServer == "true")
                    {
                        taskDefinition.Principal.UserId = "NT AUTHORITY\\LOCALSERVICE";
                        // Run Task for IIS user.
                        taskDefinition.Principal.LogonType = TaskLogonType.ServiceAccount;
                    }
                    else
                    {
                        if (Environment.UserName == "SYSTEM")
                        { taskDefinition.Principal.UserId = Environment.UserName; }
                        else
                        { taskDefinition.Principal.UserId = string.Concat(Environment.UserDomainName, "\\", Environment.UserName); }
                        // Run Task whether user logged on or not
                        taskDefinition.Principal.LogonType = TaskLogonType.InteractiveToken;
                    }

                    taskDefinition.RegistrationInfo.Description = erpTaskSchedulerModel.SchedulerName;

                    // Create a trigger that will fire the task at specific time 
                    taskDefinition.Triggers.Add(SetTrigger(erpTaskSchedulerModel));

                    taskDefinition.Settings.DeleteExpiredTaskAfter = new TimeSpan(0, 1, 0);

                    //Hides the scheduler's command window.
                    taskDefinition.Settings.Hidden = true;

                    // Create an action that will launch Cleanup Exe whenever the trigger fires.                  
                    taskDefinition.Actions.Add(new ExecAction(erpTaskSchedulerModel.ExePath, erpTaskSchedulerModel.ExeParameters, null));

                    // Register the task in the root folder                  
                    taskService.RootFolder.RegisterTaskDefinition(erpTaskSchedulerModel.SchedulerName, taskDefinition);
                }
                ZnodeLogging.LogMessage("Task Schedule created for " + erpTaskSchedulerModel.SchedulerName);
                status = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Error in ScheduleTask :{ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error,ex);
            }
            return status;
        }

        /// <summary>
        /// Delete Task scheduler
        /// </summary>
        /// <param name="schedulerName">Name of task scheduler</param>
        /// <returns></returns>
        public bool DeleteScheduleTask(List<String> taskSchedulerNames)
        {
            bool deleteStatus = false;
            using (TaskService ts = new TaskService())
            {
                foreach (var schedulerName in taskSchedulerNames)
                {
                    if (!Equals(ts.GetTask(schedulerName), null))
                    {
                        ts.RootFolder.DeleteTask(schedulerName);
                        deleteStatus = true;
                    }
                    else
                        deleteStatus = true;
                }
            }
            return deleteStatus;
        }

        /// <summary>
        /// Enable/Disable Schedule task by scheduler name
        /// </summary>
        /// <param name="schedulerName"></param>
        /// <param name="state"></param>
        public void EnableDisableScheduleTask(string schedulerName, bool state)
        {
            using (TaskService ts = new TaskService())
            {
                Task task = ts.GetTask(schedulerName);
                if (!Equals(task, null))
                    task.Enabled = state;
            }
        }
        #endregion

        #region Private Methods
        private Trigger SetTrigger(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            switch (erpTaskSchedulerModel.SchedulerFrequency)
            {
                case ZnodeConstant.Daily:
                    return DailyTrigger(erpTaskSchedulerModel);
                case ZnodeConstant.Weekly:
                    return WeeklyTrigger(erpTaskSchedulerModel);
                case ZnodeConstant.Monthly:
                    if (erpTaskSchedulerModel.IsMonthlyDays)
                        return MonthlyTrigger(erpTaskSchedulerModel);
                    else
                        return MonthlyDOWTrigger(erpTaskSchedulerModel);
                default:
                    return OneTimeTrigger(erpTaskSchedulerModel);
            }
        }

        //Create trigger that will fire one time only
        private TimeTrigger OneTimeTrigger(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            TimeTrigger trigger = new TimeTrigger();
            trigger.StartBoundary = (DateTime)erpTaskSchedulerModel.StartDate;
            trigger.EndBoundary = (DateTime)erpTaskSchedulerModel.ExpireDate;
            RepeatTaskscheduler(trigger, erpTaskSchedulerModel);
            return trigger;
        }

        // Create a trigger that will fire every day
        private DailyTrigger DailyTrigger(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            DailyTrigger daily = new DailyTrigger();
            daily.StartBoundary = (DateTime)erpTaskSchedulerModel.StartDate;
            daily.EndBoundary = (DateTime)erpTaskSchedulerModel.ExpireDate;
            if (erpTaskSchedulerModel.RecurEvery > 0)
                daily.DaysInterval = (short)erpTaskSchedulerModel.RecurEvery;

            RepeatTaskscheduler(daily, erpTaskSchedulerModel);
            return daily;
        }

        //Create trigger that will fire weekly
        private WeeklyTrigger WeeklyTrigger(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            WeeklyTrigger weekly = new WeeklyTrigger();
            weekly.StartBoundary = (DateTime)erpTaskSchedulerModel.StartDate;
            weekly.EndBoundary = (DateTime)erpTaskSchedulerModel.ExpireDate;
            if (erpTaskSchedulerModel.RecurEvery > 0)
                weekly.WeeksInterval = (short)erpTaskSchedulerModel.RecurEvery;

            if (!string.IsNullOrEmpty(erpTaskSchedulerModel.WeekDays))
                weekly.DaysOfWeek = DaysOfTheWeeks(erpTaskSchedulerModel.WeekDays);

            RepeatTaskscheduler(weekly, erpTaskSchedulerModel);
            return weekly;
        }

        //Create trigger that will fire Monthly schedule
        private MonthlyTrigger MonthlyTrigger(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            MonthlyTrigger monthly = new MonthlyTrigger();
            monthly.StartBoundary = (DateTime)erpTaskSchedulerModel.StartDate;
            monthly.EndBoundary = (DateTime)erpTaskSchedulerModel.ExpireDate;

            if (!string.IsNullOrEmpty(erpTaskSchedulerModel.Months))
                monthly.MonthsOfYear = MonthsOfTheYears(erpTaskSchedulerModel.Months);

            if (!string.IsNullOrEmpty(erpTaskSchedulerModel.Days))
                monthly.DaysOfMonth = erpTaskSchedulerModel.Days.Split(',').Select(int.Parse).ToArray();

            RepeatTaskscheduler(monthly, erpTaskSchedulerModel);
            return monthly;
        }

        //Create trigger that will fire Monthly Day Of Week Schedule
        private MonthlyDOWTrigger MonthlyDOWTrigger(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            MonthlyDOWTrigger monthlyDOWTrigger = new MonthlyDOWTrigger();
            monthlyDOWTrigger.StartBoundary = (DateTime)erpTaskSchedulerModel.StartDate;
            monthlyDOWTrigger.EndBoundary = (DateTime)erpTaskSchedulerModel.ExpireDate;

            if (!string.IsNullOrEmpty(erpTaskSchedulerModel.Months))
                monthlyDOWTrigger.MonthsOfYear = MonthsOfTheYears(erpTaskSchedulerModel.Months);

            if (!string.IsNullOrEmpty(erpTaskSchedulerModel.WeekDays))
                monthlyDOWTrigger.DaysOfWeek = DaysOfTheWeeks(erpTaskSchedulerModel.WeekDays);

            if (!string.IsNullOrEmpty(erpTaskSchedulerModel.OnDays))
                monthlyDOWTrigger.WeeksOfMonth = WhichWeeks(erpTaskSchedulerModel.OnDays);

            RepeatTaskscheduler(monthlyDOWTrigger, erpTaskSchedulerModel);
            return monthlyDOWTrigger;
        }

        //Repeat scheduler
        private Trigger RepeatTaskscheduler(Trigger trigger, ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            if (erpTaskSchedulerModel.IsRepeatTaskEvery)
            {
                //Repeat interval
                trigger.Repetition.Interval = TimeSpan.FromMinutes(erpTaskSchedulerModel.RepeatTaskEvery);
                if (!Equals(erpTaskSchedulerModel.RepeatTaskForDuration, "0"))
                {
                    var repeatTaskDuration = erpTaskSchedulerModel.RepeatTaskForDuration.Remove(erpTaskSchedulerModel.RepeatTaskForDuration.Length - 1);
                    //stop repeation of task
                    trigger.Repetition.Duration = TimeSpan.FromMinutes(Convert.ToInt32(repeatTaskDuration));
                }
            }
            return trigger;
        }

        // Method for Days Of The Weeks
        private DaysOfTheWeek DaysOfTheWeeks(string weekDays)
        {
            var daysOfWeek = DaysOfTheWeek.AllDays;
            daysOfWeek &= ~DaysOfTheWeek.AllDays;

            foreach (var weekDay in weekDays.Split(','))
            {
                switch (weekDay)
                {
                    case "Monday":
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Monday;
                        break;
                    case "Tuesday":
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Tuesday;
                        break;
                    case "Wednesday":
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Wednesday;
                        break;
                    case "Thursday":
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Thursday;
                        break;
                    case "Friday":
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Friday;
                        break;
                    case "Saturday":
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Saturday;
                        break;
                    default:
                        daysOfWeek = daysOfWeek | DaysOfTheWeek.Sunday;
                        break;
                }
            }

            return daysOfWeek;
        }

        // Method for Months Of The Years
        private MonthsOfTheYear MonthsOfTheYears(string months)
        {
            var monthsOfYear = MonthsOfTheYear.AllMonths;
            monthsOfYear &= ~MonthsOfTheYear.AllMonths;

            foreach (var month in months.Split(','))
            {
                switch (month)
                {
                    case "January":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.January;
                        break;
                    case "February":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.February;
                        break;
                    case "March":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.March;
                        break;
                    case "April":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.April;
                        break;
                    case "May":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.May;
                        break;
                    case "June":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.June;
                        break;
                    case "July":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.July;
                        break;
                    case "August":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.August;
                        break;
                    case "September":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.September;
                        break;
                    case "October":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.October;
                        break;
                    case "November":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.November;
                        break;
                    case "December":
                        monthsOfYear = monthsOfYear | MonthsOfTheYear.December;
                        break;
                }
            }
            return monthsOfYear;
        }

        // Method for Which Weeks
        private WhichWeek WhichWeeks(string onWeekDays)
        {
            var weeksOfMonth = WhichWeek.AllWeeks;
            weeksOfMonth &= ~WhichWeek.AllWeeks;

            foreach (var onDays in onWeekDays.Split(','))
            {
                switch (onDays)
                {
                    case "First":
                        weeksOfMonth = weeksOfMonth | WhichWeek.FirstWeek;
                        break;
                    case "Second":
                        weeksOfMonth = weeksOfMonth | WhichWeek.SecondWeek;
                        break;
                    case "Third":
                        weeksOfMonth = weeksOfMonth | WhichWeek.ThirdWeek;
                        break;
                    case "Fourth":
                        weeksOfMonth = weeksOfMonth | WhichWeek.FourthWeek;
                        break;
                }
            }
            return weeksOfMonth;
        }
        #endregion
    }
}
