﻿using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Engine.Analytics
{
    public class AnalyticsHelper : IAnalyticsHelper
    {
        //Gets the access token from the google API from the passed JSON string
        public virtual AnalyticsModel GetAccessToken(string analyticsJSONKey)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.Reports.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(analyticsJSONKey))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.TextCheckGAConfiguration);

            try
            {
                string[] scopes = new string[] { AnalyticsService.Scope.AnalyticsReadonly };
                GoogleCredential credential = GoogleCredential.FromJson(analyticsJSONKey);
                credential = credential.CreateScoped(scopes);

                Task<string> task = ((ITokenAccess)credential).GetAccessTokenForRequestAsync();
                task?.Wait();
                string accessToken = task?.Result;

                ZnodeLogging.LogMessage("Analytics access token: ", ZnodeLogging.Components.Reports.ToString(), TraceLevel.Verbose, new { accessToken = accessToken });

                return new AnalyticsModel { AnalyticsAccessToken = accessToken };
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Error while generating analytics access token: " + ex.Message, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.TextCheckGAConfiguration);
            }
        }
    }
}
