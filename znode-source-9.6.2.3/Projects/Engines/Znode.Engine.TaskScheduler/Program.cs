﻿using StructureMap;
using System;
using System.Linq;
using System.Net;
using StructureMap;

namespace Znode.Engine.TaskScheduler
{
    static class Program
    {
        static void Main(string[] args)
        {
            string type = Convert.ToString(args[0]);
            ISchdulerProviders _provider = GetSchdulerProviderObject(type);

            // at least for dev and QA, trust any certificate
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            try
            {
                if (!Equals(_provider, null))
                    _provider.InvokeMethod(args);
            }
            catch (Exception ex)
            {
                BaseScheduler bs = new BaseScheduler();
                bs.LogMessage($"{ex.Message}-------{ex.StackTrace}", "MainProgram");
            }
        }

        private static ISchdulerProviders GetSchdulerProviderObject(string schedulerType)
        {
            if (!string.IsNullOrEmpty(schedulerType))
            {
                Container container = new Container(content =>
                content.Scan(scan =>
                {
                    scan.AssemblyContainingType<ISchdulerProviders>();
                    scan.AddAllTypesOf<ISchdulerProviders>();
                }));
                string schedulerName = container.Model.AllInstances.FirstOrDefault(x => x.Description.Contains(schedulerType)).Name;
                return !string.IsNullOrEmpty(schedulerType) ? container.GetInstance<ISchdulerProviders>(schedulerName) : null;
            }
            return null;
        }
    }
}
