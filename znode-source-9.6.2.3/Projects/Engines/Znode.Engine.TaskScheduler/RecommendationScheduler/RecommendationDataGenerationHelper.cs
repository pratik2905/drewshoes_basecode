﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.TaskScheduler
{
    public class RecommendationDataGenerationHelper : BaseScheduler, ISchdulerProviders
    {
        #region MyRegion
        private const string SchedulerName = "RecommendationDataGenerationHelper";
        private const string AuthorizationHeader = "Authorization";
        private const string UserHeader = "Znode-UserId";
        private const string TokenHeader = "Token";
        private string TokenValue = string.Empty;
        #endregion

        public void InvokeMethod(string[] args)
        {
            if (args.Length > 6)
                TokenValue = args[6];
            // args[7] contains request timeout value.
            if (args.Length > 7 && !string.IsNullOrEmpty(args[7]))
                base.requesttimeout = int.Parse(args[7]);

            bool isBuildPartial;
            bool.TryParse(args[2], out isBuildPartial);

            CallRecommendationDataGenerationAPI(Convert.ToInt32(args[1]), isBuildPartial, args[3], args[4], args[5]);
        }

        //To call recommendation data generation API.
        private void CallRecommendationDataGenerationAPI(int portalId, bool isBuildPartial, string apiDomainURL, string userId, string token)
        {
            //Request model.
            RecommendationDataGenerationRequestModel recommendationRequest = new RecommendationDataGenerationRequestModel() { PortalId = portalId, IsBuildPartial = isBuildPartial };
            var dataBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(recommendationRequest));
            string requestPath = $"{apiDomainURL}/recommendation/GenerateRecommendationData";            
            string jsonString = string.Empty;
            string message = string.Empty;
            LogMessage(requestPath, SchedulerName);

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = dataBytes.Length;
                request.Headers.Add($"{ UserHeader }: { userId }");
                request.Headers.Add($"{ AuthorizationHeader }: Basic { token }");

                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");

                request.Timeout = base.requesttimeout;

                using (var reqStream = request.GetRequestStream())
                {
                    reqStream.Write(dataBytes, 0, dataBytes.Length);
                }

                LogMessage("Recommendation data generation API will be called.", SchedulerName);
                using (HttpWebResponse responce = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = responce.GetResponseStream();
                    LogMessage("Got Response Stream.", SchedulerName);
                    StreamReader reader = new StreamReader(datastream);
                    LogMessage("read Response Stream.", SchedulerName);
                    jsonString = reader.ReadToEnd();
                    reader.Close();
                    datastream.Close();

                    RecommendationDataGenerationResponse result = new RecommendationDataGenerationResponse();
                    result = JsonConvert.DeserializeObject<RecommendationDataGenerationResponse>(jsonString);
                    RecommendationGeneratedDataModel model = result?.recommendationGeneratedData;
                    LogMessage("Recommendation API called successfully.", SchedulerName);

                    //Scheduler activity log.
                    if (model.IsDataGenerationStarted)
                        LogMessage($"Scheduler for recommendation data generation for portal Id {portalId} started successfully at {DateTime.Now} ", SchedulerName);
                    else
                        LogMessage($"Failed to create recommendation data for portal Id {portalId} because { result.ErrorMessage}", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(apiDomainURL, token);
                    CallRecommendationDataGenerationAPI(portalId, isBuildPartial, apiDomainURL, userId, token);
                }
                else
                    LogMessage($"Recommendation data generation - Failed: {webex.Message} , StackTrace: {webex.StackTrace}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"Recommendation data generation - Failed: {ex.Message} , StackTrace: {ex.StackTrace}", SchedulerName);
            }
        }
    }
}
