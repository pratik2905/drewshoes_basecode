using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionAmountOffProduct : ZnodeCartPromotionType
    {
        #region Private Variable
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        #endregion

        #region Constructor
        public ZnodeCartPromotionAmountOffProduct()
        {
            Name = "Amount Off Product";
            Description = "Applies an amount off a product for an order; affects the shopping cart.";
            AvailableForFranchise = true;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.RequiredProduct);
            Controls.Add(ZnodePromotionControl.RequiredProductMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the amount off a product in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy);
            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            //to get all product of promotion by PromotionId
            List<ProductModel> promotionsProduct = promotionHelper.GetPromotionProducts(PromotionBag.PromotionId);

            // Loop through each cart Item
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (CheckProductPromotion(promotionsProduct, cartItem.Product.ProductID)
                    && cartItem.Product.ZNodeConfigurableProductCollection.Count == 0
                    && cartItem.Product.ZNodeGroupProductCollection.Count == 0)
                {
                    ApplyDiscount(isCouponValid, couponIndex, cartItem);
                }

                //to apply promotion for addon product
                if (cartItem.Product.ZNodeAddonsProductCollection.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                    {
                        ApplyLineItemDiscount(cartItem.Quantity, couponIndex, addon, promotionsProduct);
                    }
                }

                //to apply promotion for configurable product
                if (cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                {
                    if (CheckProductPromotion(promotionsProduct, cartItem.ParentProductId))
                        ApplyLineItemDiscount(cartItem.Quantity, couponIndex, cartItem.Product,promotionsProduct, cartItem.Product.ZNodeConfigurableProductCollection[0], true);
                    else
                    {
                        foreach (ZnodeProductBaseEntity configurable in cartItem.Product.ZNodeConfigurableProductCollection)
                        {
                            ApplyLineItemDiscount(cartItem.Quantity, couponIndex, configurable, promotionsProduct);
                        }
                    }
                }

                //to apply promotion for group product
                if (cartItem.Product.ZNodeGroupProductCollection.Count > 0)
                {
                    if (CheckProductPromotion(promotionsProduct, cartItem.Product.ProductID))
                        ApplyLineItemDiscount(cartItem.Quantity, couponIndex,cartItem.Product, promotionsProduct, cartItem.Product.ZNodeGroupProductCollection[0], true);
                    else
                    {
                        foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                        {
                            ApplyLineItemDiscount(group.SelectedQuantity, couponIndex, group, promotionsProduct);
                        }
                    }
                }
            }

            AddPromotionMessage(couponIndex);
        }
        #endregion

        #region Private Method
        //to apply discount amount
        private void ApplyDiscount(bool isCouponValid, int? couponIndex, ZnodeShoppingCartItem cartItem)
        {
            decimal subTotal = GetCartSubTotal(ShoppingCart);
            decimal qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartItem.Quantity, ShoppingCart.SubTotal, false))
                    return;

                if (PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    if (IsDiscountApplicable(cartItem.Product.DiscountAmount, cartItem.Product.FinalPrice))
                    {
                        decimal discountamount = 0;
                        if (PromotionBag.Discount > cartItem.Product.FinalPrice)
                            discountamount = GetDiscountAmountAsPerQuantity(cartItem.Product.FinalPrice, qtyOrdered);
                        else
                            discountamount = PromotionBag.Discount;

                        cartItem.Product.DiscountAmount += discountamount;
                        cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(PromotionBag.PromoCode, discountamount, OrderDiscountTypeEnum.PROMOCODE, cartItem.Product.OrdersDiscount);
                        SetPromotionalPriceAndDiscount(cartItem, PromotionBag.Discount);
                    }
                    ShoppingCart.IsAnyPromotionApplied = true;
                }
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartItem.Quantity, ShoppingCart.SubTotal, true))
                    return;

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        if (PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                        {
                            cartItem.Product.DiscountAmount += PromotionBag.Discount;
                            cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(coupon.Code, PromotionBag.Discount, OrderDiscountTypeEnum.COUPONCODE, cartItem.Product.OrdersDiscount);
                            SetPromotionalPriceAndDiscount(cartItem, PromotionBag.Discount);
                            SetCouponApplied(coupon.Code);
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                        }

                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
            }
        }

        //to apply line item discount
        private void ApplyLineItemDiscount(decimal qtyOrdered, int? couponIndex, ZnodeProductBaseEntity product, List<ProductModel> promotionsProduct, ZnodeProductBaseEntity childproduct = null, bool isCofigproduct = false)
        {
            decimal subTotal = GetCartSubTotal(ShoppingCart);
            if (CheckProductPromotion(promotionsProduct, product.ProductID) || childproduct != null)
            {
                decimal quantity = product.SelectedQuantity == 0 ? qtyOrdered : product.SelectedQuantity;
                decimal productFinalPice = GetProductFinalPrice(product.FinalPrice,childproduct?.FinalPrice);
                if (((productFinalPice > 0.0M )
                    && PromotionBag.RequiredCategoryMinimumQuantity <= quantity
                    && PromotionBag.MinimumOrderAmount <= subTotal) && IsDiscountApplicable(product.DiscountAmount, productFinalPice))
                {
                    if (Equals(PromotionBag.Coupons, null))
                    {
                       ApplyChildItemDiscount(couponIndex, product,childproduct, quantity);
                    }
                    else if (!Equals(PromotionBag.Coupons, null))
                    {
                        ValidateCoupon(couponIndex);
                        foreach (CouponModel coupon in PromotionBag.Coupons)
                        {
                            if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && ShoppingCart.Coupons[couponIndex.Value].Coupon.Equals(coupon.Code, System.StringComparison.InvariantCultureIgnoreCase))
                            {
                                 ApplyChildItemDiscount(couponIndex, product, childproduct, quantity, coupon.Code);
                                if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        //to apply child item discount
        private void ApplyChildItemDiscount(int? couponIndex, ZnodeProductBaseEntity product, ZnodeProductBaseEntity childProduct,decimal quantity, string couponCode = "", bool isconfigProduct = false)
        {
            decimal discount = 0;
            decimal finalProductPrice = GetProductFinalPrice(product.FinalPrice, childProduct?.FinalPrice);
            if (PromotionBag.Discount > finalProductPrice)
                discount = GetDiscountAmountAsPerQuantity(finalProductPrice, quantity);
            else
                discount = PromotionBag.Discount;
          
            if (HelperUtility.IsNull(childProduct))
            {
                product.DiscountAmount += discount;
                product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), product.OrdersDiscount);
            }
            else
            {
                if (isconfigProduct)
                    product.DiscountAmount += discount;
                else
                    childProduct.DiscountAmount += discount;
                childProduct.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), product.OrdersDiscount);
            }
               
            if (!string.IsNullOrEmpty(couponCode))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, quantity, ShoppingCart.SubTotal, true))
                    return;

                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = true;
            }
        }

        //to get get discount amount as per quantity selected
        private decimal GetDiscountAmountAsPerQuantity(decimal productPrice, decimal quantity)
        {
            decimal discount = (PromotionBag.Discount / quantity);
            return discount > productPrice ? productPrice : discount;
        }

        //to check product have associated promotion to it
        private bool CheckProductPromotion(List<ProductModel> promotionsProduct, int productId)
        => promotionsProduct.Where(x => x.ProductId == productId)?.FirstOrDefault() == null ? false : true;

        private decimal GetProductFinalPrice(decimal? productPrice, decimal? childProductPrice)
            => (productPrice > 0.0M ? productPrice : childProductPrice > 0.0M ? childProductPrice : 0.0m).GetValueOrDefault();       
			
		 #endregion
    }
}
