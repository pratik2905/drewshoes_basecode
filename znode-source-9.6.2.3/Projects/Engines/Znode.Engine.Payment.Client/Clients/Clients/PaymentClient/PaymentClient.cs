﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Payment.Client.Endpoints;
using System.Net;
using Znode.Engine.Exceptions;
using Newtonsoft.Json;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Payment.Client
{
    public class PaymentClient : BaseClient, IPaymentClient
    {
        //Call PayNow method in Payment Application
        public virtual GatewayResponseModel PayNow(SubmitPaymentModel model)
        {
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient PayNow start " + model.OrderId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            string endpoint = PaymentEndpoint.PayNow();
            ApiStatus status = new ApiStatus();
            PaymentGatewayResponse response = PostResourceToEndpoint<PaymentGatewayResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient PayNow end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            return response?.GatewayResponse;
        }

        //Capture Payment 
        public virtual BooleanModel CapturePayment(string paymentTransactionToken)
        {
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient CapturePayment start " + paymentTransactionToken, ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            string endpoint = PaymentEndpoint.CapturePayment(paymentTransactionToken);

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient CapturePayment end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            return response?.booleanModel;
        }

        //Call PayPal method in Payment Application
        public virtual GatewayResponseModel PayPal(SubmitPaymentModel model)
        {
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient PayPal start " + model.OrderId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            string endpoint = PaymentEndpoint.PayPal();
            ApiStatus status = new ApiStatus();
            PaymentGatewayResponse response = PostResourceToEndpoint<PaymentGatewayResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient PayPal end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            return response?.GatewayResponse;
        }

        //Call PayPal method in Payment Application
        public virtual GatewayResponseModel FinalizePayPalProcess(SubmitPaymentModel submitPaymentModel)
        {
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient FinalizePayPalProcess start " + submitPaymentModel.OrderId, ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            string endpoint = PaymentEndpoint.FinalizePayPalProcess();
            ApiStatus status = new ApiStatus();
            PaymentGatewayResponse response = PostResourceToEndpoint<PaymentGatewayResponse>(endpoint, JsonConvert.SerializeObject(submitPaymentModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            ZnodeLogging.LogMessage("DSODE-97-PaymentClient FinalizePayPalProcess end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), System.Diagnostics.TraceLevel.Error);
            return response?.GatewayResponse;
        }
    }
}
