﻿using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using System.Linq;
using static Znode.Engine.Services.Helper.ClearCacheHelper;
using Znode.Engine.Services.Helper;
using System.Web;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public class ZnodeEventObserver
    {
        #region Private Variables 
        Connector<IEnumerable<PublishedProductEntityModel>> productToken;
        Connector<List<int>> categoryToken;
        Connector<List<CategoryPublishEventModel>> categoryPublishToken;
        Connector<DefaultGlobalConfigListModel> cacheKey;
        Connector<PortalModel> webstorePortalKey;
        readonly EventAggregator eventAggregator;
        Connector<ZnodePortalCountry> myZnodePortalCountryToken;
        Connector<ZnodePromotionCoupon> myZnodePromotionCouponToken;
        Connector<PriceSKUModel> myPriceSKUModelToken;
        Connector<InventorySKUModel> myInventorySKUModelToken;
        Connector<PromotionModel> myPromotionModelToken;
        Connector<string> myWebstorePageToken;
        Connector<RecommendationSettingModel> PortalRecommendation;
        Connector<CMSTypeMappingModel> myContentPagePreviewModelToken;
        Connector<CloudflarePurgeModel> clearCloudflareCache;
        Connector<SearchKeywordsRedirectModel> searchKeywordsRedirectToken;
        Connector<PortalApprovalModel> portalApprovalDetailsToken;
        #endregion

        #region Constructor
        public ZnodeEventObserver(EventAggregator eve)
        {
            eventAggregator = eve;

            eve.Attach<IEnumerable<PublishedProductEntityModel>>(this.OnPublishProduct); //1
            eve.Attach<List<int>>(this.OnPublishCategory);//1
            eve.Attach<List<CategoryPublishEventModel>>(this.OnPublishCategory);//1
            eve.Attach<Dictionary<string, string>>(this.OnLoggingConfigurationUpdate);
            eve.Attach<PortalModel>(this.OnPortalUpdate);
            eve.Attach<ZnodePortalCountry>(this.OnChangeCountryAssociation);
            eve.Attach<ZnodePromotionCoupon>(this.OnCreatePromotion);

            eve.Attach<PriceSKUModel>(this.OnUpdatePrice);
            eve.Attach<InventorySKUModel>(this.OnUpdateInventory);
            eve.Attach<PromotionModel>(this.OnUpdatePromotion);
            eve.Attach<string>(this.OnWebstorePageChange);
            eve.Attach<RecommendationSettingModel>(this.OnUpdateProductRecommendations);
            eve.Attach<CMSTypeMappingModel>(this.OnPublishContentPage);
            eve.Attach<CloudflarePurgeModel>(this.OnPublishStore);
            eve.Attach<SearchKeywordsRedirectModel>(this.OnKeywordsRedirectListChange);
            eve.Attach<PortalApprovalModel>(this.OnPortalApprovalDetailsChange);
            //SEO => Product - >Content -> Category -
        }
        #endregion

        /// <summary>
        /// Delete Webstore cache on updating pricing in product.
        /// </summary>
        /// <param name="model"></param>
        private void OnUpdatePrice(PriceSKUModel model)
        {
            ClearWebstoreCache(string.Join(",", Convert.ToInt32(model.PortalId)), "OnUpdatePrice");

            eventAggregator.Detach(myPriceSKUModelToken);
        }

        /// <summary>
        /// Delete Webstore cache on updating inventory in product.
        /// </summary>
        /// <param name="model"></param>
        private void OnUpdateInventory(InventorySKUModel model)
        {
            ClearWebstoreCache(string.Join(",", Convert.ToInt32(model.PortalId)), "OnUpdateInventory");

            eventAggregator.Detach(myInventorySKUModelToken);
        }

        /// <summary>
        /// Delete Webstore cache on updating promotion in product.
        /// </summary>
        /// <param name="model"></param>
        private void OnUpdatePromotion(PromotionModel model)
        {
            ClearWebstoreCache(string.Join(",", Convert.ToInt32(model.PortalId)), "OnUpdatePromotion");

            eventAggregator.Detach(myPromotionModelToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnCreatePromotion(ZnodePromotionCoupon model)
        {
            HttpRuntime.Cache.Remove("AllPromotionCache");

            eventAggregator.Detach(myZnodePromotionCouponToken);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPortalUpdate(PortalModel model)
        {
            //Clear API cache 
            ClearApiCache("webstoreportal");

            ClearWebstoreCache(string.Join(",", model.PortalId), "webstoreportal");

            eventAggregator.Detach(webstorePortalKey);
        }

        /// <summary>
        /// Clear webstore cache on product recommendation setting update event.
        /// </summary>
        /// <param name="recommendationSettingModel"></param>
        private void OnUpdateProductRecommendations(RecommendationSettingModel recommendationSettingModel)
        {
            ClearWebstoreCache(recommendationSettingModel.PortalId.ToString(), "webstoreportal");

            eventAggregator.Detach(PortalRecommendation);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishProduct(IEnumerable<PublishedProductEntityModel> model)
        {
            if (HelperUtility.IsNull(model))
            {
                return;
            }

            var catalogid = model.Select(x => x.ZnodeCatalogId).AsEnumerable().Distinct();
            int[] portalIds = GetPortalIds(catalogid);

            //Clear API cache 
            ClearApiCache("ProductListKey_");
            ClearApiCache("fulltextsearch");
            ClearApiCache("webstoreproduct");
            ClearApiCache("getlinkproductlist");
            ClearWebstoreCache(string.Join(",", portalIds), "ProductListKey_");

            new CloudflareHelper().PurgeUrlContentsOnProductPublish(model, portalIds.ToList());

            eventAggregator.Detach(productToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// This event is obsolete instead of this event please use new event which have parameter List<CategoryPublishEventModel>.
        [Obsolete]
        private void OnPublishCategory(List<int> model)
        {
            //Clear API cache 
            ClearApiCache("CategoryListKey_");
            ClearApiCache("categorydetails");
            ClearWebstoreCache(string.Join(",", GetPortalIds(model)), "CategoryListKey_");

            eventAggregator.Detach(categoryToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishCategory(List<CategoryPublishEventModel> model)
        {
            if (HelperUtility.IsNull(model))
            {
                return;
            }

            //Clear API cache 
            ClearApiCache("CategoryListKey_");
            ClearApiCache("categorydetails");
            ClearWebstoreCache(string.Join(",", model.Select(x => x.PortalId).ToList()), "CategoryListKey_");

            new CloudflareHelper().PurgeUrlContentsOnCategoryPublish(model);

            eventAggregator.Detach(categoryPublishToken);
        }



        private void OnLoggingConfigurationUpdate(Dictionary<string, string> model)
        {
            ClearWebstoreCache(string.Join(",", GetAllPortalIds()), "DefaultLoggingConfigCache");

            HttpContext.Current.Cache.Remove("DefaultLoggingConfigCache");

            ZnodeCacheDependencyManager.Remove("DefaultLoggingConfigCache");

            ReInitializeWebstoreCacheForLogConfiguration(string.Join(",", GetAllPortalIds()), "DefaultLoggingConfigCache");

            eventAggregator.Detach(cacheKey);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnChangeCountryAssociation(ZnodePortalCountry model)
        {
            ClearWebstoreCache(Convert.ToString(model.PortalId), string.Concat("CountriesList", Convert.ToString(model.PortalId)));

            eventAggregator.Detach(myZnodePortalCountryToken);
        }

        /// <summary>
        /// Delete Donut Webstore cache.
        /// </summary>
        /// <param name="model"></param>
        private void OnWebstorePageChange(string portalIds)
        {
            ClearWebstorePageCache(portalIds);

            eventAggregator.Detach(myWebstorePageToken);
        }


        private int[] GetPortalIds(IEnumerable<int> catalogids)
        {
            //get portal id
            IZnodeRepository<ZnodePortalCatalog> portalCatalog = new ZnodeRepository<ZnodePortalCatalog>();

            var portal = (from p in portalCatalog.Table
                          where catalogids.Contains(p.PublishCatalogId)
                          select new
                          {
                              p.PortalId
                          }).ToArray();
            int[] terms = new int[portal.Count()];
            int i = 0;
            foreach (var item in portal)
            {
                terms[i] = Convert.ToInt32(item.PortalId);
                i++;
            }

            return terms;
        }

        private int[] GetAllPortalIds()
        {
            //get portal id
            IZnodeRepository<ZnodePortal> portal = new ZnodeRepository<ZnodePortal>();
            var portalids = (from p in portal.Table
                             select new
                             {
                                 p.PortalId
                             }).ToArray();
            int[] terms = new int[portalids.Count()];
            int i = 0;
            foreach (var item in portalids)
            {
                terms[i] = Convert.ToInt32(item.PortalId);
                i++;
            }

            return terms;
        }

        /// <summary>
        /// Publishes the Content page.
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishContentPage(CMSTypeMappingModel model)
        {
            CMSPreviewHelper.PublishWidgetToPreview(model);

            eventAggregator.Detach(myContentPagePreviewModelToken);
        }

        /// <summary>
        /// Purge Cloudflare Cache.
        /// </summary>
        /// <param name="cloudflarePurgeModel"></param>
        private void OnPublishStore(CloudflarePurgeModel cloudflarePurgeModel)
        {
            if (HelperUtility.IsNull(cloudflarePurgeModel))
            {
                return;
            }

            new CloudflareHelper().PurgeEverythingByPortalId(string.Join(",", cloudflarePurgeModel.PortalId));

            eventAggregator.Detach(clearCloudflareCache);
        }

        /// <summary>
        /// To clear catalog keywords redirect list cache.
        /// </summary>
        /// <param name="searchKeywordsRedirect"></param>
        private void OnKeywordsRedirectListChange(SearchKeywordsRedirectModel searchKeywordsRedirect)
        {
            //Clear API cache 
            ClearApiCache("getcatalogkeywordsredirectlist");            

            eventAggregator.Detach(searchKeywordsRedirectToken);
        }

        /// <summary>
        /// To clear portal approval details cache.
        /// </summary>
        /// <param name="portalApprovalDetails">Portal approval details.</param>
        private void OnPortalApprovalDetailsChange(PortalApprovalModel portalApprovalDetails)
        {
            //Clear API cache 
            ClearApiCache("getportalapprovaldetailsbyid");

            eventAggregator.Detach(portalApprovalDetailsToken);
        }
    }
}
