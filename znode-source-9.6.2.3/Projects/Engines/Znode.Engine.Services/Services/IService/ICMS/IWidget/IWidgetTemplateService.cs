﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public interface IWidgetTemplateService
    {
        /// <summary>
        /// Get the List of Widget Template
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>WidgetTemplateListModel model</returns>
        WidgetTemplateListModel GetWidgetTemplateList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Widget Template
        /// </summary>
        /// <param name="widgetTemplate">WidgetTemplateCreateModel model</param>
        /// <returns>WidgetTemplateModel model</returns>
        WidgetTemplateModel CreateWidgetTemplate(WidgetTemplateCreateModel widgetTemplate);

        /// <summary>
        /// Get Widget Template
        /// </summary>
        /// <param name="widgetTemplateId">Widget Template Id</param>
        /// <returns>WidgetTemplateModel model</returns>
        WidgetTemplateModel GetWidgetTemplate(string templateCode);

        /// <summary>
        /// Update Widget Template
        /// </summary>
        /// <param name="widgetTemplateModel">WidgetTemplateUpdateModel model</param>
        /// <returns>WidgetTemplate model</returns>
        WidgetTemplateModel UpdateWidgetTemplate(WidgetTemplateUpdateModel widgetTemplateModel);

        /// <summary>
        /// Delete Widget Template
        /// </summary>
        /// <param name="WidgetTemplateIds">Widget Template Ids</param>
        /// <returns>status</returns>
        bool DeleteWidgetTemplateById(ParameterModel WidgetTemplateIds);

        /// <summary>
        /// Delete Template By Code
        /// </summary>
        /// <param name="templateCode">templateCode</param>
        /// <returns>Status</returns>
        bool DeleteWidgetTemplateByCode(string templateCode);

        /// <summary>
        /// Validate if Widget Template Exists
        /// </summary>
        /// <param name="code">Widget Template code</param>
        /// <returns>status</returns>
        bool IsWidgetTemplateExists(string templateCode);





    }
}
