﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class AddToCartStatusModel : BaseModel
    {       
        public bool Status { get; set; }
        public int SavedCartId { get; set; }
        public string CookieMapping { get; set; }
        public int CookieMappingId { get; set; }
        public decimal CartCount { get; set; }
    }
}
