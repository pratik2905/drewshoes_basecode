﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RecommendationDataGenerationRequestModel
    {
        public int? PortalId { get; set; }
        public bool IsBuildPartial { get; set; }
    }
}
