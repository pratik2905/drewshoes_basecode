﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ImpersonationActivityListModel : BaseListModel
    {
        public List<ImpersonationActivityLogModel> LogActivityList { get; set; }

        public ImpersonationActivityListModel()
        {
            LogActivityList = new List<ImpersonationActivityLogModel>();
        }
    }
}
