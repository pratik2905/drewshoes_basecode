﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class QuickOrderProductListModel : BaseListModel
    {
        public List<QuickOrderProductModel> Products { get; set; }
    }
}
