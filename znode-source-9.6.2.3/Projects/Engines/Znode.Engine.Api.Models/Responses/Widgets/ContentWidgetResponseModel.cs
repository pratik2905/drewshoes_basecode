﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ContentWidgetResponseModel : BaseModel
    {
        public int ContentWidgetId { get; set; }
        public int? PortalId { get; set; }
        public int? WidgetTemplateId { get; set; }
        public string StoreCode { get; set; }
        public string WidgetKey { get; set; }
        public string IsGlobalContentWidget { get; set; }
        public string Tags { get; set; }
        public string FamilyCode { get; set; }
        public string FamilyName { get; set; }
        public string StoreName { get; set; }
        public int FamilyId { get; set; }
        public int GlobalEntityId { get; set; }
        public string Name { get; set; }
        public string TemplateName { get; set; }

        public List<AssociatedVariantModel> WidgetVariants { get; set; }
    }
}
