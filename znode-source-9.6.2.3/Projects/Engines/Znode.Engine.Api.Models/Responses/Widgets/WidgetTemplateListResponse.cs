﻿using System.Collections.Generic;

namespace Znode.Engine.Api.Models.Responses
{
    public class WidgetTemplateListResponse : BaseListResponse
    {

        public WidgetTemplateModel widgetTemplate { get; set; }

        public List<WidgetTemplateModel> WidgetTemplates { get; set; }
    }
}
