﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Models.Responses
{
    public class ContentWidgetResponse : BaseListResponse
    {
        public ContentWidgetResponseModel ContentWidgetModel { get; set; }
    }
}
