﻿namespace Znode.Engine.Api.Models
{
    public class LBDetailsModel : BaseModel
    {
        public bool IsAPILBActive { get; set; }
        public string APILBDetails { get; set; }
        public bool IsWebstoreLBActive { get; set; }
        public string WebstoreLBDetails { get; set; }
    }
}
