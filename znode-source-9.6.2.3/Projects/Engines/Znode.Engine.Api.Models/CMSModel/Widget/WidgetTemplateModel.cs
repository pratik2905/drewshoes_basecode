﻿

namespace Znode.Engine.Api.Models
{
    public class WidgetTemplateModel : BaseModel
    {
        public int WidgetTemplateId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string FileName { get; set; }
        public int? MediaId { get; set; }
        public string MediaPath { get; set; }
        public string MediaThumbNailPath { get; set; }
        public int Version { get; set; }
    }
}
