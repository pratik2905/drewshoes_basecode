﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ContentWidgetListModel : BaseListModel
    {
        public List<ContentWidgetResponseModel> WidgetList { get; set; }
    }
}
