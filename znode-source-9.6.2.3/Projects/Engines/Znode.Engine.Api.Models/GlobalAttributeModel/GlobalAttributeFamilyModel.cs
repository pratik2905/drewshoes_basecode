﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Resources;

namespace Znode.Engine.Api.Models
{
    public class GlobalAttributeFamilyModel : BaseModel
    {
        public int GlobalAttributeFamilyId { get; set; }
        public string FamilyCode { get; set; }
        public List<GlobalAttributeFamilyLocaleModel> AttributeFamilyLocales { get; set; }
        public string AttributeFamilyName { get; set; }
        public int GlobalEntityId { get; set; }
        public string EntityName { get; set; }
    }
}
