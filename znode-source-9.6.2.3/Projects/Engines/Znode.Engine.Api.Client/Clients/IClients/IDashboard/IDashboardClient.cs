﻿using System.Threading.Tasks;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public interface IDashboardClient : IBaseClient
    {
        /// <summary>
        /// Gets top products list
        /// </summary>
        /// <param name="filters">filters for top products list</param>
        /// <param name="sorts">sorts for top products list</param>
        /// <param name="pageIndex">pageIndex for top products list</param>
        /// <param name="pageSize">pageSize for top products list</param>
        /// <returns>returns the list of top products</returns>
        Task<DashboardTopItemsListModel> GetDashboardTopProductsList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets top categories list
        /// </summary>
        /// <param name="filters">filters for top categories list</param>
        /// <param name="sorts">sorts for top categories list</param>
        /// <param name="pageIndex">pageIndex for top categories list</param>
        /// <param name="pageSize">pageSize for top categories list</param>
        /// <returns>returns the list of top categories</returns>
        Task<DashboardTopItemsListModel> GetDashboardTopCategoriesList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets the top brands list
        /// </summary>
        /// <param name="filters">filters for top brands list</param>
        /// <param name="sorts">sorts for top brands list</param>
        /// <param name="pageIndex">pageIndex for top brands list</param>
        /// <param name="pageSize">pageSize for top brands list</param>
        /// <returns>returns the list of top brands</returns>
        Task<DashboardTopItemsListModel> GetDashboardTopBrandsList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets the top searches list
        /// </summary>
        /// <param name="filters">filters for top searches list</param>
        /// <param name="sorts">sorts for top searches list</param>
        /// <param name="pageIndex">pageIndex for top searches list</param>
        /// <param name="pageSize">pageSize for top searches list</param> 
        /// <returns>returns the list of top searches</returns>
        Task<DashboardTopItemsListModel> GetDashboardTopSearchesList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets total sales, total orders, total customers and average orders
        /// </summary>
        /// <param name="filters">filters for revenue details list</param>
        /// <param name="sorts">sorts for revenue details list</param>
        /// <param name="pageIndex">pageIndex for revenue details list</param>
        /// <param name="pageSize">pageSize for revenue details list</param> 
        /// <returns>returns total sales, total orders, total customers and average orders</returns>
        Task<DashboardTopItemsListModel> GetDashboardSalesDetails(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// //Gets the count of low inventory products
        /// </summary>
        /// <param name="filters">filters for revenue details list</param>
        /// <param name="sorts">sorts for revenue details list</param>
        /// <param name="pageIndex">pageIndex for revenue details list</param>
        /// <param name="pageSize">pageSize for revenue details list</param>
        /// <returns>returns the count of low inventory products</returns>
        Task<DashboardTopItemsListModel> GetDashboardLowInventoryProductCount(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets total sales, total orders, total customers and average orders
        /// </summary>
        /// <param name="filters">filters for revenue details list</param>
        /// <param name="sorts">sorts for revenue details list</param>
        /// <param name="pageIndex">pageIndex for revenue details list</param>
        /// <param name="pageSize">pageSize for revenue details list</param> 
        /// <returns>returns total sales, total orders, total customers and average orders</returns>
        Task<DashboardTopItemsListModel> GetDashboardSalesCountDetails(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

    }
}
