﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class ImportEndpoint : BaseEndpoint
    {
        //Get all templates with respect to header id
        public static string GetAllTemplates(int importHeadId, int familyId) => $"{ApiRoot}/import/getalltemplates/{importHeadId}/{familyId}";
        
        //Get all import types
        public static string GetImportTypeList() => $"{ApiRoot}/import/getimporttypelist";
        
        //Get template data with respect to template id
        public static string GetTemplateData(int templateId, int importHeadId, int familyId) => $"{ApiRoot}/import/gettemplatedata/{templateId}/{importHeadId}/{familyId}";
        
        //Post and process import data
        public static string ImportData() => $"{ApiRoot}/import/importdata";

        //Download template
        public static string DownloadTemplate(int importHeadId, int downloadImportFamilyId) => $"{ApiRoot}/import/downloadtemplate/{importHeadId}/{downloadImportFamilyId}";

        //Get the Import logs
        public static string GetImportLogs() => $"{ApiRoot}/import/getimportlogs";

        //Get the Import logs status
        public static string GetImportLogStatus(int importProcessLogId) => $"{ApiRoot}/import/getlogstatus/{importProcessLogId}";

        //Get the import log details
        public static string GetImportLogDetails(int importProcessLogId) => $"{ApiRoot}/import/getimportlogdetails/{importProcessLogId}";

        //Delete the Import log records from ZnodeImportProcessLog as well as ZnodeImportLog table
        public static string DeleteLog() => $"{ApiRoot}/import/delete";

        //Get all families for product import
        public static string GetAllFamilies(bool isCategory) => $"{ApiRoot}/import/getfamilies/{isCategory}";

        //Update Mappings
        public static string UpdateMappings() => $"{ApiRoot}/import/updatemappings";

        //check import status
        public static string CheckImportProcess() => $"{ApiRoot}/import/checkimportprocess";

        // Get default template.
        public static string GetDefaultTemplate(string importType) => $"{ApiRoot}/import/getdefaulttemplate/{importType}";
    }
}
