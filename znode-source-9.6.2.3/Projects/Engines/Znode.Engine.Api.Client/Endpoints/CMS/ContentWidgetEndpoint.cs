﻿

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ContentWidgetEndpoint: BaseEndpoint
    {
        //Get List of Content Widget
        public static string List() => $"{ApiRoot}/contentwidget/list";

        //Create Content Widget
        public static string Create() => $"{ApiRoot}/ContentWidget/create";

        //Get Content Widget
        public static string GetContentWidget(string widgetKey) => $"{ApiRoot}/contentwidget/getcontentwidget/{widgetKey}";

        //Update Content Widget
        public static string Update() => $"{ApiRoot}/contentwidget/update"; 

        //Get Associated Variants
        public static string GetAssociatedVariants(string widgetKey) => $"{ApiRoot}/contentwidget/getassociatedvariants/{widgetKey}";

        //Associate Variants
        public static string AssociateVariant() => $"{ApiRoot}/contentwidget/associatevariant";

        //Delete Variant
        public static string DeleteAssociateVariant(int variantId) => $"{ApiRoot}/contentwidget/deleteassociatedvariant/{variantId}";

        //Delete Content Widget
        public static string DeleteContentWidget() => $"{ApiRoot}/contentwidget/deletecontentWidget";

        //Verify if the Content Widget Exist
        public static string IsWidgetKeyExist(string widgetKey) => $"{ApiRoot}/contentwidget/iswidgetkeyexists/{widgetKey}";

        //Associate Widget Template
        public static string AssociateWidgetTemplate(int variantId, int widgetTemplateId) => $"{ApiRoot}/contentwidget/associatewidgettemplate/{variantId}/{widgetTemplateId}";

    }


}
