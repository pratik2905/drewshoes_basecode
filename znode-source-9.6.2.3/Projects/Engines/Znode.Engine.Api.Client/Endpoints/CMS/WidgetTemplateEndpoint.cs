﻿
namespace Znode.Engine.Api.Client.Endpoints
{
    public class WidgetTemplateEndpoint : BaseEndpoint
    {
        //Get the List of Widget Templates
        public static string List() => $"{ApiRoot}/widgettemplate/list";

        //Create Widget Templates
        public static string Create() => $"{ApiRoot}/widgettemplate/create";

        //Get Widget Templates
        public static string Get(string templateCode) => $"{ApiRoot}/widgettemplate/get/{templateCode}";

        //Update Widget Templates
        public static string Update() => $"{ApiRoot}/widgettemplate/update";

        //Delete Widget Templates
        public static string Delete() => $"{ApiRoot}/widgettemplate/delete";

        //Verify if the Widget Templates Exists
        public static string IsWidgetTemplateExist(string templateCode) => $"{ApiRoot}/widgettemplate/iswidgettemplateexist/{templateCode}";




    }
}

