﻿CREATE  PROCEDURE [dbo].[Znode_RevertOrderInventory]
(   @OmsOrderDetailsId   INT,
    @OmsOrderLineItemIds VARCHAR(2000) = '',
    @Status              BIT OUT,
    @UserId              INT,
	@IsRevertAll         BIT = 0)
AS 
   /* Summary: this proceedure is used to revert the order  inventory in case of order revert
      Unit Testing:
	  begin tran
	  EXEC  Znode_RevertOrderInventory 
      rollback tran
    */
     BEGIN
         BEGIN TRAN ZROI;
         BEGIN TRY
             SET NOCOUNT ON;
			 DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
			 DECLARE @Revert NVARCHAR(MAX), @SQL NVARCHAR(MAX)
			 
			 SET @SQL = '
			
             UPDATE ZI
               SET
                   ZI.Quantity = ZI.Quantity + ZOOW.Quantity,
                   ZI.MOdifiedBy = '''+CAST(@UserId AS VARCHAR(1000))+''',
                   ZI.ModifiedDate = '''+CAST(@GetDate AS VARCHAR(1000))+'''
             FROM ZnodeOmsOrderWarehouse ZOOW
                  INNER JOIN ZnodeOmsOrderLineItems ZOOLI ON(ZOOLI.OmsOrderLineItemsId = ZOOW.OmsOrderLineItemsId)
                  INNER JOIN ZnodeInventory ZI ON(ZI.WarehouseId = ZOOW.WarehouseId
                                                  AND ZI.SKU = ZOOLI.SKU)
             WHERE 
			  ZOOLI.OmsOrderDetailsId = '+CAST(@OmsOrderDetailsId AS VARCHAR(1000))+'
                   AND EXISTS
             (
                 SELECT TOP 1 1     FROM dbo.split('''+@OmsOrderLineItemIds+''', '','') SP WHERE Sp.Item = ZOOLI.OmsOrderLineItemsId OR Sp.Item = ZOOLI.ParentOmsOrderLineItemsId OR '''+@OmsOrderLineItemIds+''' = ''''
             )  
			 ' +CASE WHEN @IsRevertAll = 0 THEN 'AND NOT EXISTS   (SELECT TOP  1 1 FROM ZnodeOmsOrderState OOS WHERE OOS.OrderStateName = ''RETURNED'' AND OOS.OmsOrderStateId = ZOOLI.OrderLineItemStateId) ' ELSE '' END

			

			 PRINT @sql

			 EXEC(@SQL)

             SET @Status = 1;
             SELECT 1 ID,
                    CAST(1 AS BIT) Status;
             COMMIT TRAN ZROI;
         END TRY
         BEGIN CATCH
              DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			   @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_RevertOrderInventory @OmsOrderDetailsId = '+CAST(@OmsOrderDetailsId AS VARCHAR(200))+',@OmsOrderLineItemIds='+@OmsOrderLineItemIds+',@UserId='+CAST(@UserId AS VARCHAR(200))+',@Status='+CAST(@Status AS VARCHAR(200));
             SET @Status = 0;
             SELECT 0 AS ID,
                    CAST(0 AS BIT) AS Status;
			 ROLLBACK TRAN ZROI;
             EXEC Znode_InsertProcedureErrorLog
                  @ProcedureName = 'Znode_RevertOrderInventory',
                  @ErrorInProcedure = @Error_procedure,
                  @ErrorMessage = @ErrorMessage,
                  @ErrorLine = @ErrorLine,
                  @ErrorCall = @ErrorCall;
         END CATCH;
     END;