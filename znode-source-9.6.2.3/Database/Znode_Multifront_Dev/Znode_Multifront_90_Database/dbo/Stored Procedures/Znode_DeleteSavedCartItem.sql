﻿CREATE PROCEDURE Znode_DeleteSavedCartItem  
(  
@OmsCookieMappingId INT = 0  
,@UserId INT = 0  
,@Status Bit OUT  
)  
/*  
 EXEC Znode_DeleteSavedCartItem  
 
*/  
 
AS  
BEGIN  
  BEGIN TRAN  
  BEGIN TRY  
     if(@OmsCookieMappingId < 1)
BEGIN
set @OmsCookieMappingId =(SELECT TOP 1 OmsCookieMappingId FROM ZnodeOmsCookieMapping WHERE USERID = @UserId )
END

    DECLARE @OmsSavedCartId INT =  (SELECT TOP 1 OmsSavedCartId FROM ZnodeOmsSavedCart WHERE OmsCookieMappingId = @OmsCookieMappingId )  
     
 DELETE FROM ZnodeOmsPersonalizeCartItem WHERE EXISTS (SELECT TOP 1 1 FROM ZnodeOmsSavedCartLineItem  
  WHERE ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId =  ZnodeOmsSavedCartLineItem.OmsSavedCartLineItemId  
  AND ZnodeOmsSavedCartLineItem.OmsSavedCartId = @OmsSavedCartId )  
   
 DELETE FROM ZnodeOmsSavedCartLineItem  
  WHERE ZnodeOmsSavedCartLineItem.OmsSavedCartId = @OmsSavedCartId  
   
 --DELETE FROM ZnodeOmsSavedCart WHERE OmsSavedCartId = @OmsSavedCartId  
 
 --DELETE FROM ZnodeOmsCookieMapping WHERE OmsCookieMappingId = @OmsCookieMappingId  
 
  SET @Status = 1  
 
  SELECT @OmsCookieMappingId Id , CAST(1 AS BIT ) Status  
 
  COMMIT TRAN  
 
  END TRY  
  BEGIN CATCH  
     SET @Status = 1  
  
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_DeleteSavedCartItem @OmsCookieMappingId = '+CAST(@OmsCookieMappingId AS VARCHAR(50))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
	SELECT @OmsCookieMappingId Id , CAST(0 AS BIT ) Status                  
		  
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_DeleteSavedCartItem',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall; 

  ROLLBACK TRAN  
    
  END CATCH  
 
END