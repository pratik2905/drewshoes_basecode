﻿CREATE PROCEDURE [dbo].[Znode_DeleteSaveCartLineItem]
(
	  @OmsSavedCartLineItemId  int,
	  @Status bit OUT 
)
AS 
BEGIN
	
	BEGIN TRY
	SET NOCOUNT ON;

		DECLARE @TBL_DeleteSavecartLineitems TABLE (OmsSavedCartLineItemId int)
		IF OBJECT_ID(N'tempdb..#TBL_ZnodeOmsSavedCartLineItem') IS NOT NULL
			DROP TABLE #TBL_ZnodeOmsSavedCartLineItem

		----Getting date related to @OmsSavedCartLineItemId input parameter into a table
		SELECT OmsSavedCartLineItemId,	ParentOmsSavedCartLineItemId   
			INTO #TBL_ZnodeOmsSavedCartLineItem  from ZnodeOmsSavedCartLineItem  with (NOLOCK)
			where OmsSavedCartLineItemId = @OmsSavedCartLineItemId or ParentOmsSavedCartLineItemId =@OmsSavedCartLineItemId 

		--selecting all the child line items and the parent line Item which has no child item
			INSERT INTO @TBL_DeleteSavecartLineitems
				SELECT OmsSavedCartLineItemId from #TBL_ZnodeOmsSavedCartLineItem
					union
					SELECT ParentOmsSavedCartLineItemId from #TBL_ZnodeOmsSavedCartLineItem
						where not exists (select  OmsSavedCartLineItemId,	ParentOmsSavedCartLineItemId from ZnodeOmsSavedCartLineItem   with (NOLOCK)
								where OmsSavedCartLineItemId != #TBL_ZnodeOmsSavedCartLineItem.OmsSavedCartLineItemId and  ParentOmsSavedCartLineItemId =#TBL_ZnodeOmsSavedCartLineItem.ParentOmsSavedCartLineItemId)
								and ParentOmsSavedCartLineItemId is not null
	BEGIN TRAN DeleteSaveCartLineItem;

			IF exists (select top 1 1 from @TBL_DeleteSavecartLineitems)
			Begin
						DELETE FROM ZnodeOmsPersonalizeCartItem
						WHERE EXISTS
						(
							SELECT TOP 1 1
							FROM @TBL_DeleteSavecartLineitems DeleteSaveCart
							WHERE DeleteSaveCart.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId
						);
						DELETE ZnodeOmsSavedCartLineItemDetails
						WHERE EXISTS
						(
							SELECT TOP 1 1
							FROM @TBL_DeleteSavecartLineitems DeleteSaveCart
							WHERE DeleteSaveCart.OmsSavedCartLineItemId = ZnodeOmsSavedCartLineItemDetails.OmsSavedCartLineItemId
		
						);
						DELETE FROM ZnodeOmsSavedCartLineItem 
						WHERE EXISTS
						(
							SELECT TOP 1 1
							FROM @TBL_DeleteSavecartLineitems DeleteSaveCart
							WHERE DeleteSaveCart.OmsSavedCartLineItemId = ZnodeOmsSavedCartLineItem.OmsSavedCartLineItemId
						);

			End	
	COMMIT TRAN DeleteSaveCartLineItem;
	SET @Status = 1;
	
	END TRY
	BEGIN CATCH
		SET @Status = 0;
		DECLARE @Error_procedure varchar(1000)= ERROR_PROCEDURE(), @ErrorMessage nvarchar(max)= ERROR_MESSAGE(), @ErrorLine varchar(100)= ERROR_LINE(), @ErrorCall nvarchar(max)= 'EXEC Znode_DeleteSaveCartLineItem @OmsSavedCartLineItemId = '+CAST(@OmsSavedCartLineItemId AS varchar(max))
		SELECT 0 AS ID, CAST(0 AS bit) AS Status,ERROR_MESSAGE();
		ROLLBACK TRAN DeleteSaveCartLineItem;
		EXEC Znode_InsertProcedureErrorLog @ProcedureName = 'Znode_DeleteSaveCartLineItem', @ErrorInProcedure = @Error_procedure, @ErrorMessage = @ErrorMessage, @ErrorLine = @ErrorLine, @ErrorCall = @ErrorCall;
	END CATCH;
END