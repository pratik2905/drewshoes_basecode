﻿create PROCEDURE [dbo].Znode_GetBillingShippingAddress
(
	 @BillingaddressId        INT   = 0,
    @orderShipmentId        INT   = 0
   
)
AS 
/*
	 Summary :- This Procedure is used to get the display the shipping and billing address 
	 Unit Testig 
	 EXEC  GetBillingShippingAddress 1,1
	 
*/
   BEGIN 
		BEGIN TRY 
		--SET NOCOUNT ON ;
		
			declare @addressid int =0;
			select @addressid =AddressId  from ZnodeOmsOrderShipment where OmsOrderShipmentId = @orderShipmentId
			
			
			IF OBJECT_ID('tempdb..#ZnodeAddress') IS NOT NULL DROP TABLE #ZnodeAddress
			select AddressId,
FirstName,
LastName,
DisplayName,
CompanyName,
Address1,
Address2,
Address3,
CountryName,
StateName StateCode,
CityName,
PostalCode,
PhoneNumber,
Mobilenumber,
AlternateMobileNumber,
FaxNumber,
IsDefaultBilling,
IsDefaultShipping,
IsActive,
ExternalId,
IsShipping,
IsBilling,
EmailAddress  into #ZnodeAddress  from ZnodeAddress where AddressId= @BillingaddressId or AddressId= @addressid

			select ZA.*,ZS.StateName from #ZnodeAddress ZA
				inner join ZnodeState ZS on ZS.StateCode = ZA.StateCode and ZS.CountryCode = ZA.CountryName
		
		 END TRY 
		 BEGIN CATCH 
			DECLARE @Status BIT ;
			SET @Status = 0;
			DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), 
			@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetBillingShippingAddress @BillingaddressId = '''+ISNULL(@BillingaddressId,'''''')+''',@orderShipmentId='+ISNULL(CAST(@orderShipmentId AS
			VARCHAR(50)),'''''')
              			 
			SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
			EXEC Znode_InsertProcedureErrorLog
					@ProcedureName = 'Znode_GetBillingShippingAddress',
					@ErrorInProcedure = 'Znode_GetBillingShippingAddress',
					@ErrorMessage = @ErrorMessage,
					@ErrorLine = @ErrorLine,
					@ErrorCall = @ErrorCall;
		 END CATCH 
   END
GO


