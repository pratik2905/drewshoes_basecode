﻿
CREATE PROCEDURE [dbo].[Znode_GetPublishAssociatedAddonsJson]
											(
												@PublishCatalogId NVARCHAR(MAX) = 0,
												@PimProductId    TransferId Readonly,
												@VersionId        INT           = 0,
												@UserId           INT,														 
												@PimCategoryHierarchyId int = 0, 
												@LocaleId       TransferId READONLY,
												@PublishStateId INT = 0, 
												@VersionIdString  VARCHAR(100)= '',
												@Status		   Bit  OutPut,
												@RevisionType Varchar(50)
											)
AS 
   
/*
    Summary : If PimcatalogId is provided get all products with Addons and provide above mentioned data
              If PimProductId is provided get all Addons if associated with given product id and provide above mentioned data
    			Input: @PublishCatalogId or @PimProductId
    		    output should be in XML format
              sample xml5
    Begin transaction  
	 declare  @PimProductId TransferId 
	 Declare @Status bit 
	 insert into @PimProductId  values (768)

			Exec [_Znode_GetPublishAssociatedAddonsJson]
				@PublishCatalogId = 0 ,
				@PimProductId   = @PimProductId,
				@UserId =2,														 
				@VersionIdString = '',--'1662,1664,1663,1665',
				@Status	 =@Status Out,
				@RevisionType= 'Production'
				rollback transaction
				--select * from ZnodePublishVersionEntity 
   
	*/

     BEGIN
        
         BEGIN TRY
          SET NOCOUNT ON 
			 DECLARE @GetDate DATETIME= dbo.Fn_GetDate();
             DECLARE @LocaleIdIn INT, @DefaultLocaleId INT= dbo.Fn_GetDefaultLocaleId()
			 , @Counter INT= 1
			 , @MaxRowId INT= 0;

            -- DECLARE @PimAddOnGroupId VARCHAR(MAX);

			 CREATE TABLE #TBL_PublisshIds  (PublishProductId INT , PimProductId INT , PublishCatalogId INT)
			 DECLARE @TBL_LocaleId TABLE (RowId    INT IDENTITY(1, 1),LocaleId INT);
			 IF OBJECT_ID('tempdb..#VesionIds') is not null
				DROP TABLE #VesionIds
			 Create Table #VesionIds(ZnodeCatalogId int , VersionId int , LocaleId int , RevisionType varchar(50) )

  			 If @VersionIdString <> ''		 
				Insert into #VesionIds (ZnodeCatalogId, VersionId, LocaleId, RevisionType)	
				SELECT PV.ZnodeCatalogId, PV.VersionId, PV.LocaleId, PV.RevisionType FROM ZnodePublishVersionEntity PV Inner join Split(@VersionIdString,',') S ON PV.VersionId = S.Item
			 Else 
				Begin
					If  (@RevisionType like '%Preview%'  OR @RevisionType like '%Production%' )
						Insert into #VesionIds (ZnodeCatalogId, VersionId, LocaleId, RevisionType)	
						SELECT PV.ZnodeCatalogId, PV.VersionId, PV.LocaleId, PV.RevisionType FROM ZnodePublishVersionEntity PV  where PV.IsPublishSuccess =1 
						AND PV.RevisionType ='Preview'

					If  (@RevisionType like '%Production%' OR @RevisionType = 'None')
						Insert into #VesionIds (ZnodeCatalogId, VersionId, LocaleId, RevisionType)	
						SELECT PV.ZnodeCatalogId, PV.VersionId, PV.LocaleId, PV.RevisionType FROM ZnodePublishVersionEntity PV  where PV.IsPublishSuccess =1 
						AND PV.RevisionType ='Production'
										
				End 
			 
			IF  @PublishCatalogId IS NULL  OR @PublishCatalogId = 0 
			 BEGIN 
			   INSERT INTO #TBL_PublisshIds 
			   EXEC [dbo].[Znode_InsertPublishProductIds] @PublishCatalogId,@userid,@PimProductId,1
			 END 
			 IF  ISnull(@PimCategoryHierarchyId,0) <> 0 
			 BEGIN 
			   INSERT INTO #TBL_PublisshIds 
			   EXEC [dbo].[Znode_InsertPublishProductIds] @PublishCatalogId,@userid,@PimProductId,1,@PimCategoryHierarchyId 
			 END 

			 CREATE TABLE #TBL_PublishCatalogId (PublishCatalogId INT,PublishProductId INT,PimProductId  INT , VersionId INT ,LocaleId INT  );

			 IF  ISnull(@PimCategoryHierarchyId,0) <> 0 
			 BEGIN 
					INSERT INTO #TBL_PublishCatalogId 
					SELECT ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId, MAX(ZPCP.PublishCatalogLogId)  ,LocaleId 
					FROM ZnodePublishProduct ZPP 
					INNER JOIN ZnodePublishCatalogLog ZPCP ON (ZPCP.PublishCatalogId  = ZPP.PublishCatalogId)
					WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PublisshIds SP WHERE SP.PublishProductId = ZPP.PublishProductId   ) 
					AND ZPCP.Publishstateid = @PublishStateId
					GROUP BY ZPP.PublishCatalogId,ZPP.PublishProductId,PimProductId,LocaleId 
			 END 
			 ELSE 
			 Begin
					INSERT INTO #TBL_PublishCatalogId  
					 SELECT ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId,0  VersionId  ,0 LocaleId 
					 FROM ZnodePublishProduct ZPP  
					 WHERE EXISTS  (SELECT TOP 1 1 FROM #VesionIds ZPCP WHERE (ZPCP.ZnodeCatalogId  = ZPP.PublishCatalogId)) AND
					 (EXISTS (SELECT TOP 1 1 FROM #TBL_PublisshIds SP WHERE SP.PublishProductId = ZPP.PublishProductId  
					 AND  @PublishCatalogId = '0' )  OR (ZPP.PublishCatalogId =  @PublishCatalogId ))
			 END
             DECLARE @TBL_AddonGroupLocale TABLE( PimAddonGroupId INT, DisplayType NVARCHAR(400),AddonGroupName NVARCHAR(MAX),LocaleId INT );
             INSERT INTO @TBL_LocaleId(LocaleId)
                    SELECT LocaleId FROM ZnodeLocale MT  WHERE IsActive = 1 AND (EXISTS (SELECT TOP 1 1  FROM @LocaleId RT WHERE RT.Id = MT.LocaleId )
					OR NOT EXISTS (SELECT TOP 1 1 FROM @LocaleId ));
             SET @MaxRowId = ISNULL(( SELECT MAX(RowId) FROM @TBL_LocaleId ), 0);
    
             WHILE @Counter <= @MaxRowId
                 BEGIN
                     SET @LocaleIdIn =( SELECT LocaleId FROM @TBL_LocaleId WHERE RowId = @Counter );
                     INSERT INTO @TBL_AddonGroupLocale (PimAddonGroupId, DisplayType, AddonGroupName)
                     EXEC Znode_GetAddOnGroupLocale '', @LocaleIdIn;
	   				 UPDATE @TBL_AddonGroupLocale SET LocaleId = @LocaleIdIn WHERE LocaleId IS NULL 
                     SET @Counter = @Counter + 1;
                 END;
		
					 IF OBJECT_ID('tempdb..#AddonProductPublishedXML') is not null
						drop table #AddonProductPublishedXML
						
						SELECT  ZPPP.PublishProductId,ZPPP.PublishCatalogId,V.LocaleId,v.VersionId,
						ZPPP.[PublishProductId]  ZnodeProductId,
						ZPPP.[PublishCatalogId] ZnodeCatalogId,
						ZPP.PublishProductId  AssociatedZnodeProductId,
						ZPAOP.DisplayOrder DisplayOrder,
						ZPOPD.DisplayOrder AssociatedProductDisplayOrder,
						RequiredType ,
						DisplayType, 
						ISNULL((select ''+AddonGroupName for xml path('')),'') GroupName,
						ISnull(IsDefault,0) IsDefault
						INTO #AddonProductPublishedXML
						FROM [ZnodePimAddOnProductDetail] AS ZPOPD
						INNER JOIN [ZnodePimAddOnProduct] AS ZPAOP ON ZPOPD.[PimAddOnProductId] = ZPAOP.[PimAddOnProductId]
						INNER JOIN #TBL_PublishCatalogId ZPPP ON (ZPPP.PimProductId = ZPAOP.PimProductId )
						INNER JOIN #TBL_PublishCatalogId ZPP ON(ZPP.PimProductId = ZPOPD.[PimChildProductId] AND ZPP.PublishCatalogId = ZPPP.PublishCatalogId  )
						INNER JOIN #VesionIds V ON ZPPP.PublishCatalogId  = V.ZnodeCataLogId
						INNER JOIN @TBL_AddonGroupLocale TBAG ON (TBAG.PimAddonGroupId   = ZPAOP.PimAddonGroupId AND TBAG.LocaleId = V.LocaleId )
					
					If (isnull(@PublishCatalogId ,'') = '' and @VersionIdString = '') OR 
					 (isnull(@PublishCatalogId ,'') = 0 and @VersionIdString = '')
					Begin 
			 			Delete from ZnodePublishAddonEntity where   PublishAddonEntityId  in (
						Select PublishAddonEntityId from ZnodePublishAddonEntity  BP inner join 
						#AddonProductPublishedXML A ON BP.ZnodeProductId =A.PublishProductId AND 
						BP.ZnodeCatalogId = A.PublishCatalogId 
						AND BP.VersionId = A.VersionId
						) 
					End 
					
						Insert into  ZnodePublishAddonEntity
						(VersionId,ZnodeProductId,ZnodeCatalogId,AssociatedZnodeProductId,AssociatedProductDisplayOrder,
						 LocaleId,GroupName,DisplayType,DisplayOrder,IsRequired,RequiredType,IsDefault)
						 Select 
						 VersionId,ZnodeProductId,ZnodeCatalogId,AssociatedZnodeProductId,Isnull(AssociatedProductDisplayOrder,0),
						 LocaleId,GroupName,DisplayType,DisplayOrder,1 IsRequired,RequiredType,IsDefault from 
						 #AddonProductPublishedXML
						 SET @Status = 1;
            
         END TRY
         BEGIN CATCH
		     SELECT ERROR_MESSAGE(),ERROR_PROCEDURE()
            
             SET @Status = 0;
             DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetPublishAssociatedAddonsJson @PublishCatalogId = '+@PublishCatalogId+',@VersionId='+CAST(@VersionId AS VARCHAR(50))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
             SELECT 0 AS ID,
                    CAST(0 AS BIT) AS Status;
              EXEC Znode_InsertProcedureErrorLog
                  @ProcedureName = 'Znode_GetPublishAssociatedAddonsJson',
                  @ErrorInProcedure = @Error_procedure,
                  @ErrorMessage = @ErrorMessage,
                  @ErrorLine = @ErrorLine,
                  @ErrorCall = @ErrorCall;
         END CATCH;
     END;