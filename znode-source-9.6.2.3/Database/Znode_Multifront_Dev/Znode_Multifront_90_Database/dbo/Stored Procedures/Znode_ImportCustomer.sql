﻿
CREATE PROCEDURE [dbo].[Znode_ImportCustomer](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @LocaleId int= 0,@PortalId int ,@CsvColumnString nvarchar(max))
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import SEO Details
	
	-- Unit Testing : 
	--------------------------------------------------------------------------------------

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max),@AspNetZnodeUserId nvarchar(256),@ASPNetUsersId nvarchar(256),
		@PasswordHash nvarchar(max),@SecurityStamp nvarchar(max),@RoleId nvarchar(256),@IsAllowGlobalLevelUserCreation nvarchar(10)
		Declare @ProfileId  int
		DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		 
		SET @SecurityStamp = '0wVYOZNK4g4kKz9wNs-UHw2'
		SET @PasswordHash = 'APy4Tm1KbRG6oy7h3r85UDh/lCW4JeOi2O2Mfsb3OjkpWTp1YfucMAvvcmUqNaSOlA==';
		SELECT  @RoleId  = Id from AspNetRoles where   NAME = 'Customer'  

		Select @IsAllowGlobalLevelUserCreation = FeatureValues from ZnodeGlobalsetting where FeatureName = 'AllowGlobalLevelUserCreation'

		DECLARE @GetDate datetime= dbo.Fn_GetDate();
		-- Retrive RoundOff Value from global setting 

		-- Three type of import required three table varible for product , category and brand
		DECLARE @InsertCustomer TABLE
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, UserName nvarchar(512) ,FirstName	nvarchar(200),
			LastName nvarchar(200), BudgetAmount	numeric,Email	nvarchar(100),PhoneNumber	nvarchar(100),
		    EmailOptIn	bit	,ReferralStatus	nvarchar(40),IsActive	bit	,ExternalId	nvarchar(max),CreatedDate Datetime,
			ProfileName varchar(200),AccountCode nvarchar(100),DepartmentName varchar(300),RoleName nvarchar(256), GUID NVARCHAR(400)
			,PerOrderLimit int,
				PerOrderAnnualLimit nvarchar(100),
				BillingAccountNumber nvarchar(100),
				EnableUserShippingAddressSuggestion  nvarchar(100),
				EnablePowerBIReportOnWebStore bit
		);

			--SET @SSQL = 'SELECT RowNumber,UserName,FirstName,LastName,BudgetAmount,Email,PhoneNumber,EmailOptIn,IsActive,ExternalId,GUID FROM '+ @TableName;
		SET @SSQL = 'SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		INSERT INTO @InsertCustomer( RowNumber,UserName,FirstName,LastName,Email,PhoneNumber, EmailOptIn,IsActive,ExternalId,CreatedDate,ProfileName,AccountCode,DepartmentName,RoleName 
										,PerOrderAnnualLimit
										,BillingAccountNumber
										,EnableUserShippingAddressSuggestion
										,EnablePowerBIReportOnWebStore
										,PerOrderLimit,GUID)
		EXEC sys.sp_sqlexec @SSQL;
		
		
		select TOP 1 @ProfileId   =  ProfileId from ZnodePortalprofile where Portalid = @Portalid and IsDefaultRegistedProfile=1
		If( Isnull(@ProfileId ,0) = 0 ) 
		Begin
		
		
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '62', 'Default Portal Profile', '', @NewGUId, 1 , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
							
				UPDATE ZnodeImportProcessLog
				SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
				WHERE ImportProcessLogId = @ImportProcessLogId;
			

				SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog 
				WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
				Select @SuccessRecordCount = 0

				UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
				TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
				WHERE ImportProcessLogId = @ImportProcessLogId;

				DELETE FROM @InsertCustomer 
				SET @Status = 0;

				COMMIT TRAN A;
				Return 0 
		End
	
	    -- start Functional Validation 

		-----------------------------------------------
		--If @IsAllowGlobalLevelUserCreation = 'false'
		--		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--			   SELECT '10', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--			   FROM @InsertCustomer AS ii
		--			    WHERE ltrim(rtrim(ii.UserName)) in 
		--			   (
		--				   SELECT UserName FROM AspNetZnodeUser   where PortalId = @PortalId
		--			   );
		--Else 
		--		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--			   SELECT '10', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--			   FROM @InsertCustomer AS ii
		--			   WHERE ltrim(rtrim(ii.UserName)) in 
		--			   (
		--				   SELECT UserName FROM AspNetZnodeUser   
		--			   );
		
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '35', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM @InsertCustomer AS ii
					   WHERE ii.UserName not like '%_@_%_.__%' 
				
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '30', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM @InsertCustomer AS ii
					   WHERE ltrim(rtrim(ii.UserName)) in 
					   (SELECT ltrim(rtrim(UserName))  FROM @InsertCustomer group by ltrim(rtrim(UserName))  having count(*) > 1 )

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				select '77', 'AccountCode', ii.AccountCode, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				from @InsertCustomer ii 
				where isnull(ltrim(rtrim(ii.AccountCode)),'') !='' 
				and not exists(select * from ZnodeAccount za inner join ZnodePortalAccount zpa on za.AccountId = zpa.AccountId
					where  isnull(ltrim(rtrim(ii.AccountCode)),'') = za.AccountCode and zpa.PortalId = @PortalId )
				and exists(SELECT isnull(ltrim(rtrim(AccountCode)),'') FROM ZnodeAccount za1 where isnull(ltrim(rtrim(ii.AccountCode)),'') = za1.AccountCode );

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '73', 'AccountCode', AccountCode, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				FROM @InsertCustomer AS ii
				WHERE   isnull(ltrim(rtrim(ii.AccountCode)),'') !=''   and isnull(ltrim(rtrim(ii.AccountCode)),'') not in 
				(
					SELECT isnull(ltrim(rtrim(AccountCode)),'') FROM ZnodeAccount   
				);

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '88', 'AccountCode', AccountCode, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				FROM @InsertCustomer AS ii
				WHERE   isnull(ltrim(rtrim(ii.AccountCode)),'') !='' and  exists
				(
					SELECT top 1 1 FROM  AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	 
				inner join ZnodeAccount ZA on  ZU.AccountId = ZA.AccountId
				where ANZU.UserName = ii.UserName and  ZA.AccountCode != ii.AccountCode
				);

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '92', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				FROM @InsertCustomer AS ii
				where isnull(ltrim(rtrim(ii.RoleName)),'') <> '' and  isnull(ltrim(rtrim(ii.RoleName)),'') in ('User','Manager','Administrator')
				and exists(select * from ZnodeUser a INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
						inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
						--inner join @InsertCustomer IC on (IC.UserName = c.UserName)						
						inner join AspNetUserRoles u on u.UserId = b.Id
						inner join AspNetRoles ZD on u.RoleId = zd.Id
						where (ii.UserName = c.UserName) and isnull(ltrim(rtrim(ii.RoleName)),'') <> ZD.Name )

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '75', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				FROM @InsertCustomer AS ii
				WHERE   isnull(ltrim(rtrim(ii.AccountCode)),'') ='' and isnull(ltrim(rtrim(RoleName)),'') <> '' 

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '74', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				FROM @InsertCustomer AS ii
				WHERE --ltrim(rtrim(ii.RoleName)) not in ('User','Manager','Administrator') and isnull(ltrim(rtrim(RoleName)),'') <> '' and
				 isnull(ltrim(rtrim(RoleName)),'') <> '' and not exists (select top 1 1 from  AspNetRoles ANR where name in ('User','Manager','Administrator') and  ANR.name =ii.RoleName)


				--INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				--SELECT '75', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				--FROM @InsertCustomer AS ii
				--WHERE  isnull(ltrim(rtrim(AccountCode)),'') != '' and isnull(ltrim(rtrim(RoleName)),'') <> ''

				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '76', 'DepartmentName', DepartmentName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				FROM @InsertCustomer AS ii
				WHERE isnull(ltrim(rtrim(ii.DepartmentName)),'') <> ''
				and not exists(select * from  ZnodeAccount ZA inner join ZnodeDepartment ZD on ZA.AccountId = ZD.AccountId
					where isnull(ltrim(rtrim(ii.AccountCode)),'') = ltrim(rtrim(za.AccountCode))
					and isnull(ltrim(rtrim(ii.DepartmentName)),'') = ltrim(rtrim(ZD.DepartmentName)))
				

		 UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ UserName - ' + ISNULL(UserName,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN @InsertCustomer IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL

		--Note : Content page import is not required 
		
		-- End Function Validation 	
		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  

		DELETE FROM @InsertCustomer
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
			--AND GUID = @NewGUID
		);


		-- Update Record count in log 
        
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @InsertCustomer
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End

		-- Insert Product Data 
				
				
				DECLARE @InsertedAspNetZnodeUser TABLE (AspNetZnodeUserId nvarchar(256) ,UserName nvarchar(512),PortalId int )
				DECLARE @InsertedASPNetUsers TABLE (Id nvarchar(256) ,UserName nvarchar(512))
				DECLARE @InsertZnodeUser TABLE (UserId int,AspNetUserId nvarchar(256),CreatedDate Datetime )

				UPDATE ANU SET 
				ANU.PhoneNumber	= IC.PhoneNumber, ANU.LockoutEndDateUtc = case when IC.IsActive = 0 then @GetDate when IC.IsActive = 1 then null else ANU.LockoutEndDateUtc end
				from AspNetZnodeUser ANZU 
				INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName 
				where case when @IsAllowGlobalLevelUserCreation = 'true' then -1 else Isnull(ANZU.PortalId,0) end = case when @IsAllowGlobalLevelUserCreation = 'true' then -1 else Isnull(@PortalId ,0) end
				----Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)

				UPDATE ZU SET 
				ZU.FirstName	= IC.FirstName,
				ZU.LastName		= IC.LastName,
				--ZU.MiddleName	= IC.MiddleName,
				ZU.BudgetAmount = IC.BudgetAmount,
				ZU.Email		= IC.Email,
				ZU.PhoneNumber	= IC.PhoneNumber,
				ZU.EmailOptIn	= Isnull(IC.EmailOptIn,0),
				ZU.IsActive		= IC.IsActive
				--ZU.ExternalId = ExternalId
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName 
				where case when @IsAllowGlobalLevelUserCreation = 'true' then -1 else Isnull(ANZU.PortalId,0) end = case when @IsAllowGlobalLevelUserCreation = 'true' then -1 else Isnull(@PortalId ,0) end
				--where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)

				Insert into AspNetZnodeUser (AspNetZnodeUserId, UserName, PortalId)		
				OUTPUT INSERTED.AspNetZnodeUserId, INSERTED.UserName, INSERTED.PortalId	INTO  @InsertedAspNetZnodeUser 			 
				Select NEWID(),IC.UserName, @PortalId FROM @InsertCustomer IC 
				where Not Exists (Select TOP 1 1  from AspNetZnodeUser ANZ 
				where Isnull(ANZ.PortalId,0) = Isnull(@PortalId,0) AND ANZ.UserName = IC.UserName)

				INSERT INTO ASPNetUsers (Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,
				LockoutEndDateUtc,LockOutEnabled,AccessFailedCount,PasswordChangedDate,UserName)
				output inserted.Id, inserted.UserName into @InsertedASPNetUsers
				SELECT NewId(), Email,0 ,@PasswordHash,@SecurityStamp,PhoneNumber,0,0,case when A.IsActive = 0 then @GetDate else null end LockoutEndDateUtc,1 LockoutEnabled,
				0,@GetDate,AspNetZnodeUserId from @InsertCustomer A INNER JOIN @InsertedAspNetZnodeUser  B 
				ON A.UserName = B.UserName
				
				INSERT INTO  ZnodeUser(AspNetUserId,FirstName,LastName,CustomerPaymentGUID,Email,PhoneNumber,EmailOptIn,
				IsActive,ExternalId, CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UserName)
				OUTPUT Inserted.UserId, Inserted.AspNetUserId,Inserted.CreatedDate into @InsertZnodeUser
				SELECT IANU.Id AspNetUserId ,IC.FirstName,IC.LastName,null CustomerPaymentGUID,IC.Email
				,IC.PhoneNumber,Isnull(IC.EmailOptIn,0),IC.IsActive,IC.ExternalId, @UserId,
				CASE WHEN IC.CreatedDate IS NULL OR IC.CreatedDate = '' THEN  @Getdate ELSE IC.CreatedDate END,
				@UserId,@Getdate,IC.UserName
				from @InsertCustomer IC Inner join 
				@InsertedAspNetZnodeUser IANZU ON IC.UserName = IANZU.UserName  INNER JOIN 
				@InsertedASPNetUsers IANU ON IANZU.AspNetZnodeUserId = IANU.UserName 
				  	     
				INSERT INTO AspNetUserRoles (UserId,RoleId)  Select AspNetUserId, @RoleID from @InsertZnodeUser 
				INSERT INTO ZnodeUserPortal (UserId,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) 
				SELECT UserId, @PortalId , @UserId, IZU.CreatedDate,@UserId,@Getdate 
				from @InsertZnodeUser IZU

				insert into ZnodeAccountUserPermission(UserId,AccountPermissionAccessId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT UserId, 4 , @UserId, @Getdate,@UserId,@Getdate 
				from @InsertZnodeUser IZU
				--Declare @ProfileId  int 
				--select TOP 1 @ProfileId   =  ProfileId from ZnodePortalprofile where Portalid = @Portalid and IsDefaultRegistedProfile=1

				--insert into ZnodeUserProfile (ProfileId,UserId,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				--SELECT @ProfileId  , UserId, 1 , @UserId,CreatedDate,@UserId,@Getdate from @InsertZnodeUser


------------insert into ZnodeUserGlobalAttributeValue
	
				IF OBJECT_ID('tempdb..#globaldata') IS NOT NULL DROP TABLE #globaldata
				select ZU.userid,PerOrderLimit,
									PerOrderAnnualLimit,
									BillingAccountNumber,
									EnableUserShippingAddressSuggestion,
									EnablePowerBIReportOnWebStore 
									into #globaldata
				from znodeuser ZU inner join  @InsertCustomer IC on IC.Email =ZU.EMAIL
				where ZU.userid in (select userid from @InsertZnodeUser)
				group by ZU.userid,PerOrderLimit,
									PerOrderAnnualLimit,
									BillingAccountNumber,
									EnableUserShippingAddressSuggestion,
									EnablePowerBIReportOnWebStore 

				IF OBJECT_ID('tempdb..#globaldata1') IS NOT NULL DROP TABLE #globaldata1
				select * into #globaldata1 from (select userid, Cast(PerOrderLimit as varchar(100)) PerOrderLimit,
				 Cast(PerOrderAnnualLimit as varchar(100)) PerOrderAnnualLimit,
				 Cast(BillingAccountNumber as varchar(100)) BillingAccountNumber,
				 Cast(case when cast(EnableUserShippingAddressSuggestion as varchar(10))= '1' or cast(EnableUserShippingAddressSuggestion as varchar(10)) ='YES' or  cast(EnableUserShippingAddressSuggestion as varchar(10)) ='True' then 'True' else 'False'  end  as varchar(100)) EnableUserShippingAddressSuggestion,
				 Cast(case when cast(EnablePowerBIReportOnWebStore as varchar(10))= '1'  or cast(EnablePowerBIReportOnWebStore as varchar(10)) ='YES' or  cast(EnablePowerBIReportOnWebStore as varchar(10)) ='True' then 'True' else 'False' end  as varchar(100)) EnablePowerBIReportOnWebStore from #globaldata) ab
				UNPIVOT  
				   (avalue FOR acode IN   
					  (PerOrderLimit,
				PerOrderAnnualLimit,
				BillingAccountNumber,
				EnableUserShippingAddressSuggestion,
				EnablePowerBIReportOnWebStore)  
				)AS unpvt;  

				insert into ZnodeUserGlobalAttributeValue (UserId,	GlobalAttributeId,	GlobalAttributeDefaultValueId,	AttributeValue,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
				Select g.Userid,ZGA.GlobalAttributeId,null,null,@UserId,@Getdate,@UserId,@Getdate
					from #globaldata1 g inner join ZnodeGlobalAttribute ZGA on ZGA.AttributeCode =g.acode
					where not exists (select top 1 1 from ZnodeUserGlobalAttributeValue ZGAV where ZGAV.Userid =g.Userid and ZGA.GlobalAttributeId=ZGAV.GlobalAttributeId)

				insert into ZnodeUserGlobalAttributeValuelocale(UserGlobalAttributeValueId,	LocaleId,	AttributeValue,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate,	GlobalAttributeDefaultValueId,	MediaId,	MediaPath)
				select UserGlobalAttributeValueId, @LocaleId,avalue ,@UserId,@Getdate,@UserId,@Getdate,null,null,null
					from ZnodeUserGlobalAttributeValue ZUGAV inner join #globaldata1 g on ZUGAV.userid =g.userid
						inner join ZnodeGlobalAttribute ZGA on ZGA.AttributeCode =g.acode and ZGA.GlobalAttributeId = ZUGAV.GlobalAttributeId
						where not exists (select Top 1 1 from ZnodeUserGlobalAttributeValuelocale z where z.UserGlobalAttributeValueId = ZUGAV.UserGlobalAttributeValueId)

---------------------------------------------------------------------------------

				declare @Profile table (ProfileId int)

				INSERT INTO ZnodeProfile (ProfileName,ShowOnPartnerSignup,Weighting,TaxExempt,DefaultExternalAccountNo,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,ParentProfileId)
				OUTPUT inserted.ProfileId INTO @Profile(ProfileId)
				SELECT Distinct ProfileName, 0, null,0, replace(ltrim(rtrim(ProfileName)),' ','') as DefaultExternalAccountNo, @UserId,@Getdate, @UserId,@Getdate, null as ParentProfileId				
				from @InsertCustomer IC
				where not exists(select * from ZnodeProfile ZP where IC.ProfileName = ZP.ProfileName )
				AND ISNULL(ic.ProfileName,'') <> ''

				INSERT INTO ZnodePortalProfile (PortalId,	ProfileId,	IsDefaultAnonymousProfile,	IsDefaultRegistedProfile,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
				SELECT @PortalId, ProfileId, 0 AS IsDefaultAnonymousProfile, 0 AS IsDefaultRegistedProfile, @UserId,@Getdate, @UserId,@Getdate
				from @Profile

				UPDATE ZnodeUserProfile 
				SET ProfileId = COALESCE(ZP.ProfileId,@ProfileId)
				FROM ZnodeUser a
				inner join ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join ZnodeUserProfile u ON u.UserId = a.UserId
				LEFT join ZnodeProfile ZP on IC.ProfileName = ZP.ProfileName
				--where IC.ProfileName <> ''
				
				INSERT INTO ZnodeUserProfile (ProfileId,UserId,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT COALESCE(ZP.ProfileId,@ProfileId)  , a.UserId, 1 , @UserId,a.CreatedDate,@UserId,@Getdate 
				from ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				LEFT join ZnodeProfile ZP on IC.ProfileName = ZP.ProfileName
				where NOT EXISTS (SELECT TOP  1 1 FROM ZnodeUserProfile u WHERE u.UserId = a.UserId )
				AND EXISTS(SELECT * FROM @InsertZnodeUser IZU WHERE A.UserId = IZU.UserId)

				---to update accountid agaist user
				UPDATE ZU SET ZU.AccountId = ZA.AccountId 
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	 
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName
				INNER JOIN ZnodeAccount ZA ON ZA.AccountCode = IC.AccountCode 
				--inner join @InsertZnodeUser IZU on IZU.UserId =ZU.UserId
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0) and isnull(IC.AccountCode,'') <> ''
				
				update ZDU set ZDU.DepartmentId = ZD.DepartmentId, ModifiedBy = @UserId, ModifiedDate = @Getdate
				from ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join ZnodeDepartment ZD on IC.DepartmentName = ZD.DepartmentName
				inner join ZnodeDepartmentUser ZDU on ZDU.UserId = a.UserId
				where isnull(IC.DepartmentName,'') <> ''

				insert into ZnodeDepartmentUser(UserId,DepartmentId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT a.UserId, ZD.DepartmentId, @UserId,a.CreatedDate,@UserId,@Getdate 
				from ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join ZnodeDepartment ZD on IC.DepartmentName = ZD.DepartmentName
				where NOT EXISTS (SELECT TOP  1 1 FROM ZnodeDepartmentUser u WHERE u.UserId = a.UserId)
				AND isnull(IC.DepartmentName,'') <> ''
		
				update u set u.RoleId = ZD.Id
				from ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join AspNetRoles ZD on IC.RoleName = ZD.Name
				inner join AspNetUserRoles u on u.UserId = b.Id
				where isnull(IC.RoleName,'') <> ''
				
				insert into AspNetUserRoles(UserId,RoleId)
				SELECT b.Id as ASPNetUserId, ZD.Id as RoleId
				from ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join AspNetRoles ZD on IC.RoleName = ZD.Name
				where NOT EXISTS (SELECT TOP  1 1 FROM AspNetUserRoles u WHERE u.UserId = b.Id)
				AND EXISTS(SELECT * FROM @InsertZnodeUser IZU WHERE A.UserId = IZU.UserId)
				AND isnull(IC.RoleName,'') <> ''


		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN A;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		
		 DECLARE @Error_procedure VARCHAR(8000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportCustomer @TableName = '+CAST(@TableName AS VARCHAR(max))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@ImportProcessLogId='+CAST(@ImportProcessLogId AS VARCHAR(10))+',@PortalId='+CAST(@PortalId AS VARCHAR(10))+',@CsvColumnString='+CAST(@CsvColumnString AS VARCHAR(max));
              			
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_ImportCustomer',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
	END CATCH;
END;