﻿CREATE PROCEDURE [dbo].[Znode_PublishSingleCategoryEntity]
(
    @PimCategoryId  int 
   ,@RevisionType   Varchar(30) = ''
   ,@UserId int = 0
   ,@IsAssociate int  = 0 OUT
   ,@PimCatalogId int = 0
)
AS
/*
	To publish single category 
	Unit Testing : 
	*/
BEGIN
BEGIN TRY 
SET NOCOUNT ON
	Declare @Status Bit =0 
	Declare @Type varchar(50) = '',	@CMSSEOCode varchar(300);
	SET @Status = 1 
	Declare @IsPreviewEnable int 
	If Exists (SELECT  * from ZnodePublishStateApplicationTypeMapping PSA where PSA.IsEnabled =1 and  
	Exists (select TOP 1 1  from ZnodePublishState PS where PS.PublishStateId = PSA.PublishStateId ) and ApplicationType =  'WebstorePreview')
		SET @IsPreviewEnable = 1 
	else 
		SET @IsPreviewEnable = 0 

	Truncate table ZnodePublishSingleCategoryErrorLogEntity 
	Begin Transaction 
		
	If @Type = 'ZnodePublishSingleCategoryEntity' OR @Type = ''
	Begin
		-- Initiate Single Category Publish 
		--Insert INTO @TBL_OutSingleCategory(PublishCategoryId,VersionId,LocaleId,PublishCatalogId) 
		EXEC Znode_GetPublishSingleCategoryJson 
			 @PimCategoryId = @PimCategoryId 
			,@UserId = @UserId 
			,@Status = @Status  Out 
			,@PimCatalogId =@PimCatalogId 
			,@RevisionType = @RevisionType
			,@IsAssociate = @IsAssociate out 
		If @Status = 0 
			Rollback Transaction 
		If @IsAssociate  = 0  
		Begin
			if object_Id('tempdb..##PublishCategoryDetails') is not null
			drop table ##PublishCategoryDetails

			Create TABLE ##PublishCategoryDetails 
             (PublishCategoryId INT,
			  PublishCatalogId  INT,
              CategoryXml       XML,
              LocaleId          INT,
			  VersionId		    INT
             );
		end
		INSERT INTO ZnodePublishSingleCategoryErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
		SELECT 'ZnodePublishSingleCategoryEntity', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(),@UserId , '' 
		If @Status  = 0 
		Begin
			SELECT 1 AS ID,@Status AS Status;
			Return 0 
		End
	End
	If @Type = 'ZnodePublishSEOEntity' OR @Type = ''
	Begin
			Select Distinct ZPC.PortalId, a.PublishCatalogId, a.PublishCategoryId, ZPCD.CategoryCode, ZCSD.SEOUrl, a.VersionId,a.LocaleId
			into #SEOSingleCategoryDetailsDetails 
			from ##PublishCategoryDetails a
			inner join ZnodePortalCatalog ZPC on a.PublishCatalogId = ZPC.PublishCatalogId
			inner join ZnodePublishCategory PC on a.PublishCatalogId = PC.PublishCatalogId and a.PublishCategoryId = PC.PublishCategoryId
			inner join ZnodePublishCategoryDetail ZPCD on a.PublishCategoryId = ZPCD.PublishCategoryId 
			left join ZnodeCMSSEODetail ZCSD on ZPC.PortalId = ZCSD.PortalId and ZPCD.CategoryCode = ZCSD.SEOCode
			    and CMSSEOTypeId = (select top 1 CMSSEOTypeId from ZnodeCMSSEOType where Name = 'Category')
				where ZCSD.CMSSEODetailId is not null 

			Declare @Seo_VersionId Varchar(10 )
			,@Seo_LocaleId int 
			,@Seo_CategoryCode Varchar(300)
			,@Seo_PortalId int 

			DECLARE Cr_Attribute CURSOR LOCAL FAST_FORWARD
			FOR SELECT VersionId,LocaleId ,CategoryCode,PortalId from #SEOSingleCategoryDetailsDetails    
			OPEN Cr_Attribute;
			FETCH NEXT FROM Cr_Attribute INTO @Seo_VersionId,@Seo_LocaleId ,@Seo_CategoryCode,@Seo_PortalId ;
			WHILE @@FETCH_STATUS = 0
			BEGIN

			EXEC [Znode_SetPublishSEOEntity]
				  @CMSSEOTypeId = '2' 
				 ,@UserId  = @UserId 
				 ,@Status = @Status  OUTPUT 
				 ,@IsCatalogPublish = 0  
				 ,@IsSingleProductPublish =1 
				 ,@VersionIdString = @Seo_VersionId  
				 ,@CMSSEOCode = @Seo_CategoryCode
				 ,@PortalId  =@Seo_PortalId 
				 ,@LocaleId = @Seo_LocaleId
				 ,@RevisionState = @RevisionType 
				 ,@IsPreviewEnable =@IsPreviewEnable
				 If @Status  = 0 
				 Begin
					Rollback Transaction 
					CLOSE Cr_Attribute;
					DEALLOCATE Cr_Attribute;
				 End
		
				INSERT INTO ZnodePublishSingleCategoryErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
				SELECT 'ZnodePublishSEOEntity', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
				@UserId ,''
				If @Status  = 0 
				Begin
					SELECT 1 AS ID,@Status AS Status;
					Return 0 
				End
			FETCH NEXT FROM Cr_Attribute INTO @Seo_VersionId,@Seo_LocaleId ,@Seo_CategoryCode,@Seo_PortalId ;
			END;
			CLOSE Cr_Attribute;
			DEALLOCATE Cr_Attribute;
	End 

	IF Exists (select TOP 1 1  from ZnodePublishSingleCategoryErrorLogEntity where  ProcessStatus = 'Fail') 
		Begin
			Rollback transaction
			SET @Status  =0 
			SELECT 1 AS ID,@Status AS Status;
			INSERT INTO ZnodePublishSingleCategoryErrorLogEntity
			(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'ZnodePublishsingleCategoryEntity', '' , 'Fail' , Getdate(), 
			@UserId ,''
			Return 0 
		End

	SET @Status = 1
	Commit Transaction 

	select * from ##PublishCategoryDetails
	Select * from #SEOSingleCategoryDetailsDetails     		
	SELECT 0 AS id,@Status AS Status;   

END TRY 
BEGIN CATCH 
	SET @Status =0  
	 SELECT 1 AS ID,@Status AS Status;   
	 Rollback transaction
	 DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
		@ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_PublishSingleCategoryEntity
		@PimCategoryId = '+CAST(@PimCategoryId  AS VARCHAR	(max))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@RevisionType='+CAST(@RevisionType AS VARCHAR(10))
			
	INSERT INTO ZnodePublishSingleCategoryErrorLogEntity
	(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
	SELECT 'PublishSingleCategoryEntity', '' + isnull(@ErrorMessage,'') , 'Fail' , Getdate(), 
	@UserId ,''


	EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_PublishSingleCategoryEntity',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
END CATCH
END