﻿CREATE PROCEDURE [dbo].[Znode_GetSearchTriggerItemRuleDetails]
(
	@Keyword nvarchar(100) = '',
	@PublishCatalogId int
)
AS
BEGIN

	SET NOCOUNT ON;
	BEGIN TRY

	DECLARE @GetDate DATETIME= dbo.Fn_GetDate()

		;WITH cte_Trigger AS
		(
			SELECT DISTINCT ZSCR.SearchCatalogRuleId, ZSTR.SearchTriggerRuleId, ZSTR.SearchTriggerValue, ZSTR.SearchTriggerCondition,ZSCR.IsItemForAll
			FROM ZnodeSearchCatalogRule ZSCR 
			INNER JOIN ZnodeSearchTriggerRule ZSTR ON ZSCR.SearchCatalogRuleId = ZSTR.SearchCatalogRuleId 
			WHERE ZSCR.PublishCatalogId = @PublishCatalogId AND ZSCR.IsGlobalRule = 0 AND IsPause <> 1
			AND CAST(@GetDate AS DATE) BETWEEN CAST(ZSCR.StartDate AS DATE) AND ISNULL(ZSCR.EndDate,'9999-01-01')
		)
		,cte_PresentCount as
		(
			SELECT CT.SearchCatalogRuleId, CT.SearchTriggerRuleId,CT.IsItemForAll,Count(1) Cnt
			FROM cte_Trigger CT 
			CROSS APPLY Dbo.Split( CT.SearchTriggerValue ,' ') ff
			WHERE (( CT.SearchTriggerCondition='Contains' AND EXISTS (SELECT 1 FROM  dbo.Split(@KeyWord,' ') CK WHERE CK.Item like '%'+ff.Item+'%'  ))
					--OR ( CT.SearchTriggerCondition='Is' AND EXISTS (SELECT 1 FROM dbo.Split(@KeyWord,' ') CK1 WHERE CK1.Item like  ff.Item   ))
					OR ( CT.SearchTriggerCondition='Is' AND CT.SearchTriggerValue = @KeyWord ))
			GROUP BY CT.SearchCatalogRuleId, CT.SearchTriggerRuleId,CT.IsItemForAll
		)
		SELECT DISTINCT CPC.SearchCatalogRuleId,ZSIR.SearchItemKeyword, ZSIR.SearchItemCondition, ZSIR.SearchItemValue, ZSIR.SearchItemBoostValue, CPC.IsItemForAll
		FROM cte_PresentCount CPC
		INNER JOIN ZnodeSearchItemRule ZSIR ON CPC.SearchCatalogRuleId = ZSIR.SearchCatalogRuleId 
		WHERE EXISTS (SELECT 1 FROM cte_Trigger CT WHERE CT.SearchTriggerRuleId=CPC.SearchTriggerRuleId 
		AND  ((CT.IsItemForAll=0) OR (CT.IsItemForAll=1 and ( len(CT.SearchTriggerValue)- len(replace(CT.SearchTriggerValue,' ',''))+1) = CPC.Cnt )))
		UNION ALL
		SELECT DISTINCT ZSCR.SearchCatalogRuleId,ZSIR.SearchItemKeyword, ZSIR.SearchItemCondition, ZSIR.SearchItemValue, ZSIR.SearchItemBoostValue, ZSCR.IsItemForAll 
		FROM ZnodeSearchCatalogRule ZSCR 
		LEFT JOIN ZnodeSearchTriggerRule ZSTR ON ZSCR.SearchCatalogRuleId = ZSTR.SearchCatalogRuleId 
		INNER JOIN ZnodeSearchItemRule ZSIR ON ZSCR.SearchCatalogRuleId = ZSIR.SearchCatalogRuleId 
		WHERE ZSCR.PublishCatalogId = @PublishCatalogId AND ZSCR.IsGlobalRule = 1 
		AND CAST( @GetDate AS DATE ) BETWEEN CAST( ZSCR.StartDate AS DATE ) AND ISNULL( ZSCR.EndDate,'9999-01-01' ) 
		AND IsPause <> 1

	END TRY
	BEGIN CATCH
		DECLARE @Status BIT ;
		SET @Status = 0;
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), 
				@ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
				@ErrorLine VARCHAR(100)= ERROR_LINE(), 
				@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetSearchTriggerItemRuleDetails @Keyword = '+@Keyword+' @PublishCatalogId = '+CAST(@PublishCatalogId AS VARCHAR(50));
              			 
		SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		   
		EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetSearchTriggerItemRuleDetails',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
	END CATCH;

END