﻿CREATE procedure [dbo].[Znode_GetCMSWidgetlist]  
(    
  @WhereClause NVARCHAR(Max)       
 ,@Rows INT = 100       
 ,@PageNo INT = 1       
 ,@Order_BY VARCHAR(1000) = ''    
 ,@RowsCount INT OUT    
)    
AS    
  
BEGIN      
	  BEGIN TRY     
		SET NOCOUNT ON        
		DECLARE @SQL NVARCHAR(MAX)    
        

		DECLARE @TBL_ContentWidget TABLE (ContentWidgetId INT,WidgetKey NVARCHAR(max),PortalId INT,Tags NVARCHAR(max),StoreName NVARCHAR(max),RowId INT,CountNo INT,IsGlobalContentWidget nvarchar(5))  
      SET @SQL = '   
				 ;With Cte_ContentWidget AS   
				 (  
				 SELECT ZCW.CMSContentWidgetId, ZCW.WidgetKey, ZCW.PortalId, ZCW.Tags,
					Case when ZCW.PortalId is null then  ''All'' else ZP.StoreName end StoreName  ,
					cast(CASE WHEN ZCW.PortalId IS NULL  THEN  ''true'' ELSE  ''false'' END as varchar(10)) IsGlobalContentWidget  
				 FROM ZnodeCMSContentWidget ZCW  
				 LEFT JOIN ZnodePortal ZP  on (ZCW.PortalId = ZP.PortalId )        
				 )  
				 ,Cte_ContentWidgetList AS
				 (
					SELECT CMSContentWidgetId, WidgetKey, PortalId, Tags,StoreName,IsGlobalContentWidget,
					'+dbo.Fn_GetPagingRowId(@Order_BY,'WidgetKey ASC')+',Count(*)Over() CountNo 
					FROM Cte_ContentWidget    
					   WHERE 1=1 '+dbo.Fn_GetFilterWhereClause(@WhereClause)+'    
				 )

				SELECT CMSContentWidgetId,WidgetKey,PortalId,Tags,StoreName,RowId,CountNo ,IsGlobalContentWidget   
				 FROM Cte_ContentWidgetList   
				 '+dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows)  
       
				 print @sql  
  
  
				 INSERT INTO @TBL_ContentWidget (ContentWidgetId,WidgetKey,PortalId,Tags,StoreName,RowId,CountNo,IsGlobalContentWidget)  
				 EXEC (@SQL)  
				 SET @RowsCount = ISNULL((SELECT TOP 1 CountNo FROM @TBL_ContentWidget ),0)  

  
				 SELECT ContentWidgetId,PortalId,WidgetKey,Tags, StoreName,IsGlobalContentWidget
				 FROM @TBL_ContentWidget  

		 END TRY    
         BEGIN CATCH    
            DECLARE @Status BIT ;    
		    SET @Status = 0;    
		    DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= 
		    ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 
		   'EXEC Znode_GetCMSWidgetlist @WhereClause = '+CAST(@WhereClause AS VARCHAR(max))+',
		    @Rows='+CAST(@Rows AS VARCHAR(50))+',@PageNo='+CAST(@PageNo AS VARCHAR(50))+',@Order_BY='+@Order_BY+',
		    @RowsCount='+CAST(@RowsCount AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));    
                      
               SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                       
        
			EXEC Znode_InsertProcedureErrorLog    
			@ProcedureName = 'Znode_GetCMSWidgetlist',    
			@ErrorInProcedure = @Error_procedure,    
			@ErrorMessage = @ErrorMessage,    
			@ErrorLine = @ErrorLine,    
			@ErrorCall = @ErrorCall;                                
         END CATCH; 
 END;    
