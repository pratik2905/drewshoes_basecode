﻿CREATE PROCEDURE [dbo].[Znode_DeleteAssociatedVariants]
(@WidgetProfileVariantId  INT,
@status  BIT OUT)


AS

BEGIN
		BEGIN TRY
			 BEGIN TRAN DeleteAssociatedVariants
			
			DECLARE @DefaultId INT
			DECLARE @widgetId INT, @localeId INT, @profileId INT, @count INT
			SELECT TOP 1 @DefaultId  = LocaleId FROM ZnodeLocale WHERE IsDefault = 1

			SELECT TOP 1 @widgetId = CMSContentWidgetId,@localeId = LocaleId, @profileId = ProfileId  FROM ZnodeCMSWidgetProfileVariant 
			WHERE CMSWidgetProfileVariantId = @WidgetProfileVariantId

			 IF(@localeId = @DefaultId)
				 BEGIN
						SET @Status = 0;
						SELECT 1 AS ID,
						CAST(0 AS BIT) AS [Status];
				 End
			 
			 ELSE
				 BEGIN
						DELETE  L FROM ZnodeWidgetGlobalAttributeValueLocale L	
								inner join ZnodeWidgetGlobalAttributeValue V ON
								L.WidgetGlobalAttributeValueId = V.WidgetGlobalAttributeValueId
								WHERE V.CMSWidgetProfileVariantId = @WidgetProfileVariantId 

						DELETE  ZnodeWidgetGlobalAttributeValue WHERE CMSWidgetProfileVariantId = @WidgetProfileVariantId 

						DELETE  ZnodeCMSWidgetProfileVariant WHERE CMSWidgetProfileVariantId = @WidgetProfileVariantId 

						SET @Status = 1;
						SELECT 1 AS ID,
						CAST(1 AS BIT) AS [Status];
				 end			
		
             COMMIT TRAN DeleteAssociatedVariants;
		END TRY

		BEGIN CATCH

             DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
			 @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_DeleteAssociatedVariants 
			 @WidgetProfileVariantId = '+@WidgetProfileVariantId+',@Status='+CAST(@Status AS VARCHAR(50));

			 SET @Status =0  
			 SELECT 1 AS ID,@Status AS Status;  
             ROLLBACK TRAN DeleteAttributeFamily;
             EXEC Znode_InsertProcedureErrorLog
                  @ProcedureName = 'Znode_DeleteAssociatedVariants',
                  @ErrorInProcedure = @Error_procedure,
                  @ErrorMessage = @ErrorMessage,
                  @ErrorLine = @ErrorLine,
                  @ErrorCall = @ErrorCall;
	       
		END CATCH

END
