﻿CREATE  PROCEDURE [dbo].[Znode_DeleteHighlights] 
	(
       @HighlighId VARCHAR(300) = NULL ,
       @Status         INT OUT)
AS
/*
    Summary:  Remove Highlights with their respective details and from reference tables		   	             
    Unit Testing   
    			
     DECLARE @Status INT 
	 EXEC Znode_DeleteHighlights @HighlighId = 3 ,@Status=@Status OUT  
    */
BEGIN

	BEGIN TRY
             SET NOCOUNT ON;
             BEGIN TRAN A;
             DECLARE @DeletdHighlightId TABLE (
                                              HighlightCode VARCHAR(300)
                                              );

			




             INSERT INTO @DeletdHighlightId
                    SELECT b.HighlightCode
                    FROM dbo.split ( @HighlighId , ','
                                   ) AS a INNER JOIN ZnodeHighlight AS B ON ( a.item = b.HighlightId )


			
update ZPP set PublishStateId = (select top 1 PublishStateId from ZnodePublishState where StateName = 'Draft')
from ZnodePimProduct ZPP
INNER JOIN ZnodePimAttributeValue ZPAV ON ZPP.PimProductId = ZPAV.PimProductId
INNER JOIN ZnodePimProductAttributeDefaultValue ZPPADV ON ZPAV.PimAttributeValueId = ZPPADV.PimAttributeValueId
inner join ZnodePimAttributeDefaultValue ZPDV ON ZPPADV.PimAttributeDefaultValueId = ZPDV.PimAttributeDefaultValueId and ZPAV.PimAttributeId = ZPDV.PimAttributeId
INNER JOIN ZnodeHighlight ZHL ON ZPDV.AttributeDefaultValueCode = ZHL.HighlightCode
where ZPDV.PimAttributeId = (select TOP 1 PimAttributeId from ZnodePimAttribute ZPD where AttributeCode = 'Highlights')
and ZHL.HighlightCode in ( select HighlightCode from  @DeletdHighlightId)
              
			  Delete from ZnodePimAttributeDefaultValueLocale
			  where exists (select top 1 1 from ZnodePimAttributeDefaultValue ZD 
								inner join @DeletdHighlightId h on h.HighlightCode =zd.AttributeDefaultValueCode and zd.PimAttributeId = dbo.Fn_GetProductHighlightsAttributeId()
								where zd.PimAttributeDefaultValueId =ZnodePimAttributeDefaultValueLocale.PimAttributeDefaultValueId);
				
	  		 
			Delete from ZnodePimProductAttributeDefaultValue
				where exists (  select top 1 1 from ZnodePimAttributeDefaultValue ZD 
								inner join @DeletdHighlightId h on h.HighlightCode =zd.AttributeDefaultValueCode 
								INNER JOIN ZnodePimAttributeValue za ON zd.PimAttributeDefaultValueId = ZA.PimAttributeDefaultValueId
								where ZA.PimAttributeValueId =ZnodePimProductAttributeDefaultValue.PimAttributeValueId and ZA.PimAttributeDefaultValueId =ZnodePimProductAttributeDefaultValue.PimAttributeDefaultValueId);
  			
			Delete from ZnodePimAttributeValue
			  where exists (select top 1 1 from ZnodePimAttributeDefaultValue ZD 
								inner join @DeletdHighlightId h on h.HighlightCode =zd.AttributeDefaultValueCode and zd.PimAttributeId = dbo.Fn_GetProductHighlightsAttributeId()
								where zd.PimAttributeDefaultValueId =ZnodePimAttributeValue.PimAttributeDefaultValueId)
								and PimAttributeValueId not in ( select PimAttributeValueId from  ZnodePimProductAttributeDefaultValue);
			--Delete from ZnodePimAttributeDefaultValue
			--	where exists (select Top 1 1 from @DeletdHighlightId H where H.HighlightCode= ZnodePimAttributeDefaultValue.AttributeDefaultValueCode
			--			and ZnodePimAttributeDefaultValue.PimAttributeId = dbo.Fn_GetProductHighlightsAttributeId());
  
			Delete from ZnodeHighlightLocale
				where exists (select top 1 1 from @DeletdHighlightId h inner join ZnodeHighlight ZH on h.HighlightCode =ZH.HighlightCode
					where ZH.HighlightId  = ZnodeHighlightLocale.HighlightId);
  
			Delete from ZnodeHighlight
				where exists (select top 1 1 from @DeletdHighlightId h where  h.HighlightCode =ZnodeHighlight.HighlightCode);

			   SET @Status = 1;
             COMMIT TRAN A;
                         
	END TRY
	BEGIN CATCH
			ROLLBACK TRAN A;
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_DeleteHighlights @HighlighId = '+@HighlighId+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		     
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_DeleteHighlights',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;

				select  ERROR_MESSAGE(),ERROR_LINE()
	END CATCH

   
END