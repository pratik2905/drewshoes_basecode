﻿CREATE TABLE ZnodeCMSWidgetProfileVariant (
    CMSWidgetProfileVariantId int NOT NULL  IDENTITY(1,1),
    CMSContentWidgetId int  NOT NULL,
	ProfileId int,
	LocaleId int  NOT NULL,
	CMSWidgetTemplateId int,
	CONSTRAINT [PK_ZnodeCMSWidgetProfileLocale] PRIMARY KEY CLUSTERED ([CMSWidgetProfileVariantId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeCMSWidgetProfileLocale_ZnodeCMSContentWidget] FOREIGN KEY ([CMSContentWidgetId]) REFERENCES [dbo].[ZnodeCMSContentWidget] ([CMSContentWidgetId]),
    CONSTRAINT [FK_ZnodeCMSWidgetProfileLocale_ZnodeProfile] FOREIGN KEY ([ProfileId]) REFERENCES [dbo].[ZnodeProfile] ([ProfileId]),
    CONSTRAINT [FK_ZnodeCMSWidgetProfileLocale_ZnodeLocale] FOREIGN KEY ([LocaleId]) REFERENCES [dbo].[ZnodeLocale] ([LocaleId]),
    CONSTRAINT [FK_ZnodeCMSWidgetProfileLocale_ZnodeCMSWidgetTemplate] FOREIGN KEY ([CMSWidgetTemplateId]) REFERENCES [dbo].[ZnodeCMSWidgetTemplate] ([CMSWidgetTemplateId]),

);