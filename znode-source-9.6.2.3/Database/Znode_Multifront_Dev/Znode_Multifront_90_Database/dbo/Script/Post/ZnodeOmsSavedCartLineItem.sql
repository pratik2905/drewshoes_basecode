﻿
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IDX_ZnodeOmsPersonalizeCartItem_OmsSavedCartLineItemId' AND object_id = OBJECT_ID('ZnodeOmsPersonalizeCartItem'))
    BEGIN
       CREATE NONCLUSTERED INDEX IDX_ZnodeOmsPersonalizeCartItem_OmsSavedCartLineItemId
			ON [dbo].ZnodeOmsPersonalizeCartItem (OmsSavedCartLineItemId)
			
    END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IDX_ZnodeOmsSavedCartLineItemDetails_OmsSavedCartLineItemId' AND object_id = OBJECT_ID('ZnodeOmsSavedCartLineItemDetails'))
    BEGIN
        CREATE NONCLUSTERED INDEX IDX_ZnodeOmsSavedCartLineItemDetails_OmsSavedCartLineItemId
			ON [dbo].ZnodeOmsSavedCartLineItemDetails (OmsSavedCartLineItemId)
    END

	IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_ZnodeOmsSavedCart_OmsCookieMappingId' AND object_id = OBJECT_ID('ZnodeOmsSavedCart'))
    BEGIN
        CREATE NONCLUSTERED INDEX IX_ZnodeOmsSavedCart_OmsCookieMappingId
			ON [dbo].[ZnodeOmsSavedCart] (OmsCookieMappingId)
    END