﻿update a set a.Email = b.Email, a.PhoneNumber = b.PhoneNumber from ZnodeOmsOrderDetails a
inner join ZnodeUser b on a.UserId = b.UserId
where a.Email is null

update a set a.PhoneNumber = b.PhoneNumber from ZnodeOmsOrderDetails a
inner join ZnodeUser b on a.UserId = b.UserId
where a.PhoneNumber is null