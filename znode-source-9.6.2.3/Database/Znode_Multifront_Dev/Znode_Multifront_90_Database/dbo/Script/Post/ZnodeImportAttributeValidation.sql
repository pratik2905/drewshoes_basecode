﻿----ZPD-7111 --> ZPD-8887
insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','CostPrice',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing'),0,'Yes/No','AllowNegative',
null,'false','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'CostPrice' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','CostPrice',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing'),0,'Yes/No','AllowDecimals',
null,'false','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'CostPrice' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','CostPrice',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing'),0,'Number','MinNumber',
null,'0','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'CostPrice' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','CostPrice',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing'),0,'Number','MaxNumber',
null,'999999','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'CostPrice' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Pricing')
	  and ValidationName = 'MaxNumber')

--dt 31-07-2020
update ZnodeImportAttributeValidation set IsRequired = 1 
where AttributeCode = 'IsActive' and ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')

update  ZnodeImportAttributeValidation set IsRequired = 0
where AttributeCode = 'UserName' and ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')

delete from ZnodeImportAttributeValidation where importheadid = (select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'VoucherAmount' and AttributeTypeName = 'Number'

delete from ZnodeImportAttributeValidation where importheadid = (select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'RemainingAmount' and AttributeTypeName = 'Number'

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','VoucherAmount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),null
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'VoucherAmount' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','RemainingAmount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),null
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'RemainingAmount' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

------------ZPD-13943
insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select 'Number','EmailOptIn',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Yes/No','AllowNegative',	NULL,	'false',' '	,	NULL,	2,getdate(),2,getdate(),15
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EmailOptIn' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select'Number','EmailOptIn',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Yes/No','AllowDecimals',	NULL,	'false',' ',		NULL,	2,getdate(),2,getdate(),15
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EmailOptIn' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select'Number','EmailOptIn',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Number','MinNumber',	NULL,	0	,'',	NULL,	2,getdate(),2,getdate(),15
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EmailOptIn' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)  
select 'Number','EmailOptIn',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Number','MaxNumber',	NULL,	999999,'',		NULL,	2,getdate(),2,getdate(),15
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EmailOptIn' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'MaxNumber')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select 'Text','PerOrderAnnualLimit',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Text','RegularExpression',	NULL,'',	'',		NULL,	2,getdate(),2,getdate(),16
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'PerOrderAnnualLimit' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)  
select 'Text','BillingAccountNumber',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Text','RegularExpression',	NULL,'',	'',		NULL,	2,getdate(),2,getdate(),17
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'BillingAccountNumber' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select 'Text','EnableUserShippingAddressSuggestion',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Text','RegularExpression',	NULL,'',	'',		NULL,	2,getdate(),2,getdate(),18
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'EnableUserShippingAddressSuggestion' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select 'Number','EnablePowerBIReportOnWebStore',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Yes/No','AllowNegative',	NULL,	'false'	,' '	,	NULL,	2,getdate(),2,getdate(),19
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EnablePowerBIReportOnWebStore' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)   
select 'Number','EnablePowerBIReportOnWebStore',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Yes/No','AllowDecimals',	NULL,	'false',' '	,		NULL,	2,getdate(),2,getdate(),19
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EnablePowerBIReportOnWebStore' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'AllowDecimals')
insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select 'Number','EnablePowerBIReportOnWebStore',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Number','MinNumber',	NULL,	0	,' '	,	NULL,	2,getdate(),2,getdate(),19
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EnablePowerBIReportOnWebStore' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)  
select 'Number','EnablePowerBIReportOnWebStore',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Number','MaxNumber',	NULL,	999999,' '	,		NULL,	2,getdate(),2,getdate(),19
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'EnablePowerBIReportOnWebStore' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'MaxNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber) 
select 'Text','PerOrderLimit',	(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),	0,	'Text','RegularExpression',	NULL,'',	'',		NULL,	2,getdate(),2,getdate(),20
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'PerOrderLimit' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

	