﻿--dt 30-07-2020 ZPD-11579 --> ZPD-11731
update ZnodeCMSContentPageslocale set PageTitle  = 'Landing Page' 
where CMSContentPagesId = ( select top 1 CMSContentPagesId from ZnodeCMSContentPages where PageNAme = 'Landing Page' )

update  ZnodeCMSSEODetail set MetaInformation = 'Landing-Page' , SEOUrl = 'Landing-Page' where SEOCode = 'Landing Page' 

update ZnodeCMSSEODetailLocale set SEOTitle = 'Landing Page' ,SEOKeywords = 'Landing Page' 
 where CMSSEODetailId = (select top 1 CMSSEODetailId from ZnodeCMSSEODetail where SEOCode = 'Landing Page')
