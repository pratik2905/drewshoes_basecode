﻿
----ZPD-7111 --> ZPD-8887
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'PriceTemplate'),
'CostPrice' SourceColumnName,'CostPrice' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=3 AND SourceColumnName ='CostPrice')

--dt 27-02-2020 ZPD-4958 and ZLMC-1154
update a set a.SourceColumnName = 'CategoryCode' , a.TargetColumnName = 'CategoryCode'
from ZnodeImportTemplateMapping a
inner join ZnodeImportTemplate b on a.ImportTemplateId = b.ImportTemplateId
where b.TemplateName = 'CategoryAssociation' and SourceColumnName = 'CategoryName'

update a set a.AttributeCode = 'CategoryCode'
from ZnodeImportAttributeValidation a
inner join ZnodeImportHead b on a.ImportHeadId = b.ImportHeadId
where b.Name = 'CategoryAssociation' and a. AttributeCode = 'CategoryName'

---------------------------------------------------------------------
-- ZPD-11728

insert into ZnodeImportHead(Name,	IsUsedInImport,	IsUsedInDynamicReport,	IsActive,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate,	IsCsvUploader)
select 'Voucher',1,1,1,2,getdate(),2,getdate(),null
where not exists(select * from ZnodeImportHead where Name = 'Voucher')

insert into ZnodeImportTemplate(ImportHeadId,TemplateName,TemplateVersion,PimAttributeFamilyId,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 ImportHeadId from ZnodeImportHead where name = 'Voucher'),'VoucherTemplate',null,null,1,2,getdate(),2,getdate()
where not exists(select * from ZnodeImportTemplate where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where name = 'Voucher')
	and TemplateName = 'VoucherTemplate' )


INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'StoreCode' SourceColumnName,'StoreCode' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='StoreCode')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'VoucherName' SourceColumnName,'VoucherName' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='VoucherName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'CardNumber' SourceColumnName,'CardNumber' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='CardNumber')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'Amount' SourceColumnName,'Amount' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='Amount')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'UserName' SourceColumnName,'UserName' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='UserName')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'ExpirationDate' SourceColumnName,'ExpirationDate' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='ExpirationDate')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'IsReferralCommission' SourceColumnName,'IsReferralCommission' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='IsReferralCommission')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'IsActive' SourceColumnName,'IsActive' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='IsActive')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'RemainingAmount' SourceColumnName,'RemainingAmount' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='RemainingAmount')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'RestrictToCustomerAccount' SourceColumnName,'RestrictToCustomerAccount' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='RestrictToCustomerAccount')


INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'StartDate' SourceColumnName,'StartDate' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='StartDate')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','StoreCode',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),1
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'StoreCode' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','VoucherName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),2
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'VoucherName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','CardNumber',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'CardNumber' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','Amount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowNegative',
null,'false','',null,2,getdate(),2,getdate(),4
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'Amount' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','Amount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowDecimals',
null,'false','',null,2,getdate(),2,getdate(),4
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'Amount' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','Amount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MinNumber',
null,'0','',null,2,getdate(),2,getdate(),4
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'Amount' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','Amount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MaxNumber',
null,'999999','',null,2,getdate(),2,getdate(),4
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'Amount' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MaxNumber')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','UserName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),5
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'UserName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Date','StartDate',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Date','MinDate',
null,'','',null,2,getdate(),2,getdate(),6
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Date' and AttributeCode = 'StartDate' 
      and ControlName = 'Date' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinDate')



insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Date','ExpirationDate',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Date','MinDate',
null,'','',null,2,getdate(),2,getdate(),7
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Date' and AttributeCode = 'ExpirationDate' 
      and ControlName = 'Date' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinDate')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsReferralCommission',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowNegative',
null,'false','',null,2,getdate(),2,getdate(),8
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsReferralCommission' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsReferralCommission',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowDecimals',
null,'false','',null,2,getdate(),2,getdate(),8
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsReferralCommission' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsReferralCommission',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MinNumber',
null,'0','',null,2,getdate(),2,getdate(),8
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsReferralCommission' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsReferralCommission',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MaxNumber',
null,'999999','',null,2,getdate(),2,getdate(),8
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsReferralCommission' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MaxNumber')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsActive',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Yes/No','AllowNegative',
null,'false','',null,2,getdate(),2,getdate(),9
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsActive' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsActive',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Yes/No','AllowDecimals',
null,'false','',null,2,getdate(),2,getdate(),9
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsActive' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsActive',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Number','MinNumber',
null,'0','',null,2,getdate(),2,getdate(),9
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsActive' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','IsActive',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),0,'Number','MaxNumber',
null,'999999','',null,2,getdate(),2,getdate(),9
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'IsActive' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MaxNumber')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RemainingAmount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowNegative',
null,'false','',null,2,getdate(),2,getdate(),10
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RemainingAmount' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RemainingAmount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowDecimals',
null,'false','',null,2,getdate(),2,getdate(),10
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RemainingAmount' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RemainingAmount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MinNumber',
null,'0','',null,2,getdate(),2,getdate(),10
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RemainingAmount' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RemainingAmount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MaxNumber',
null,'999999','',null,2,getdate(),2,getdate(),10
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RemainingAmount' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MaxNumber')


insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RestrictToCustomerAccount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowNegative',
null,'false','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RestrictToCustomerAccount' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowNegative')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RestrictToCustomerAccount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Yes/No','AllowDecimals',
null,'false','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RestrictToCustomerAccount' 
      and ControlName = 'Yes/No' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'AllowDecimals')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RestrictToCustomerAccount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MinNumber',
null,'0','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RestrictToCustomerAccount' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MinNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Number','RestrictToCustomerAccount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Number','MaxNumber',
null,'999999','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Number' and AttributeCode = 'RestrictToCustomerAccount' 
      and ControlName = 'Number' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'MaxNumber')

go
update ZnodeImportAttributeValidation set ValidationValue = '0.01'
where AttributeCode = 'Amount' and ValidationName = 'MinNumber'
go

--dt 18-08-2020

delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'CardNumber'

delete from ZnodeImportTemplateMapping 
where ImportTemplateId = (select top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate')
and SourceColumnName  = 'CardNumber'

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'VoucherNumber' SourceColumnName,'VoucherNumber' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='VoucherNumber')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','VoucherNumber',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'VoucherNumber' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

update  ZnodeImportAttributeValidation set IsRequired = 0
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'VoucherNumber' and AttributeTypeName = 'Text'


update ZnodeImportAttributeValidation set AttributeCode = 'VoucherAmount'
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'Amount' 

update ZnodeImportAttributeValidation set AttributeCode = 'RestrictVoucherToCustomer'
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'RestrictToCustomerAccount' 

update ZnodeImportTemplateMapping set SourceColumnName = 'VoucherAmount', TargetColumnName = 'VoucherAmount'
where SourceColumnName = 'Amount' AND ImportTemplateId = (select top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate')

update ZnodeImportTemplateMapping set SourceColumnName = 'RestrictVoucherToCustomer', TargetColumnName = 'RestrictVoucherToCustomer'
where SourceColumnName = 'RestrictToCustomerAccount' AND ImportTemplateId = (select top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate')

--------

delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'IsActive' and AttributeTypeName = 'Number'

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','IsActive',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'IsActive' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'RestrictToCustomerAccount' and AttributeTypeName = 'Number'

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','RestrictToCustomerAccount',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'RestrictToCustomerAccount' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')
--------
delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'RestrictVoucherToCustomer' and AttributeTypeName = 'Number'


delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'RestrictToCustomerAccount' and AttributeTypeName = 'Text'

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','RestrictVoucherToCustomer',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'RestrictVoucherToCustomer' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')

delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'IsReferralCommission'

delete from ZnodeImportTemplateMapping 
where ImportTemplateId = (select top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate')
and SourceColumnName  = 'IsReferralCommission'


delete from ZnodeImportAttributeValidation
where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
and AttributeCode = 'RestrictVoucherToCustomer' 

delete from ZnodeImportTemplateMapping
where SourceColumnName = 'RestrictVoucherToCustomer' and ImportTemplateId = (select top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate'),
'RestrictVoucherToACustomer' SourceColumnName,'RestrictVoucherToACustomer' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'VoucherTemplate') 
	AND SourceColumnName ='RestrictVoucherToACustomer')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','RestrictVoucherToACustomer',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher'),1,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'RestrictVoucherToACustomer' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Voucher')
	  and ValidationName = 'RegularExpression')



INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate'),
'AccountCode' SourceColumnName,'AccountCode' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='AccountCode')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','AccountCode',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),12
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'AccountCode' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate'),
'DepartmentName' SourceColumnName,'DepartmentName' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='DepartmentName')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','DepartmentName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),13
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'DepartmentName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate'),
'RoleName' SourceColumnName,'RoleName' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='RoleName')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','RoleName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),14
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'RoleName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Customer')
	  and ValidationName = 'RegularExpression')

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 73,'Text','The value is not associated with any existing Account.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 73)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 74,'Text','The value should be either User or Administrator or Manager',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 74)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 75,'Text','Account Code is mandatory to add a Role Name.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 75)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 76,'Text','The value is not associated with the Account.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 76)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 77,'Text','Account and Customer should belong to the same Store.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 77)

--18-09-2020 ZPD-11531 --> ZPD-12188 
insert into ZnodeImportHead(Name,	IsUsedInImport,	IsUsedInDynamicReport,	IsActive,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate,	IsCsvUploader)
select 'Account',1,1,1,2,getdate(),2,getdate(),null
where not exists(select * from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportTemplate(ImportHeadId,TemplateName,TemplateVersion,PimAttributeFamilyId,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 ImportHeadId from ZnodeImportHead where name = 'Account'),'AccountTemplate',null,null,1,2,getdate(),2,getdate()
where not exists(select * from ZnodeImportTemplate where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where name = 'Account')
	and TemplateName = 'AccountTemplate' )


INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'ParentAccountCode' SourceColumnName,'ParentAccountCode' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='ParentAccountCode')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'AccountName' SourceColumnName,'AccountName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='AccountName')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'AccountCode' SourceColumnName,'AccountCode' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='AccountCode')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'ExternalID' SourceColumnName,'ExternalID' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='ExternalID')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'CatalogCode' SourceColumnName,'CatalogCode' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='CatalogCode')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'AddressName' SourceColumnName,'AddressName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='AddressName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'FirstName' SourceColumnName,'FirstName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='FirstName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'LastName' SourceColumnName,'LastName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='LastName')

	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'CompanyName' SourceColumnName,'CompanyName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='CompanyName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'Address1' SourceColumnName,'Address1' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='Address1')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'Address2' SourceColumnName,'Address2' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='Address2')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'CountryName' SourceColumnName,'CountryName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='CountryName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'StateName' SourceColumnName,'StateName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='StateName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'CityName' SourceColumnName,'CityName' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='CityName')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'PostalCode' SourceColumnName,'PostalCode' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='PostalCode')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'PhoneNumber' SourceColumnName,'PhoneNumber' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='PhoneNumber')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'IsDefaultBilling' SourceColumnName,'IsDefaultBilling' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='IsDefaultBilling')
	
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate'),
'IsDefaultShipping' SourceColumnName,'IsDefaultShipping' TargetColumnName,0 DisplayOrder,1 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'AccountTemplate') 
	AND SourceColumnName ='IsDefaultShipping')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','ParentAccountCode',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),1
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'ParentAccountCode' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','AccountName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),2
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'AccountName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','AccountCode',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),3
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'AccountCode' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','ExternalID',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),4
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'ExternalID' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')	  

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','CatalogCode',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),5
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'CatalogCode' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')	

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','AddressName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),6
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'AddressName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')	

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','FirstName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),7
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'FirstName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','LastName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),8
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'LastName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','CompanyName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),9
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'CompanyName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','Address1',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),10
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'Address1' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','Address2',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),11
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'Address2' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','CountryName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),12
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'CountryName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','StateName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),13
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'StateName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','CityName',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),14
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'CityName' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','PostalCode',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),15
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'PostalCode' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','PhoneNumber',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),16
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'PhoneNumber' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','IsDefaultBilling',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),17
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'IsDefaultBilling' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','IsDefaultShipping',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),18
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'IsDefaultShipping' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
	  and ValidationName = 'RegularExpression')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'AddressName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'AddressName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'FirstName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'FirstName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'LastName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'LastName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'CompanyName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'CompanyName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'Address1'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'Address1')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'Address2'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'Address2')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'CountryName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'CountryName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'StateName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'StateName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'CityName'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'CityName')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'PostalCode'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'PostalCode')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'PhoneNumber'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'PhoneNumber')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'IsDefaultBilling'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'IsDefaultBilling')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')

insert into ZnodeImportUpdatableColumns(ImportHeadId,ColumnName)
select (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account'),'IsDefaultShipping'
where not exists(select * from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName = 'IsDefaultShipping')
and exists(select top 1 ImportHeadId from ZnodeImportHead where Name = 'Account')


insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 78,'Text','Maximum 100 characters are allowed.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 78)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 79,'Text','Only alphabets and numbers are allowed.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 79)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 80,'Text','The value is not associated with any existing Catalog.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 80)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 81,'Text','Maximum 200 characters are allowed.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 81)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 82,'Text','Maximum 300 characters are allowed.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 82)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 83,'Text','One of the predefined values is required.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 83)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 84,'Text','Mandatory value is missing',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 84)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 85,'Text','The value should be 1 for new accounts.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 85)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 86,'Text','Parent Account should belong to the selected Store.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 86)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 87,'Text','The value is not associated with any existing Parent Account.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 87)

--18-09-2020 ZPD-12377 --> ZPD-8314
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'ProductAssociation'),
'IsDefault' SourceColumnName,'IsDefault' TargetColumnName,0 DisplayOrder,0 IsActive, 0 IsAllowNull,2 CreatedBy,GETDATE() CreatedDate,2 ModifiedBy,GETDATE() ModifiedDate
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'ProductAssociation') 
	AND SourceColumnName ='IsDefault')

insert into ZnodeImportAttributeValidation(AttributeTypeName,AttributeCode,ImportHeadId,IsRequired,ControlName,ValidationName,SubValidationName
,ValidationValue,RegExp,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SequenceNumber)
select 'Text','IsDefault',(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'ProductAssociation'),0,'Text','RegularExpression',
null,'','',null,2,getdate(),2,getdate(),null
where not exists(select * from ZnodeImportAttributeValidation where AttributeTypeName ='Text' and AttributeCode = 'IsDefault' 
      and ControlName = 'Text' and ImportHeadId=(select Top 1 ImportHeadId from ZnodeImportHead where Name = 'ProductAssociation')
	  and ValidationName = 'RegularExpression')

-- ZPD-12377 
update [dbo].[ZnodeImportAttributeValidation]
set IsRequired =0
where importheadid=(select top 1 importheadid from ZnodeImportHead where Name ='ProductAssociation') and AttributeCode ='DisplayOrder'


------------ZPD-13943
INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') ,'EnablePowerBIReportOnWebStore','EnablePowerBIReportOnWebStore',0,1,1,2,getdate(),2,getdate()
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='EnablePowerBIReportOnWebStore')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') ,'EnableUserShippingAddressSuggestion','EnableUserShippingAddressSuggestion',0,1,1,2,getdate(),2,getdate()
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='EnableUserShippingAddressSuggestion')

INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') ,'BillingAccountNumber','BillingAccountNumber',0,1,1,2,getdate(),2,getdate()
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='BillingAccountNumber')


INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') ,'PerOrderAnnualLimit','PerOrderAnnualLimit',0,1,1,2,getdate(),2,getdate()
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='PerOrderAnnualLimit')


INSERT ZnodeImportTemplateMapping(ImportTemplateId,SourceColumnName,TargetColumnName,DisplayOrder,IsActive,IsAllowNull,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select (select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') ,'PerOrderLimit','PerOrderLimit',0,1,1,2,getdate(),2,getdate()
WHERE NOT EXISTS(SELECT * FROM ZnodeImportTemplateMapping WHERE ImportTemplateId=(select Top 1 ImportTemplateId from ZnodeImportTemplate where TemplateName = 'CustomerTemplate') 
	AND SourceColumnName ='PerOrderLimit')