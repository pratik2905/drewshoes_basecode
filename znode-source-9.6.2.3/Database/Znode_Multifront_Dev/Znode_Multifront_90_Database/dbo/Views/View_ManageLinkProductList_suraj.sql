﻿

-- SELECT   * FROM [View_ManageLinkProductList]

CREATE View [dbo].[View_ManageLinkProductList_suraj]
AS
with AttributeValuePvot AS (
SELECT        zpald.PimProductId,  zpavl1.AttributeValue , zpa.AttributeCode,Zpald.PimLinkProductDetailid,zpald.PimProductId RelatedProductId,zpavl1.LocaleId,zpald.PimAttributeId
FROM ZnodePimLinkProductDetail zpald
INNER JOIN  ZnodePimAttributeValue zpav1 ON (zpav1.PimProductId = zpald.PimProductId) 
INNER JOIN ZnodePimAttributeValueLocale zpavl1 ON (zpavl1.PimAttributeValueId = zpav1.PimAttributeValueId)
INNER JOIN ZnodePimAttribute zpa ON (zpa.PimAttributeId = zpav1.PimAttributeId) 						
WHERE zpa.AttributeCode IN ('ProductName','ProductType', 'SKU', 'Price', 'Quantity', 'Status','Assortment')
)

-- SELECT * FROM ZNodePimProduct



SELECT zpp.PimProductid Productid, [ProductName],ProductType, ''  AttributeFamily , [SKU], [Price], [Quantity][Qty], [Status],PimLinkProductDetailid,RelatedProductId,[Assortment],piv.LocaleId,Piv.PimAttributeId
FROM ZNodePimProduct zpp 
INNER JOIN  AttributeValuePvot 
PIVOT 
(
 Max(AttributeValue) FOR AttributeCode  IN ([ProductName],[SKU],[Price],[Quantity],[Status],[ProductType],[Assortment] )
)Piv  ON (Piv.PimProductId = zpp.PimProductid)